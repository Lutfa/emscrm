package com.emedsurecrm;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.text.method.PasswordTransformationMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.model.LoginEntity;
import com.emedsurecrm.model.SharedPreferenceActivity;
import com.emedsurecrm.model.UserKeyDetails;
import com.emedsurecrm.utils.MocehatCrmDBHelper;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

import javax.net.ssl.HttpsURLConnection;




/**
 * Created by ahmed on 11/3/2016.
 */

public class LogInActivity extends AppCompatActivity {
    EditText et_username, et_password, nickName, password_signUp, confirm_password,/* phNo,*/
            username_signUp, et_referral_signIn, et_referral_signUp;
    RobotoTextView txt_forgot_password, showHidePasswordTV;
    RelativeLayout signIn_btn_layout, signUpLayout, signUp_fulllayout, logIn_fulllayout, signUpLayout_signup, signUp_btn_layout;
    String userName_logIn, password_logIn, txt_username_Signup, txt_passwordSignUp, txt_confirm_password, txt_phNo, txt_nickName;
    String regId;
    private String deviceID = "";
    GoogleCloudMessaging gcm;
    MocehatCrmDBHelper db;
    String deviceUniqueID;
    SharedPreferences app_preference = null;
    public static final String MyPREFERENCES = null;
    ProgressDialog alert;
    ImageView showHidePasswordImage;
    MocehatCrmMainActivity mainActivity;
//    LoginButton loginButton;
//    private CallbackManager callbackManager;
    JSONObject facebookObject = null;
    RelativeLayout language;
    RobotoTextView loginText, signUp_btn;
    ImageView setting_url;
//    SharedPreferencesActivity sharedPreferencesActivity;
    String[] PERMISSIONS = {"android.permission.READ_PHONE_STATE", "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"};

    boolean isPermissionDeniedNever = false, isRationaleDialogShown = false;
    private static final int PERMISSION_REQUEST_CODE = 1;
    private AlertDialog alert11;
    private boolean onResumeCalledAlready;
    private String ON_RESUME_CALLED_PREFERENCE_KEY = "onResumeCalled";
    AppBarLayout appBarLayout;
    private SharedPreferenceActivity sharedPreferencesActivity;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            onResumeCalledAlready = savedInstanceState.getBoolean(ON_RESUME_CALLED_PREFERENCE_KEY);
        } else {
            onResumeCalledAlready = false;
        }
//        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.log_in);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {

                if (Math.abs(verticalOffset) - appBarLayout.getTotalScrollRange() == 0) {
                    toolbar.setBackgroundResource(R.drawable.gradient_background_home);


                } else {
                    toolbar.setBackgroundResource(R.drawable.border_transparent);


                }
            }
        });
        mainActivity = new MocehatCrmMainActivity();
        try {
            UserKeyDetails userKeyDetails = mainActivity.getTokenFromDb(LogInActivity.this);
            if (!userKeyDetails.IsUser) {
                db .CreateFirstEntry();
            }
        } catch (Exception ex) {

        }
        sharedPreferencesActivity = SharedPreferenceActivity.getInstance(LogInActivity.this);

        et_username = (EditText) findViewById(R.id.et_username);
        et_password = (EditText) findViewById(R.id.et_password);
        et_referral_signIn = (EditText) findViewById(R.id.et_reffaral_code_signIn);
        et_referral_signUp = (EditText) findViewById(R.id.et_reffaral_code_signup);
        showHidePasswordTV = (RobotoTextView) findViewById(R.id.showHidePassword);
        showHidePasswordImage = (ImageView) findViewById(R.id.showHidePasswordImage);
        txt_forgot_password = (RobotoTextView) findViewById(R.id.txt_forgot_password);
        signIn_btn_layout = (RelativeLayout) findViewById(R.id.signIn_btn_layout);
        signUpLayout = (RelativeLayout) findViewById(R.id.signUpLayout);
        signUp_fulllayout = (RelativeLayout) findViewById(R.id.signUp_fulllayout);
        logIn_fulllayout = (RelativeLayout) findViewById(R.id.logIn_full_layout);
        signUpLayout_signup = (RelativeLayout) findViewById(R.id.signUpLayout_signup);
//        bottomLayout = (RelativeLayout) findViewById(R.id.footerLayout);
        signUp_btn = (RobotoTextView) findViewById(R.id.signUp_btn);
        loginText = (RobotoTextView) findViewById(R.id.loginText);
        nickName = (EditText) findViewById(R.id.edit_name);
        password_signUp = (EditText) findViewById(R.id.et_password_signup);
        confirm_password = (EditText) findViewById(R.id.et_confirm_password_signup);
//        phNo = (EditText) findViewById(R.id.edit_phNo);
        username_signUp = (EditText) findViewById(R.id.et_username_signup);
        signUp_btn_layout = (RelativeLayout) findViewById(R.id.signUp_btn_layout);
        language = (RelativeLayout) findViewById(R.id.language);
        setting_url = (ImageView) findViewById(R.id.setting_url);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        deviceUniqueID = telephonyManager.getDeviceId();
        app_preference = getSharedPreferences(
                MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor edit = app_preference.edit();
        edit.putBoolean("IsLogInScreen", true);
        edit.commit();

/*
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.BookMEDS",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                //   Toast.makeText(getApplicationContext(),Base64.encodeToString(md.digest(), Base64.DEFAULT),Toast.LENGTH_LONG).show();
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/

        setting_url.setVisibility(View.GONE);
        /*setting_url.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showDialogForUrlChange();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });*/
        language.setVisibility(View.GONE);

       /* language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup();
            }
        });*/
        showHidePasswordImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String hideText = showHidePasswordTV.getText().toString().trim();
                try {
                    if (!hideText.equalsIgnoreCase("") && hideText.equalsIgnoreCase(getString(R.string.showPasswordText))) {
                        showHidePasswordTV.setText(getString(R.string.hidePasswordText));
                        et_password.setTransformationMethod(null);
                        showHidePasswordImage.setBackgroundResource(R.drawable.hide_pass_icon);
                    } else {
                        showHidePasswordTV.setText(getString(R.string.showPasswordText));
                        et_password.setTransformationMethod(new PasswordTransformationMethod().getInstance());
                        showHidePasswordImage.setBackgroundResource(R.drawable.show_pass_icon);
                    }
                    et_password.setSelection(et_password.length());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
        txt_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String recoveryLink;
                if (MocehatCrmMainActivity.WEB_API_URL.equalsIgnoreCase("http://3221-16730.el-alt.com/api/")) {
                    recoveryLink = MocehatCrmMainActivity.PasswordRecoveryLinkProd;
                } else {
                    recoveryLink = MocehatCrmMainActivity.PasswordRecoveryLinkTest;
                }
                Uri uri = Uri.parse(recoveryLink); // missing 'http://' will cause crashed
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });

        signIn_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (null != getCurrentFocus())
                    imm.hideSoftInputFromWindow(getCurrentFocus()
                            .getApplicationWindowToken(), 0);

                userName_logIn = et_username.getText().toString().trim();
                // boolean isEmailValid = isValidEmail(userName_logIn);
                if (userName_logIn.length() > 0) {
                    password_logIn = et_password.getText().toString().trim();
                    if (password_logIn.length() > 5) {
                        LoginEntity LoginEntity = new LoginEntity();
                        LoginEntity.Email = userName_logIn;
                        LoginEntity.Password = password_logIn;
                        LoginEntity.APIFor = "Login";
                        LoginEntity.DeviceType = "Android";
                        LoginEntity.LoginType = "email";

                        try {
                            LoginEntity.DeviceBrand = Build.BRAND;
                            LoginEntity.DeviceModel = Build.MODEL;
                            LoginEntity.DeviceVersion = Build.VERSION.RELEASE;
                            LoginEntity.Version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                            sharedPreferencesActivity.setCurrentAppVersion(LoginEntity.Version);
                        } catch (PackageManager.NameNotFoundException e) {
                            e.printStackTrace();
                        }
                       /* LoginEntity.ReferralCode = et_referral_signIn.getText().toString().trim();
                        LoginEntity.Latitude = Double.valueOf(app_preference.getString("Latitude", "0"));
                        LoginEntity.Longitude = Double.valueOf(app_preference.getString("Longitude", "0"));*/

                        boolean isLive = mainActivity.isInternetConnected(LogInActivity.this);
                        if (isLive) {
                            registerInBackground(LoginEntity);
                        } else {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.checkInternetCon), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        if (password_logIn.length() == 0) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterPassword), Toast.LENGTH_LONG).show();
                        } else
                            Toast.makeText(getApplicationContext(), "Password length should be greater than 5", Toast.LENGTH_LONG).show();

                        //et_password.setError(getResources().getString(R.string.enterPassword));

                        return;
                    }
                } else {
                    //et_username.setError(getResources().getString(R.string.enterValidEmail));
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterValidEmail), Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
        signUpLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logIn_fulllayout.setVisibility(View.GONE);
                //  bottomLayout.setVisibility(View.GONE);
                signUp_fulllayout.setVisibility(View.VISIBLE);

            }
        });
        signUpLayout_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signUp_fulllayout.setVisibility(View.GONE);
                logIn_fulllayout.setVisibility(View.VISIBLE);
                //  bottomLayout.setVisibility(View.GONE);

            }
        });

        signUp_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (null != getCurrentFocus())
                    imm.hideSoftInputFromWindow(getCurrentFocus()
                            .getApplicationWindowToken(), 0);

                txt_username_Signup = username_signUp.getText().toString().trim();
                txt_passwordSignUp = password_signUp.getText().toString();
                txt_confirm_password = confirm_password.getText().toString();
//                txt_phNo = phNo.getText().toString().trim();
                txt_nickName = nickName.getText().toString().trim();
                txt_passwordSignUp = password_signUp.getText().toString();
                boolean isEmailValid = isValidEmail(txt_username_Signup);

                if (txt_passwordSignUp.length() > 5 && txt_confirm_password.length() > 5 && txt_nickName.length() > 0 && /*txt_phNo.length() > 8 && */isEmailValid) {
                    if (!txt_passwordSignUp.equalsIgnoreCase(txt_confirm_password)) {
                        //confirm_password.setError("Password didn't match");
                        Toast.makeText(getApplicationContext(), "Password didn't match", Toast.LENGTH_LONG).show();
                        return;

                    }
                    LoginEntity LoginEntity = new LoginEntity();
                    LoginEntity.Email = txt_username_Signup;
                    LoginEntity.Password = txt_passwordSignUp;
//                    LoginEntity.PhoneNumber = txt_phNo;
                    LoginEntity.Name = txt_nickName;
                    LoginEntity.APIFor = "Registration";
                    LoginEntity.DeviceType = "Android";
                    LoginEntity.DeviceId = deviceUniqueID;
                    //LoginEntity.ReferralCode = et_referral_signUp.getText().toString().trim();
                    try {
                        LoginEntity.DeviceBrand = Build.BRAND;
                        LoginEntity.DeviceModel = Build.MODEL;
                        LoginEntity.DeviceVersion = Build.VERSION.RELEASE;
                        LoginEntity.Version = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
                        sharedPreferencesActivity.setCurrentAppVersion(LoginEntity.Version);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                  /*  LoginEntity.Latitude = Double.valueOf(app_preference.getString("Latitude", "0"));
                    LoginEntity.Longitude = Double.valueOf(app_preference.getString("Longitude", "0"));*/

                    boolean isLive = mainActivity.isInternetConnected(LogInActivity.this);
                    if (isLive) {
                        registerInBackground(LoginEntity);
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.checkInternetCon), Toast.LENGTH_LONG).show();

                    }
                } else {
                    if (txt_nickName.length() == 0 && !isEmailValid && txt_passwordSignUp.length() == 0 && txt_passwordSignUp.length() < 7 && txt_passwordSignUp.length() < 7) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.signuperrortext), Toast.LENGTH_LONG).show();
                    } else if (txt_nickName.length() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterName), Toast.LENGTH_LONG).show();
                    } else if (!isEmailValid) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterValidEmail), Toast.LENGTH_LONG).show();

                    }
                    /*if (txt_phNo.length() > 0) {
                        if (!(txt_phNo.length() > 8)) {
                            Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterPhoneNo), Toast.LENGTH_LONG).show();

                        }
                    } else {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterPhoneNo), Toast.LENGTH_LONG).show();

                    }*/

                    else if (txt_passwordSignUp.length() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.enterPassword), Toast.LENGTH_LONG).show();

                    } else if (txt_passwordSignUp.length() < 7) {
                        Toast.makeText(getApplicationContext(), "Password length should be greater than 5", Toast.LENGTH_LONG).show();

                    } else if (txt_confirm_password.length() == 0) {
                        Toast.makeText(getApplicationContext(), getResources().getString(R.string.confirmyourPass), Toast.LENGTH_LONG).show();

                    } else if (!txt_passwordSignUp.equalsIgnoreCase(txt_confirm_password)) {
                        //confirm_password.setError("Password didn't match");
                        Toast.makeText(getApplicationContext(), "Password didn't match", Toast.LENGTH_LONG).show();

                    }


                }








            }
        });


      /*  loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                final String userid = loginResult.getAccessToken().getAdminId();
                final String token = loginResult.getAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    if (object.has("email")) {


                                        Toast.makeText(getApplicationContext(), "Hi, " + object.getString("name"), Toast.LENGTH_LONG).show();
                                        facebookObject = new JSONObject();
                                        facebookObject = object;

                                        try {
                                            String fbId = userid;

                                            String profilePicUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                            profilePicUrl = "https://graph.facebook.com/" + fbId + "/picture?type=large";

//                                            new getProfilePic(profilePicUrl).execute();

                                            SharedPreferences.Editor edit = app_preference.edit();
                                            edit.putString("ProfilePic", profilePicUrl);
                                            edit.commit();


                                        } catch (Exception ex) {
                                            ex.printStackTrace();
                                        }
                                        LogInWithFb(userid, token, object, object.getString("email"));
                                    } else {
                                        // show_EmailDialog(object,userid,token);
                                        Toast.makeText(getApplicationContext(), "Sorry! Could not find email id from facebook please sign up with your valid email id", Toast.LENGTH_LONG).show();

                                    }
                                } catch (JSONException ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender, birthday,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }


            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });*/
    }

    /*private void LogInWithFb(String userid, String token, JSONObject object, String email) {
        try {
            LoginEntity LoginEntity = new LoginEntity();
            LoginEntity.Email = email;
            LoginEntity.APIFor = "Login";
            LoginEntity.DeviceType = "Android";
            LoginEntity.OAuthAccessToken = userid;
            LoginEntity.OAuthToken = token;
            LoginEntity.LoginType = "facebook";
            LoginEntity.Latitude = Double.valueOf(app_preference.getString("Latitude", "0"));
            LoginEntity.Longitude = Double.valueOf(app_preference.getString("Longitude", "0"));

            boolean isLive = mainActivity.isInternetConnected(LogInActivity_new.this);
            if (isLive) {
                if (LoginEntity.Email.length() > 0) {
                    registerInBackground(LoginEntity);
                } else {
                    Toast.makeText(getApplicationContext(), "Email id not available in facebook please sign up with your valid email id", Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.checkInternetCon), Toast.LENGTH_LONG).show();

            }
        } catch (Exception e) {

        }

    }*/

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }*/

    private void registerInBackground(final LoginEntity LoginEntity) {
        gcm = GoogleCloudMessaging.getInstance(LogInActivity.this);
        new AsyncTask<Void, Void, String>() {
            protected void onPreExecute() {
                alert = ProgressDialog.show(LogInActivity.this, null, "Loading...");
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(LogInActivity.this);
                    }
                    // ZibMi account gcm project
                    // regId = gcm.register("411781073371");
                    // Zibew account staging gcm project
                    // HealthBox project number
                    regId = gcm.register("1052719788270");
                    deviceID = regId;
                    Log.d("RegisterActivity", "registerInBackground - regId: "
                            + regId);
                    msg = "Device registered, registration ID=" + regId;

                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    Log.d("RegisterActivity", "Error: " + msg);
                }
                Log.d("RegisterActivity", "AsyncTask completed: " + msg);
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                try {
                    app_preference = getSharedPreferences(
                            MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = app_preference.edit();
                    edit.putBoolean("AppReinstall", true);
                    edit.putBoolean("IsLogInScreen", false);
                    if (LoginEntity.LoginType != null) {
                        if (LoginEntity.LoginType.equalsIgnoreCase("email")) {
                            edit.putString("LoginType", "email");
                        } else {
                            edit.putString("LoginType", "facebook");

                        }

                    } else {
                        edit.putString("LoginType", "email");

                    }
                    edit.commit();

                    LoginEntity.DeviceId = deviceID;
                    LoginEntity.DeviceUniqueId = deviceUniqueID;
                    Registration registration = new Registration(LogInActivity.this, LoginEntity);
                    String result = registration.execute().get();
                    if (result != null && !result.equalsIgnoreCase("")) {
                        try {


                            Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                            LoginEntity response = new LoginEntity();

                            response = gson.fromJson(result,
                                    LoginEntity.class);
                            if (LoginEntity.APIFor.equalsIgnoreCase("Login")) {
                                if (!response.CustomerId.equalsIgnoreCase("0")) {
                                     sharedPreferencesActivity = SharedPreferenceActivity.getInstance(LogInActivity.this);
                                    sharedPreferencesActivity.setUpdateDb(MocehatCrmMainActivity.dbVersionNumber);
                                  //  mainActivity.fireBaseLogEvent(LogInActivity_new.this, "log_in");
                                    response.Email = LoginEntity.Email;
                                    response.Password = LoginEntity.Password;
                                    response.DeviceType = LoginEntity.DeviceType;
                                    response.DeviceId = LoginEntity.DeviceId;
                                    //response.PhoneNumber=LoginEntity.PhoneNumber;
                               /*     try {
                                        if (facebookObject != null && LoginEntity.LoginType.equalsIgnoreCase("facebook")) {
                                            mainActivity.fireBaseLogEvent(LogInActivity_new.this, "facebook_login");
                                            response.Name = facebookObject.getString("name");
                                            response.Gender = facebookObject.getString("gender");
                                            if (facebookObject.has("birthday")) {
                                                response.Dob = facebookObject.getString("birthday");
                                            }
                                        }
                                    } catch (Exception ex) {

                                    }*/


                                    addToken(deviceUniqueID, response);
                                    if (LoginEntity.LoginType.equalsIgnoreCase("email")) {
                                        getProfileData(response);
                                    } else if (LoginEntity.LoginType.equalsIgnoreCase("facebook")) {

                                        getProfileData(response);

                                    } else {
                                       /* if (response.IsPhoneVerified) {
                                            edit.putBoolean("IsVerifiedPhoneNumber", true);
                                            edit.commit();*/
                                            startActivity(new Intent(LogInActivity.this,
                                                    HomeScreen.class)
                                                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                        /*} else {
                                            Intent intent = new Intent(LogInActivity_new.this, FirebaseVerifyNumber.class);
                                            startActivity(intent);
                                        }*/
                                    }


                                    //intent to homeScreen
                                } else {
                                    String errorMsg = null;
                                    if (!StringUtils.isBlank(response.Message)) {
                                        errorMsg = response.Message;
                                    }
                                    et_password.setText("");
                                    // et_password.setError("Wrong email id or password");
                                    Toast.makeText(getApplicationContext(), errorMsg, Toast.LENGTH_LONG).show();
                                    alert.dismiss();
                                    return;
                                }
                            } else if (LoginEntity.APIFor.equalsIgnoreCase("Registration")) {
                                if (response.Status.equalsIgnoreCase("Success")) {

                                  //  mainActivity.fireBaseLogEvent(LogInActivity_new.this, "Registration");

                                    response.Email = LoginEntity.Email;
                                    response.Password = LoginEntity.Password;
//                                    response.PhoneNumber = LoginEntity.PhoneNumber;
                                    response.Name = LoginEntity.Name;
                                    response.DeviceType = LoginEntity.DeviceType;
                                    response.DeviceId = LoginEntity.DeviceId;
                                    addToken(deviceUniqueID, response);


                                   // if (response.IsPhoneVerified) {
                                       /* edit.putBoolean("IsVerifiedPhoneNumber", true);
                                        edit.commit();*/
                                        startActivity(new Intent(LogInActivity.this,
                                                HomeScreen.class)
                                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                   /* } else {
                                        Intent intent = new Intent(LogInActivity_new.this, FirebaseVerifyNumber.class);
                                        startActivity(intent);
                                    }
*/
                                } else {
                                    Toast.makeText(getApplicationContext(), "You are already registered with this email id", Toast.LENGTH_LONG).show();
                                    alert.dismiss();
                                }
                            }


                        } catch (Exception ex) {
                            alert.dismiss();
                            showToastMsg(result);
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "!Oh sorry.. Something went wrong please try again", Toast.LENGTH_LONG).show();

                        alert.dismiss();
                    }


                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);
    }

    private void showToastMsg(String response) {
        Toast.makeText(getApplicationContext(), "Oops.. Time out! It's not you, its us.. Please give another try?", Toast.LENGTH_LONG).show();
    }

    private void getProfileData(LoginEntity response) {
        LoginEntity profileentity = new LoginEntity();
        profileentity.CustomerId = response.CustomerId;
        profileentity.APIFor = "GetDetails";
        profileentity.PasswordSalt = response.PasswordSalt;
        String loginType = app_preference.getString("LoginType", "email");
        profileentity.LoginType = loginType;
        alert.setMessage(getResources().getString(R.string.gettingProfileDetails));
        try {
            Registration getProfileDetails = new Registration(LogInActivity.this, profileentity);
            String result = getProfileDetails.execute().get();
            if (!result.equalsIgnoreCase("") && result != null) {
                Gson gson = new GsonBuilder().disableHtmlEscaping().create();
                LoginEntity profileData = new LoginEntity();
                profileData = gson.fromJson(result,
                        LoginEntity.class);
                UserKeyDetails updatedDetails = new UserKeyDetails();
                updatedDetails.setUserid(response.CustomerId);
                updatedDetails.setPasswordSalt(response.PasswordSalt);
                if (profileData.Name != null) {
                    updatedDetails.setNickname(profileData.Name);
                } else {
                    SharedPreferences.Editor edit = app_preference.edit();
                    edit.putBoolean("CreateWelcomeCard", true);
                    edit.commit();
                }
                if (profileData.PhoneNumber != null && !profileData.PhoneNumber.equalsIgnoreCase("admin")) {
                    updatedDetails.setPhoneNumber(profileData.PhoneNumber);
                }
                updatedDetails.setBloodGroup(profileData.BloodGroup);
                updatedDetails.setBmiheight(profileData.Height);
                updatedDetails.setBmiweight(profileData.Weight);
                updatedDetails.setDob(profileData.Dob);
                updatedDetails.setGender(profileData.Gender);
                updatedDetails.setprofilePicture(profileData.ProfilePicture);
               /* try {
                    if (profileData.BloodPressure != null && profileData.BloodPressure.contains("^")) {
                        String[] bloodPressure = profileData.BloodPressure.split("\\^");
                        String systolic = bloodPressure[0];
                        String diastolic = bloodPressure[1];
                        updatedDetails.setSystolic(systolic);
                        updatedDetails.setDyastolic(diastolic);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/


                db.addUpdateUserDetails(updatedDetails);
              /*  if (profileData.IsPhoneVerified) {
                    app_preference = getSharedPreferences(
                            MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor edit = app_preference.edit();
                    edit.putBoolean("IsVerifiedPhoneNumber", true);
                    edit.commit();*/
                    startActivity(new Intent(LogInActivity.this,
                            HomeScreen.class)
                            .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
              /*  } else {
                    Intent intent = new Intent(LogInActivity_new.this, FirebaseVerifyNumber.class);
                    startActivity(intent);
                }*/
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    private void addToken(String deviceuniqueid, LoginEntity response) {
        db = new MocehatCrmDBHelper(this);
        UserKeyDetails tkn = new UserKeyDetails();
        tkn.setDeviceid(deviceID);
        tkn.setDeviceuniqueid(deviceuniqueid);
        tkn.setUserid(response.CustomerId);
        if (response.Name != null) {
            tkn.setNickname(response.Name);
        }
        if (response.PhoneNumber != null) {
            tkn.setPhoneNumber(response.PhoneNumber);
        }
        tkn.setSharedsecretkey("");
        tkn.setemailadress(response.Email);
        tkn.setPasswordSalt(response.PasswordSalt);
       /* if (response.Gender != null) {
            tkn.setGender(response.Gender);
        }
        if (response.Dob != null) {
            tkn.setDob(response.Dob);
        }*/
        db.addUpdateUserDetails(tkn);
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target)
                && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                .matches();
    }

    private class getProfilePic extends AsyncTask {
        String url;

        public getProfilePic(String profilePicUrl) {
            url = profilePicUrl;
        }

        @Override
        protected Object doInBackground(Object[] params) {

            URL fb_url = null;//small | noraml | large
            try {
                fb_url = new URL(url);
                HttpsURLConnection conn1 = (HttpsURLConnection) fb_url.openConnection();
                HttpsURLConnection.setFollowRedirects(true);
                conn1.setInstanceFollowRedirects(true);
                Bitmap fb_img = BitmapFactory.decodeStream(conn1.getInputStream());
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                fb_img.compress(Bitmap.CompressFormat.JPEG, 100,
                        stream);
                byte[] imageInByte = stream.toByteArray();
                String base64String = Base64.encodeToString(imageInByte, Base64.DEFAULT);

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

    }






    private void refreshResource() {

        startActivity(new Intent(LogInActivity.this, LoginEntity.class));
      /*  loginText.setText(getResources().getString(R.string.logInHeaderText));

        nickName.setHint(getResources().getString(R.string.name));
        password_signUp.setHint(getResources().getString(R.string.password));
        confirm_password.setHint(getResources().getString(R.string.confirmPass));
     //   phNo.setHint(getResources().getString(R.string.phNo));
        username_signUp.setHint(getResources().getString(R.string.email));
        et_referral_signIn.setHint(getResources().getString(R.string.Reffaral_code));
        et_referral_signUp.setHint(getResources().getString(R.string.Reffaral_code));

        et_username.setHint(getResources().getString(R.string.emailOrmobile));
        et_password.setHint(getResources().getString(R.string.password));


        txt_forgot_password.setText(getResources().getString(R.string.forgotpassword));

        signUp_btn.setText(getResources().getString(R.string.signUp));
        ((RobotoTextView) findViewById(R.id.existingUser)).setText(getResources().getString(R.string.existingUser));
        ((RobotoTextView) findViewById(R.id.signIn)).setText(getResources().getString(R.string.signIn));
        ((RobotoTextView) findViewById(R.id.newToMoCehat)).setText(getResources().getString(R.string.newToMoCehat));
        ((RobotoTextView) findViewById(R.id.txt_or)).setText(getResources().getString(R.string.or));
        ((TextInputLayout) findViewById(R.id.phone_no_signUp)).setHint(getResources().getString(R.string.phNo));*/


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (alert != null && alert.isShowing()) {
            alert.dismiss();
        }
    }

   /* public void showDialogForUrlChange() {
        try {
            final Dialog dialog = new Dialog(LogInActivity_new.this);
            dialog.setContentView(R.layout.dialog_url_change);
            dialog.setTitle("Update url");
            final EditText et_url = (EditText) dialog.findViewById(R.id.urlText);
            et_url.setText(getURL());
            et_url.setSelection(et_url.length());
            final Button submit = (Button) dialog.findViewById(R.id.submit_button);
            submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        String updatedUrl = et_url.getText().toString();
                        setURL(updatedUrl);
                        dialog.dismiss();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        //dialog.dismiss();
                    }
                }
            });
            dialog.setCanceledOnTouchOutside(true);
            dialog.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    private void requestPermission() {
        try {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission.READ_PHONE_STATE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,

                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    ) {
                isRationaleDialogShown = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            } else {
                isPermissionDeniedNever = true;
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {
                    boolean isPhoneStateDone = false,
                            isAccessFineDone = false, isAccessCoarseDone = false,
                            isShowRationalePhoneState = false,
                            isShowRationaleAccessFine = false, isShowRationaleAccessCoarse = false;
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(android.Manifest.permission.READ_PHONE_STATE)) {
                            isShowRationalePhoneState = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isPhoneStateDone = true;
                            } else {
                                isPhoneStateDone = false;
                            }
                        } else if (permission.equals(android.Manifest.permission.ACCESS_FINE_LOCATION)) {
                            isShowRationaleAccessFine = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isAccessFineDone = true;
                            } else {
                                isAccessFineDone = false;
                            }
                        } else if (permission.equals(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
                            isShowRationaleAccessCoarse = ActivityCompat.shouldShowRequestPermissionRationale(this, permission);
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                isAccessCoarseDone = true;
                            } else {
                                isAccessCoarseDone = false;
                            }
                        }
                    }

                    boolean isRationale = (isShowRationalePhoneState || isShowRationaleAccessFine ||
                            isShowRationaleAccessCoarse);
                    if (!isRationale && (!isPhoneStateDone
                            || !isAccessFineDone || !isAccessCoarseDone)) {
                        showSettingsDialog();
                    } else {
                        refreshResource();
                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(LogInActivity.this);

        builder1.setMessage(getResources().getString(R.string.permissionStatement));

        builder1.setPositiveButton(getResources().getString(R.string.settings), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


                try {
                    marshmallowSetting();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                alert11.dismiss();

            }
        });

        alert11 = builder1.create();
        alert11.show();
        alert11.getButton(alert11.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));
        alert11.getButton(alert11.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.AppThemeColor));
    }

    private void marshmallowSetting() {
        try {
            Intent intent = new Intent();
            intent.setAction(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
            Uri uri = Uri.fromParts("package", getPackageName(), null);
            intent.setData(uri);
            startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!onResumeCalledAlready) {
            onResumeCalledAlready = true;
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE)
                    != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermission();

            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Save current onResumeCalledAlready state
        outState.putBoolean(ON_RESUME_CALLED_PREFERENCE_KEY, onResumeCalledAlready);
        super.onSaveInstanceState(outState);
    }
}

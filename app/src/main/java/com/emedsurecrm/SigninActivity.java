package com.emedsurecrm;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.model.LoginEntity;
import com.emedsurecrm.model.SharedPreferenceActivity;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Locale;


public class SigninActivity extends AppCompatActivity {
    Button signin_btn_layout;
    EditText tv_username, tv_password;
    RelativeLayout fullLayout;
    String name = null, password = null;
    boolean isLive = false, isFbLogin = false;
    MocehatCrmMainActivity mainActivity;
    boolean isSignIn = false;

    SharedPreferenceActivity sharedPreferenceActivity;
    GoogleCloudMessaging gcm;
    String regId;
    public static String[] PERMISSIONS_PHONESTATE = {"android.permission.READ_PHONE_STATE"};
    public static final int PERMISSIONS_PHONESTATE_REQUEST_CODE = 2;
    private String deviceToken = "";
    String deviceUniqueID = null, deviceName = null, deviceVersion = null, ApplicationName = null, version = null, language = null;
    Gson gson = new Gson();
    View username_view, password_view;
    SharedPreferences sharedPreferences;
    String My_PREFRERENCE = null;

    RelativeLayout parentlayout;
    boolean SigninClicked;
    View mLoginFormView;
    View mProgressView;
    RobotoTextView showHidePasswordTV;
    ImageView showHidePasswordImage;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().setTitle("Sign In");

        mainActivity = new MocehatCrmMainActivity();
        parentlayout = (RelativeLayout) findViewById(R.id.parentlayout);

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
       /* showHidePasswordImage = (ImageView) findViewById(R.id.showHidePasswordImage);*/

      /*  showHidePasswordTV = (RobotoTextView) findViewById(R.id.showHidePassword);
        showHidePasswordImage = (ImageView) findViewById(R.id.showHidePasswordImage);*/
       /* showHidePasswordImage.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String hideText = showHidePasswordTV.getText().toString().trim();
                try {
                    if (!hideText.equalsIgnoreCase("") && hideText.equalsIgnoreCase(getString(R.string.showPasswordText))) {
                        showHidePasswordTV.setText(getString(R.string.hidePasswordText));
                        tv_password.setTransformationMethod(null);
                        showHidePasswordImage.setBackgroundResource(R.drawable.hide_pass_icon);
                    } else {
                        showHidePasswordTV.setText(getString(R.string.showPasswordText));
                        tv_password.setTransformationMethod(new PasswordTransformationMethod().getInstance());
                        showHidePasswordImage.setBackgroundResource(R.drawable.show_pass_icon);
                    }
                    tv_password.setSelection(tv_password.length());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });*/
        sharedPreferences = getSharedPreferences(My_PREFRERENCE, Context.MODE_PRIVATE);
        sharedPreferenceActivity = SharedPreferenceActivity.getInstance(this);
        /* String localelanguage= Resources.getSystem().getConfiguration().locale.getLanguage();
        language  = localelanguage.toString();*/
        language = Locale.getDefault().getLanguage();

        isLive = mainActivity.isInternetConnected(this);
        tv_username = (EditText) findViewById(R.id.tv_username);
        tv_password = (EditText) findViewById(R.id.tv_password);
        username_view = (View) findViewById(R.id.username_view);
        password_view = (View) findViewById(R.id.password_view);
        fullLayout = (RelativeLayout) findViewById(R.id.fullLayout);

        if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPhonStatePermission();
        }
        //fortextstyle//
        //  forgot_password = (TextView) findViewById(R.id.forgot_password);
        //  signin_text = (TextView) findViewById(R.id.signin_text);


        Typeface brandon_bld = Typeface.createFromAsset(getAssets(), "fonts/Brandon_bld.otf");
        Typeface brandon_regular = Typeface.createFromAsset(getAssets(), "fonts/Brandon_reg.otf");

        //  forgot_password.setTypeface(brandon_regular);
        // signin_text.setTypeface(brandon_regular);

        tv_username.setTypeface(brandon_regular);
        tv_password.setTypeface(brandon_regular);

        signin_btn_layout = (Button) findViewById(R.id.signin_btn_layout);


        String username = sharedPreferences.getString("UserName", "");
        tv_username.setText(username);
        if (!username.equalsIgnoreCase("")) {
            tv_password.requestFocus();
        }

        //show_error_popup();
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        try {
            deviceUniqueID = telephonyManager.getDeviceId();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        version = pInfo.versionName;
        deviceName = getDeviceName();
        deviceVersion = getAndroidVersion();
        ApplicationName = getApplicationName(getApplicationContext());

        signin_btn_layout.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               /* Intent intent = new Intent(SigninActivity.this, HomeScreen.class);
                startActivity(intent);*/
                View view;
                try {
                    SigninClicked = true;
                    if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(SigninActivity.this, Manifest.permission.READ_PHONE_STATE)
                            != PackageManager.PERMISSION_GRANTED) {

                        requestPhonStatePermission();
                    } else {

                        name = tv_username.getText().toString().trim();
                        password = tv_password.getText().toString().trim();
                        isLive = mainActivity.isInternetConnected(SigninActivity.this);
                        hideKeyBoard();
                        attemptLogin();

                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        });
       /* signin_btn_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(SigninActivity.this, ForgotPasswordActivity.class);
                Bundle bndlanimation = ActivityOptions
                        .makeCustomAnimation(
                                getApplicationContext(),
                                R.anim.next_swipe2,
                                R.anim.next_swipe1).toBundle();
                startActivity(intent, bndlanimation);
            }
        });*/
      /*  forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //new ForgotPassword().execute();
              *//*  Intent intent = new Intent(SigninActivity.this, ForgotPasswordActivity.class);
                Bundle bndlanimation = ActivityOptions
                        .makeCustomAnimation(
                                getApplicationContext(),
                                R.anim.next_swipe1,
                                R.anim.next_swipe2).toBundle();
                startActivity(intent, bndlanimation);*//*
            }
        });*/

        //new GetCustomerInfo().execute();
    }

    /*private void show_error_popup() {

        popupLayout.setVisibility(View.VISIBLE);


    }*/
    public void hideKeyBoard(){
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void registerInBackground() {
        gcm = GoogleCloudMessaging.getInstance(SigninActivity.this);
        new AsyncTask<Void, Void, String>() {
            ProgressDialog dialog;

            @Override
            protected void onPreExecute() {
                try {
                   /* dialog = new ProgressDialog(SigninActivity.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                    dialog.setMessage(getString(R.string.dialogPleaseWait));
                    dialog.setIndeterminate(false);
                    dialog.setCancelable(false);
                    dialog.show();*/
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging
                                .getInstance(SigninActivity.this);
                    }
                    regId = gcm.register("1052719788270");
                    deviceToken = regId;
                   /* deviceToken = FirebaseInstanceId.getInstance().getToken();*/
                    sharedPreferenceActivity.setDeviceGuid(deviceToken);
                    sharedPreferenceActivity.SaveDeviceId(deviceToken);
                    Log.d("RegisterActivity", "registerInBackground - regId: "
                            + regId);
                    msg = "Device registered, registration ID=" + regId;

                } catch (Exception ex) {
                    msg = "Error :" + ex.getMessage();
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                try {

                    new SignIn().execute();

                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }
        }.execute(null, null, null);
    }

    private class SignIn extends AsyncTask<String, String, String> {
        LoginEntity productEntity = new LoginEntity();
        String json;
        String query;

        protected void onPreExecute() {
            LoginEntity loginEntity = new LoginEntity();
            loginEntity.Email = name;
            loginEntity.Password = password;
            loginEntity.DeviceId = deviceToken;
            loginEntity.DeviceUniqueId = deviceUniqueID;
            loginEntity.DeviceType = "Android";
            gson = new GsonBuilder().disableHtmlEscaping().create();
            json = gson.toJson(loginEntity);
            super.onPreExecute();
        }

        protected String doInBackground(String... param) {
            String result = "";
            URL url;
            try {
                url = new URL(MocehatCrmMainActivity.WEB_API_URL
                        + "chatapplogin");
                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                // writer.write(getPostData(postDataParams));
                writer.write(json);
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }
            } catch (Exception e) {
                Log.d("GetPinCode", "Error:" + e.getMessage());
                e.printStackTrace();

            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (!TextUtils.isEmpty(result) && result.contains("CustomerId")) {
                    LoginEntity loginEntity;
                    loginEntity = gson.fromJson(result, LoginEntity.class);
                    if (!StringUtils.isBlank(loginEntity.CustomerId)&&!loginEntity.CustomerId.equalsIgnoreCase("0")) {
                       // loginEntity.CustomerId = "1";
                        sharedPreferenceActivity.UserInfo(loginEntity);
                        Intent intent = new Intent(SigninActivity.this, HomeScreen.class);
                        Bundle bndlanimation = ActivityOptions
                                .makeCustomAnimation(
                                        getApplicationContext(),
                                        R.anim.next_swipe1,
                                        R.anim.next_swipe2).toBundle();
                        startActivity(intent, bndlanimation);
                        startActivity(intent);
                        finish();
                    } else {
                        Toast.makeText(SigninActivity.this, "LogIn failed! Please try again.", Toast.LENGTH_SHORT).show();

                      //  mainActivity.show_snakbar("LogIn failed! Please trt again.", parentlayout);
                        showProgress(false);
                    }
                } else {
                    //show_error_popup();
                    username_view.setBackgroundColor(getResources().getColor(R.color.red));
                    password_view.setBackgroundColor(getResources().getColor(R.color.red));
                    tv_username.setText("");
                    tv_password.setText("");
                    showProgress(false);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                showProgress(false);
            }


        }
    }


    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release + ")";
    }

    public static String getApplicationName(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : context.getString(stringId);
    }

    public void requestPhonStatePermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_PHONE_STATE)) {
                //	Toast.makeText(IntroductionScreen.this,getResources().getString(R.string.permissionStatement), Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, PERMISSIONS_PHONESTATE, PERMISSIONS_PHONESTATE_REQUEST_CODE);
            } else {
                // Contact permissions have not been granted yet. Request them directly.
                ActivityCompat.requestPermissions(this, PERMISSIONS_PHONESTATE, PERMISSIONS_PHONESTATE_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MocehatCrmMainActivity.PERMISSIONS_PHONESTATE_REQUEST_CODE:
                try {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {

                                View view;
                                try {
                                    try {
                                        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                                        deviceUniqueID = telephonyManager.getDeviceId();
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }

                                    name = tv_username.getText().toString().trim();
                                    password = tv_password.getText().toString().trim();
                                    isLive = mainActivity.isInternetConnected(SigninActivity.this);
                                    attemptLogin();


                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                SigninClicked = false;
                            } else {
                                Toast.makeText(SigninActivity.this, "Permission denied ", Toast.LENGTH_LONG).show();
                            }
                        }


                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void attemptLogin() {
       /* if (mAuthTask != null) {
            return;
        }*/

        // Reset errors.
        tv_username.setError(null);
        tv_password.setError(null);

        // Store values at the time of the login attempt.
        String email = tv_username.getText().toString();
        String password = tv_password.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            tv_password.setError(getString(R.string.error_invalid_password));
            focusView = tv_password;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            tv_username.setError(getString(R.string.error_field_required));
            focusView = tv_username;
            cancel = true;
        } else if (!isEmailValid(email)) {
            tv_username.setError(getString(R.string.error_invalid_email));
            focusView = tv_username;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            View view;
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("UserName", name);
            edit.commit();
            view = getCurrentFocus();
            mainActivity.hideSoftKeyboard(SigninActivity.this, view);
            isSignIn = true;

            if (mainActivity.isInternetConnected(SigninActivity.this))
                registerInBackground();
            else {
                Toast.makeText(SigninActivity.this, getString(R.string.check_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

}

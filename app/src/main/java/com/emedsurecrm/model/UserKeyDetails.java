package com.emedsurecrm.model;

public class UserKeyDetails {

    private String time;
    private String deviceid;
    private String deviceuniqueid;
    private String sharedsecretkey;
    private String phonenumber;
    private String emailadress;
    public boolean IsemailadressUpdated;
    private String nickname;
    public boolean IsnicknameUpdated;
    private String profileadress;
    public boolean IsprofileadressUpdated;
    private String profilestate;
    public boolean IsprofilestateUpdated;
    private String profilecity;
    public boolean IsprofilecityUpdated;
    private String userid;
    private String profilePicture;
    public boolean IsprofilePictureUpdated;
    private String country;
    public boolean IscountryUpdated;
    private String pincode;
    public boolean IspincodeUpdated;
    private String landmark;
    public boolean IslandmarkUpdated;
    private String emergencycontact;
    public boolean IsemergencycontactUpdated;
    private String bloodPressure;
    public boolean IsbloodPressureUpdated;
    public boolean IsweightUpdated;
    private String bloodGroup;
    public boolean IsbloodGroupUpdated;
    private String personalStatus;
    private String alarmCount;
    private String systolic;
    ;
    public boolean IssystolicUpdated;
    private String dyastolic;
    public boolean IsdyastolicUpdated;
    private String bmiheight;
    ;
    public boolean IsbmiheightUpdated;
    private String bmiweight;
    public boolean IsbmiweightUpdated;
    private String passwordSalt;
    private String latitude;
    private String longitude;
    private String Dob;
    private String Gender;
    public boolean IsGenderUpdated;
    public boolean IsdobUpdated;
    public boolean IsUser;
    public boolean IsPhoneVerified;

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getDeviceuniqueid() {
        return deviceuniqueid;
    }

    public void setDeviceuniqueid(String deviceuniqueid) {
        this.deviceuniqueid = deviceuniqueid;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSharedsecretkey() {
        return sharedsecretkey;
    }

    public void setSharedsecretkey(String sharedsecretkey) {
        this.sharedsecretkey = sharedsecretkey;
    }

    public String getPhoneNumber() {
        return phonenumber;
    }

    public void setPhoneNumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPinCode() {
        return pincode;
    }

    public void setPinCode(String pincode) {
        this.pincode = pincode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
    public String getprofilePicture() {
        return profilePicture;
    }

    public void setprofilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }

    public String getpersonalStatus() {
        return personalStatus;
    }

    public void setpersonalStatus(String personalStatus) {
        this.personalStatus = personalStatus;
    }

    public String getemailadress() {
        return emailadress;
    }

    public void setemailadress(String emailadress) {
        this.emailadress = emailadress;
    }

    public String getprofileadress() {
        return profileadress;
    }

    public void setprofileadress(String profileadress) {
        this.profileadress = profileadress;
    }

    public String getprofilecity() {
        return profilecity;
    }

    public void setprofilecity(String profilecity) {
        this.profilecity = profilecity;
    }

    public String getprofilestate() {
        return profilestate;
    }

    public void setprofilestate(String profilestate) {
        this.profilestate = profilestate;
    }


    public String getBloodPressure() {
        return bloodPressure;
    }

    public String getSystolic() {
        return systolic;
    }

    public String getDyastolic() {
        return dyastolic;
    }

    public String getBmiheight() {
        return bmiheight;
    }

    public String getBmiweight() {
        return bmiweight;
    }


    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }
    public void setSystolic(String systolic) {
        this.systolic = systolic;
    }
    public void setDyastolic(String dyastolic) {
        this.dyastolic = dyastolic;
    }

    public void setBmiheight(String bmiheight) {
        this.bmiheight = bmiheight;
    }
    public void setBmiweight(String bmiweight) {
        this.bmiweight = bmiweight;
    }


    public String getDob() {
        return Dob;
    }

    public void setDob(String dob) {
        this.Dob = dob;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        this.Gender = gender;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAlarmCount() {
        return alarmCount;
    }

    public void setAlarmCount(String AlarmCount) {
        this.alarmCount = AlarmCount;
    }

    public String getPasswordSalt() {
        return passwordSalt;
    }
    public void setPasswordSalt(String password) {
        this.passwordSalt = password;
    }

}
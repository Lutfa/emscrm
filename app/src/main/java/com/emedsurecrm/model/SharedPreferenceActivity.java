package com.emedsurecrm.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.emedsurecrm.R;


public class SharedPreferenceActivity {
    public static SharedPreferenceActivity instance;

    private static final String TAG = "LoginCredentials";
    SharedPreferences sharedPreferences;

    public static synchronized SharedPreferenceActivity getInstance(Context context) {
        if (instance == null)
            instance = new SharedPreferenceActivity(context);
        return instance;
    }

    public SharedPreferenceActivity(Context context) {
        sharedPreferences = context.getSharedPreferences(context.getResources().getString(R.string.shared_prefs_credentials), Context.MODE_PRIVATE);
    }
    public boolean UserInfo(LoginEntity loginEntity) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Email", loginEntity.Email);
        editor.putString("Name", loginEntity.Name);
        editor.putString("PasswordSalt",loginEntity.PasswordSalt);
        editor.putString("CustomerId",loginEntity.CustomerId);
        editor.putString("AdminId","1");
        editor.apply();
        return true;
    }

    public void setToken(String token) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("Token", token);
        editor.apply();
    }

    public void setDeviceGuid(String deviceGuid) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DeviceGuid", deviceGuid);
        editor.apply();
    }

    public String getDeviceGuid() {
        return sharedPreferences.getString("DeviceGuid","");
    }



    public void SaveDeviceId(String deviceID) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DeviceID", deviceID);
        editor.apply();
    }

    public String getAdminId() {
        return sharedPreferences.getString("AdminId","");
    }
    public String getCustomerId() {
        return sharedPreferences.getString("CustomerId","");
    }
    public String getPasswordSalt() {
        return sharedPreferences.getString("PasswordSalt","");
    }

    public String getToken() {
        return sharedPreferences.getString("Token", "");
    }
    public String getName() {
        return sharedPreferences.getString("Name", "");
    }

    public int getLocationId() {
        return sharedPreferences.getInt("LocationId",0);
    }

    public String getLocationName() {
        return sharedPreferences.getString("LocationName", "");
    }

    public int getDistributorId() {
        return sharedPreferences.getInt("DistributorId", 0);
    }

    public String getDistributorName() {
        return sharedPreferences.getString("DistributorName", "");
    }
    public String getLocationLogoUrl() {
        return sharedPreferences.getString("LocationLogoUrl", "");
    }

    public void setDistributerPhone(String distributorPhone) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("DistributerPhone", distributorPhone);
        editor.apply();
    }
    public String getDistributerPhone() {
        return sharedPreferences.getString("DistributerPhone", "");
    }

    public void setLocationLogoUrl(String LocationLogoUrl) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LocationLogoUrl", LocationLogoUrl);
        editor.apply();
    }

    public void setPopulateScreenVisible(boolean showPopulateScreen) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean("ShowPopulateScreen", showPopulateScreen);
        editor.apply();
    }
    public boolean getPopulateScreenVisible() {
        return sharedPreferences.getBoolean("ShowPopulateScreen", true);
    }

    public void setCustomerListUpdateTime(long cartAddedTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("CustomerListUpdateTime", cartAddedTime);
        editor.apply();
    }
    public long getCustomerListUpdateTime() {
        return sharedPreferences.getLong("CustomerListUpdateTime", 0);
    }
    public void setPharmacyListUpdateTime(long cartAddedTime) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putLong("PharmacyListUpdateTime", cartAddedTime);
        editor.apply();
    }
    public long getPharmacyListUpdateTime() {
        return sharedPreferences.getLong("PharmacyListUpdateTime", 0);
    }
    public void setLoginAppVersion(String appVersion) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("LoginAppVersion", appVersion);
        editor.apply();
    }
    public String getLoginAppVersion() {

        return sharedPreferences.getString("LoginAppVersion", "");
    }
    public void setCurrentAppVersion(String appVersion) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("CurrentAppVersion", appVersion);
        editor.apply();
    }
    public String getCurrentAppVersion() {

        return sharedPreferences.getString("CurrentAppVersion", "");
    }
    public void setUpdateDb(int updateDb) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("UpdateDb", updateDb);
        editor.apply();
    }
    public int getUpdateDb() {


        return sharedPreferences.getInt("UpdateDb",0);
    }
}

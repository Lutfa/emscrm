package com.emedsurecrm.model;

import java.util.ArrayList;

/**
 * Created by PCCS on 10/13/2017.
 */

public class MyCustomerEntity {
    public String CustomerId;
    public String CustomerName;
    public String LastMessage;
    public String LastActivityTime;
    public int MessageCount;
    public String PharmacyId;
    public String Passwordsalt;
    public String PharmacyName;
    public ArrayList<MyCustomerEntity> chatlist;
    public long LastUpdatedTimeTicks;
}

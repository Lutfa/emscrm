package com.emedsurecrm.model;

import java.util.List;
public class ChatMessageEntity {

    //Form sending chats
   // public int Id;
    public String SenderId;
    public String ReceiverId;
    public String StoreId;
    public String MessageBody;
    public String UserType;
    public String Passwordsalt;
    public String PasswordSalt;
    public String APIFor;
    public int ActivityId;
    public String MessageType;
    public String TimeStamp;

    //for sending and getting image
    public int PictureId;
    public int MediaId;
    public String ChatPictureBinary;
    public String SeoFilename;
    public String MimeType;
    public String ThumbNailBinary;
    public String MessageTime;
    public String SenderName;
    public String PictureBinary;
    public String PictureType;
    public String LoginType;
    public String MediaPath;
    public boolean IsPresistanceRequired;
    public int ParentMessageId;
    public String Id;
    public String ImagePath;
    public String CreatedOnUtc;
    public String Option1Value;

    public int MediaUploadStatus;
    public int chatItemPosition;
    public String MessageTimeInLong;
    public String MessageTimeInDate;
    public  String ActivityGuid;
    public boolean IsNotificationMessageRead;
    public String Option1Name;
    public List<ChatMessageEntity> chatList;
    public List<ChatMessageEntity> ReceiveChats;
    public  String CurrentTimeStampOnUtc;

    ///Notification//
    public String Option2Value;
    public String CurrentTimeStamp;
    public String SenderDeviceGuid;
}
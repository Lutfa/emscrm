package com.emedsurecrm.model;

/**
 * Created by ahmed on 9/15/2017.
 */

public class LoginEntity {
    public String Email;
    public String Name;
    public String PasswordSalt;
    public String Password;
    public String DeviceId;
    public String DeviceType;
    public String DeviceUniqueId;
    public String CustomerId;

    public String APIFor;
    public String LoginType;
    public String Version;
    public String DeviceModel;
    public String DeviceBrand;
    public String DeviceVersion;
    public String PhoneNumber;

    public String Message;
    public String Status;
    public String Gender;
    public String ProfilePicture;
    public String Dob;
    public String Weight;
    public String Height;
    public String BloodGroup;
}

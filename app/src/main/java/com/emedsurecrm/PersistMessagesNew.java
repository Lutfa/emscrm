package com.emedsurecrm;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;



import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by PCCS on 8/3/2017.
 */


public abstract class PersistMessagesNew extends AsyncTask<String, String, String> {
    StringBuilder s;

    String messageJson;

    Context context;
    long local_id=0;
    String userType;
    public PersistMessagesNew(String json, Context context, long local_id, String userType)
    {	messageJson=json;
        this.context = context;
        this.local_id = local_id;
        this.userType=userType;
    }

    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {

        String result = "";
        URL url;
        try {
            if(userType!=null &&userType.equalsIgnoreCase("Customer")) {
                url = new URL(MocehatCrmMainActivity.WEB_API_URL
                        + "chat");
            }else{
                url = new URL(MocehatCrmMainActivity.WEB_API_URL
                        + "sendmessage");
            }
            HttpURLConnection conn = (HttpURLConnection) url
                    .openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");

            try {
              /*  UserPreferences userPreferences = new UserPreferences(context);
                if ( userPreferences.getServerToken() != null && userPreferences.getAccessToken().length() > 0)
                    conn.setRequestProperty("token", userPreferences.getServerToken());*/
            }catch (Exception e)
            {
                e.printStackTrace();
            }


            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.connect();


            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            // writer.write(getPostData(postDataParams));
            writer.write(messageJson);

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = "";

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;

    }

    protected void onPostExecute(String result) {

        Log.i("result","result is"+result);
        onResponseReceived(result,local_id);
    }

    public abstract void onResponseReceived(String result, long local_id);
}
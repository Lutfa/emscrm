package com.emedsurecrm;

import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.KeyEvent;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.emedsurecrm.model.SharedPreferenceActivity;

import org.apache.commons.lang3.StringUtils;

import java.util.List;


/****************************************************************
 * The class handles splash screen and its animation
 ****************************************************************/
public class WelcomeSplashActivity extends Activity implements
        LocationListener {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "b9XUBY63PqHmmnI9R0QuTNiEk";
    private static final String TWITTER_SECRET = "iWaBLiNOyYybnVOahyRWlnPJbDRDuZvTJ8DnCn2uTqmituH0vo";


    ActionBar actionBar;
    String SplashThemecolor = null;
    //DeliveryMainActivity mainActivity = null;
    //LoginCredentials loginCredentials;
    LocationManager locationManager;
    //ConnectivityHelper connectivityHelper;


    SharedPreferences app_preference = null;
    private static final String MyPREFERENCES = null;
    boolean isGPSEnabled = false, isLive = false, isGPSDenied = false;
    Geocoder geocoder;
    //LatLng coordinate;
    List<Address> addresses;
    String detailAddress = null, CityName = null, currentLocationPincode = null;
    Animation spinin;
    SharedPreferenceActivity loginCredentials;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            /*TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
            Fabric.with(this, new TwitterCore(authConfig), new Digits(), new Crashlytics());
            mainActivity = new DeliveryMainActivity();
            loginCredentials = LoginCredentials.getInstance(this);
            mainActivity.updateResources(WelcomeSplashActivity.this);*/
            setContentView(R.layout.welcomesplash);
            app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            loginCredentials = SharedPreferenceActivity.getInstance(WelcomeSplashActivity.this);
           /* connectivityHelper = new ConnectivityHelper(this);
            isLive = connectivityHelper.isConnected();*/
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            currentLocationPincode = app_preference.getString("LocalName", null);
            if (currentLocationPincode != null) {
               /* if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermission();
                }*/
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    // Helper method to start the animation on the splash screen

    private void startAnimating() {

        // Load animations for all views within the TableLayout
        spinin = AnimationUtils.loadAnimation(this,
                R.anim.fade_in_welcomesplash);
        // spinin.setDuration(1000);
        LayoutAnimationController controller = new LayoutAnimationController(
                spinin);
        RelativeLayout table = (RelativeLayout) findViewById(R.id.Splashlayout);
        table.setLayoutAnimation(controller);
        // Transition to Main Menu when bottom title finishes animating
        spinin.setAnimationListener(new AnimationListener() {

            @Override
            public void onAnimationEnd(Animation animation) {

                if (!StringUtils.isBlank(loginCredentials.getAdminId())&&!loginCredentials.getAdminId().equalsIgnoreCase("0")) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, HomeScreen.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(WelcomeSplashActivity.this, SigninActivity.class);
                    startActivity(intent);
                    finish();
                }

               /* Intent intent = new Intent(WelcomeSplashActivity.this, SigninActivity.class);
                startActivity(intent);
                finish();*/
                // The animation has ended, transition to the Main Menu screen
                ///+XqnyI=
                //loginCredentials.setPasswordSalt("UWcEz/p=");
               /* if (loginCredentials.isIconUploaded() && (loginCredentials.isVerifiedUser())) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, MyOrdersActivity.class);
                    startActivity(intent);
                    finish();
                } else if (loginCredentials.isAuthenicated()) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, UploadIconActivity.class);
                    startActivity(intent);
                    finish();
                } else if (loginCredentials.islicenseUploaded()) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, AuthorizationActivity.class);
                    startActivity(intent);
                    finish();
                } else if (loginCredentials.isLoggedIn()) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, UploadLiscenceActivity.class);
                    startActivity(intent);
                    finish();
                } else if (loginCredentials.isTwitterLoggedIn()) {
                    Intent intent = new Intent(WelcomeSplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = new Intent(WelcomeSplashActivity.this, Language.class);
                    startActivity(intent);
                    finish();
                }*/
               /* Intent intent = new Intent(WelcomeSplashActivity.this, MyOrdersActivity.class);
                startActivity(intent);
                finish();*/
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                // Toast.makeText(WelcomeSplashActivity.this, "Animation repeat", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onAnimationStart(Animation animation) {
                // Toast.makeText(WelcomeSplashActivity.this, "Animation start", Toast.LENGTH_LONG).show();
            }
        });
        if (isGPSDenied)
            table.startAnimation(spinin);
    }



    /*private void GetDeviceCurrentLocation() {

        try {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (Build.VERSION.SDK_INT >= 23 && checkSelfPermission(android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && checkSelfPermission(android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            Location location1 = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            Location location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            if (location != null)
                onLocationChanged(new LatLng(location.getLatitude(), location.getLongitude()));
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 20000, 0, this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

  /*  public void onLocationChanged(LatLng latLng) {
        String detailAddressShowing = null, localArea = null, fullAddress;
        double latitude = latLng.latitude;
        double longitude = latLng.longitude;
        SharedPreferences.Editor edit;
        coordinate = new LatLng(latitude, longitude);
        geocoder = new Geocoder(WelcomeSplashActivity.this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            //String area = addresses.get(0).getSubLocality();
            localArea = addresses.get(0).getAddressLine(0);
            CityName = addresses.get(0).getLocality();
            String stateName = addresses.get(0).getAdminArea();
            String countryCode = addresses.get(0).getCountryCode();
            String countryName = addresses.get(0).getCountryName();
            String pincode = addresses.get(0).getPostalCode();
            double getlatitude = addresses.get(0).getLatitude();
            double getlongitude = addresses.get(0).getLongitude();

            if (localArea == null)
                localArea = addresses.get(0).getAddressLine(1);
            String colony = addresses.get(0).getSubLocality();
            fullAddress = localArea;
            if (CityName != null && !CityName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + CityName;
            if (stateName != null && !stateName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + stateName;
            if (countryName != null && !countryName.equalsIgnoreCase("null"))
                fullAddress = fullAddress + ", " + countryName;


            edit = app_preference.edit();
            edit.putString("CountryName", countryName);
            edit.putString("CountryCode", countryCode);
            edit.putString("LocalName", localArea);
            edit.putString("StateName", stateName);
            edit.putString("NewCityName", CityName);
            edit.putString("NewStateName", stateName);
            edit.putString("NewLandMark", colony);
            edit.putString("NewLocalName", localArea);
            edit.putString("Longitude", String.valueOf(getlongitude));
            edit.putString("Latitude", String.valueOf(getlatitude));
            edit.commit();

            if (pincode != null) {
                fullAddress = fullAddress + " - " + pincode;
                edit = app_preference.edit();
                edit.putString("Pincode", pincode);
                edit.putString("NewPincode", pincode);
                edit.putString("FullAddress", fullAddress);
                edit.commit();
            } else {
                for (int i = 0; i < 10; i++) {
                    String addressDetails = addresses.get(0).getAddressLine(i);
                    if (addressDetails != null) {
                        String pinCode = mainActivity.containsPinCode(addressDetails);
                        if (pinCode != null && !pinCode.equalsIgnoreCase("")) {
                            fullAddress = fullAddress + " - " + pinCode;
                            edit = app_preference.edit();
                            edit.putString("Pincode", pinCode);
                            edit.putString("NewPincode", pinCode);
                            edit.putString("FullAddress", fullAddress);
                            edit.commit();
                            break;
                        }

                    } else
                        break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }





    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            finish();
            System.exit(0);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onPause() {
        // ((MyApplication) this.getApplication()).setIsAppRunning(false);
        super.onPause();
        //GoogleAnalytics.getInstance(getBaseContext()).dispatchLocalHits();
        // Stop the animation
        RelativeLayout table = (RelativeLayout) findViewById(R.id.Splashlayout);
        table.clearAnimation();
    }

    @Override
    protected void onResume() {
        // ((MyApplication) this.getApplication()).setIsAppRunning(true);
        super.onResume();
        try {
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            startAnimating();

          /*  if (!isGPSEnabled) {
                if (isGPSDenied) {
                    startAnimating();
                   *//* spinin = AnimationUtils.loadAnimation(this,
                            R.anim.fade_in_welcomesplash);
                    RelativeLayout table = (RelativeLayout) findViewById(R.id.Splashlayout);
                    table.startAnimation(spinin);*//*
                }

                //startAnimating();
                else
                    return;
                // buildAlertMessageNoGps();
            } else {
                if (currentLocationPincode == null || currentLocationPincode.equalsIgnoreCase("null"))
                    GetDeviceCurrentLocation();
                else if (currentLocationPincode != null && isGPSEnabled && isLive)
                    GetDeviceCurrentLocation();
                startAnimating();
            }*/
            // startAnimating();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MocehatCrmMainActivity.PERMISSIONS_PHONESTATE_REQUEST_CODE:
                try {
                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.READ_PHONE_STATE)) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {

                                Intent callIntent = new Intent(Intent.ACTION_CALL);

                                callIntent.setData(Uri.parse("tel:+91 " + loginCredentials.getDistributerPhone()));
                                if (ActivityCompat.checkSelfPermission(WelcomeSplashActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    // TODO: Consider calling
                                    //    ActivityCompat#requestPermissions
                                    // here to request the missing permissions, and then overriding
                                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                    //                                          int[] grantResults)
                                    // to handle the case where the user grants the permission. See the documentation
                                    // for ActivityCompat#requestPermissions for more details.
                                    return;
                                }
                                startActivity(callIntent);


                            } else {
                                Toast.makeText(WelcomeSplashActivity.this, "Permission denied to call ", Toast.LENGTH_LONG).show();
                            }
                        }


                    }
                    break;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}

package com.emedsurecrm.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.emedsurecrm.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by PCCS on 5/18/2017.
 */


public class ShareContactsAdapter extends RecyclerView.Adapter<ShareContactsAdapter.MyViewHolder> implements CompoundButton.OnCheckedChangeListener {

    List<Map> mapsList = new ArrayList<>();
   static List<Map> staticMapList = new ArrayList<>();

   static HashMap checkmap = new HashMap();
    public ShareContactsAdapter(Activity act,
                                List<Map> mapsList) {
        // TODO Auto-generated constructor stub
        this.mapsList = mapsList;
        this.staticMapList = mapsList;
    }


    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name,phoneNumber;
        public CheckBox checkBox;
        public ImageView userIcon;

        public MyViewHolder(View view) {
            super(view);

            name        =   (TextView) view.findViewById(R.id.sh_name);
            checkBox    =   (CheckBox) view.findViewById(R.id.sh_checkbox);
            phoneNumber =   (TextView) view.findViewById(R.id.sh_phonenumber);
            userIcon    =   (ImageView) view.findViewById(R.id.sh_imageview);

        }
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.share_contact_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final HashMap map = (HashMap) mapsList.get(position);

        //in some cases, it will prevent unwanted situations
        holder.checkBox.setOnCheckedChangeListener(null);

        //if true, your checkbox will be selected, else unselected
        holder.checkBox.setChecked(getcheck(position));

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
               // numbers.get(holder.getAdapterPosition()).setSelected(isChecked);
                if(isChecked) {
                      Log.i("tag","is checked else"+isChecked);
                    checkmap.put(position, true);

                }
                    else {
                    Log.i("tag","is checked if"+isChecked);
                    if(checkmap!=null && checkmap.size()>0)
                        checkmap.remove(position);

                }
            }
        });

        holder.name.setText(map.get("name").toString());
        holder.phoneNumber.setText(map.get("phone").toString());
    }

    @Override
    public int getItemCount() {
        return (mapsList != null) ? mapsList.size() : 0;
    }


    private boolean getcheck(int position)
    {
        Set<Integer> keys = checkmap.keySet();  //get all keys
        for(Integer i: keys)
        {
            Log.i("position ","one"+i);
            Log.i("position ","two"+position);
            if(checkmap!=null && checkmap.size()>0) {
                if (position == i) {
                    return true;
                }
            }
        }
        return false;
    }


    public static List<Map> getCheckmap()
    {


        List<Map> mapList = new ArrayList<>();

        Set<Integer> keys = checkmap.keySet();  //get all keys
        for(Integer i: keys)
        {
            HashMap hashMap = new HashMap();
            hashMap = (HashMap) staticMapList.get(i);
            Log.i("position ","one"+i);
            Log.i("position ","one"+checkmap.get(i).toString());
            Log.i("position ","hashmap"+hashMap);
            mapList.add(hashMap);
        }


        return mapList;

    }

    public static void cleraMap()
    {
     checkmap.clear();
    }


}
package com.emedsurecrm.adapter;

import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.emedsurecrm.ChatMessagesActivity;
import com.emedsurecrm.MocehatCrmMainActivity;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.R;
import com.emedsurecrm.model.MyCustomerEntity;

import java.util.ArrayList;


public class ChatFragmentAdapter extends ArrayAdapter {
    public LayoutInflater inflater;
    //ArrayList<Order> arrayList;
    public ArrayList<MyCustomerEntity> chatArrayList = new ArrayList<MyCustomerEntity>();
    MyCustomerEntity map;
    String ChatFragment;
    Context activity;
    String UserType = null;
    MocehatCrmMainActivity mainActivity;
    private int messageCount;


    public ChatFragmentAdapter(Context context, int resource, ArrayList<MyCustomerEntity> arrayList, String pharmacyChatFragment) {
        super(context, resource, arrayList);
        inflater = LayoutInflater.from(getContext());
        this.chatArrayList = arrayList;
        this.activity = context;
        ChatFragment = pharmacyChatFragment;

    }

    @Override
    public int getCount() {
        return chatArrayList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        map = chatArrayList.get(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.chat_list_adapter, null);
            mainActivity = new MocehatCrmMainActivity();


            viewHolder.tv_name = (RobotoTextView) convertView.findViewById(R.id.tv_name);
            viewHolder.tv_chat_id = (RobotoTextView) convertView.findViewById(R.id.tv_chat_id);
            viewHolder.tv_message_count = (RobotoTextView) convertView.findViewById(R.id.tv_message_count);
            viewHolder.tv_last_message = (RobotoTextView) convertView.findViewById(R.id.tv_last_message);
            viewHolder.tv_last_message_photo = (RelativeLayout) convertView.findViewById(R.id.tv_last_message_photo);
            viewHolder.tv_last_message_emoji_and_text = (RelativeLayout) convertView.findViewById(R.id.tv_last_message_emoji_and_text);
            viewHolder.tv_time = (RobotoTextView) convertView.findViewById(R.id.tv_time);
            viewHolder.chatcard = (CardView) convertView.findViewById(R.id.chat_card);
            viewHolder.unread_message_count = (RobotoTextView) convertView.findViewById(R.id.unread_message_count);
            viewHolder.tv_password_salt = (RobotoTextView) convertView.findViewById(R.id.tv_password_salt);


            viewHolder.chatcard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = null;
                    intent = new Intent(activity,
                            ChatMessagesActivity.class);
                    intent.putExtra("RecieverId", viewHolder.tv_chat_id.getText().toString());
                    intent.putExtra("RecieverName", viewHolder.tv_name.getText().toString());
                    intent.putExtra("UserType", UserType);
                    intent.putExtra("Passwordsalt", viewHolder.tv_password_salt.getText().toString());

                    Bundle bndlanimation = ActivityOptions.makeCustomAnimation(
                            activity, R.anim.next_swipe2,
                            R.anim.next_swipe1).toBundle();
                    activity.startActivity(intent, bndlanimation);
                }
            });

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tv_name.setText("");
        viewHolder.tv_message_count.setText("");
        viewHolder.tv_time.setText("");
        viewHolder.tv_last_message.setText("");
        viewHolder.tv_chat_id.setText("");
        viewHolder.tv_password_salt.setText("");

        try {
            if (ChatFragment.equalsIgnoreCase("PharmacyChatFragment")) {

                UserType = "Pharmacy";
                viewHolder.tv_password_salt.setText(map.Passwordsalt);

                if (map.PharmacyName != null) {
                    viewHolder.tv_name.setText(map.PharmacyName);
                }
                if (map.PharmacyId != null) {
                    viewHolder.tv_chat_id.setText(map.PharmacyId);
                }


            } else {
                UserType = "Customer";
                if (map.CustomerName != null) {
                    viewHolder.tv_name.setText(map.CustomerName);
                }
                if (map.CustomerId != null) {
                    viewHolder.tv_chat_id.setText(map.CustomerId);
                }
            }

            messageCount = mainActivity.getAdminChatCount(activity, viewHolder.tv_chat_id.getText().toString());

            int totalUnread =  map.MessageCount - messageCount;
            if (totalUnread > 0) {
                viewHolder.unread_message_count.setVisibility(View.VISIBLE);
                viewHolder.unread_message_count.setText(totalUnread + "");
            } else {
                viewHolder.unread_message_count.setVisibility(View.GONE);
            }


           /* if (map.LastMessage != null && !map.LastMessage.equalsIgnoreCase("system_message")) {

            }*/


            if (map.LastMessage != null) {
                String val = map.LastMessage;
                if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg") || val.endsWith("JPG")) {
                    viewHolder.tv_last_message_photo.setVisibility(View.VISIBLE);
                    viewHolder.tv_last_message_emoji_and_text.setVisibility(View.GONE);
                } else {
                    String messageBody = map.LastMessage;
                    messageBody = messageBody.replace("\n", "").replace("\r", "");
                   // messageBody = StringEscapeUtils.unescapeJava(messageBody);
                    viewHolder.tv_last_message.setText(messageBody);

                    viewHolder.tv_last_message_photo.setVisibility(View.GONE);
                    viewHolder.tv_last_message_emoji_and_text.setVisibility(View.VISIBLE);
                }

            }
           /* if (map.MessageCount != null) {
                viewHolder.tv_message_count.setText(map.MessageCount);
            }*/
            if (map.LastActivityTime != null) {
                String time = null;
                time = mainActivity.getLocalTimeToShow(map.LastActivityTime);
                viewHolder.tv_time.setText(time);
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return convertView;

    }

    private static class ViewHolder {
        RobotoTextView tv_name, tv_time, tv_message_count, tv_last_message, tv_chat_id, tv_password_salt, unread_message_count;
        RelativeLayout tv_last_message_photo, tv_last_message_emoji_and_text;
        CardView chatcard;
    }
}

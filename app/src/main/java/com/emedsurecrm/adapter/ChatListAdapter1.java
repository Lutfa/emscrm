package com.emedsurecrm.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.emedsurecrm.ChatMessagesActivity;
import com.emedsurecrm.CryptLib;
import com.emedsurecrm.DownloadFile;
import com.emedsurecrm.MocehatCrmMainActivity;
import com.emedsurecrm.MusicUtilities;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.R;
import com.emedsurecrm.ViewContact;
import com.emedsurecrm.model.ChatMessageEntity;
import com.emedsurecrm.model.SharedPreferenceActivity;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

/**
 * Created by PCCS on 4/19/2017.
 */

public class ChatListAdapter1 extends RecyclerView.Adapter<ChatListAdapter1.CustomViewHolder> {

    ClipData myClip;
    private String data = "";

    public ClipboardManager myClipboard;
    List<ChatMessageEntity> chatMessageEntityList;
    private String messageBody, messageTime, messageFrom, mainMessage, messageType, senderId, receivedFileName, messageURL;
    List<Long> messageTimes = null;
    MocehatCrmMainActivity mainActivity = null;
    Context context;
    String previousScreen, fileToDownload;
    ProgressDialog pdialogue;
    Gson gson;

    private int stateMediaPlayer;
    private final int stateMP_Error = 0;
    private final int stateMP_NotStarter = 1;
    private final int stateMP_Playing = 2;
    private final int stateMP_Pausing = 3;

    String localFilePath = null, timeLong = null;

    private MusicUtilities utils;
    private ChatListAdapterListener listener;

    private SparseBooleanArray selectedItems;
    private static int currentSelectedIndex = -1;
    MediaPlayer player;
    private Handler mHandler = new Handler();
    ;
    String key = "";
    CryptLib _crypt;
    SharedPreferenceActivity loginCredentials;
    Fragment fragment;

    public ChatListAdapter1(Context context, Fragment fragment, List<ChatMessageEntity> chatMessageEntityList, String previousScreen, ChatListAdapterListener listener) {
        this.context = context;
        this.chatMessageEntityList = chatMessageEntityList;
        messageTimes = new ArrayList<Long>();
        mainActivity = new MocehatCrmMainActivity();
        this.fragment = fragment;
        this.previousScreen = previousScreen;
        gson = new Gson();


        selectedItems = new SparseBooleanArray();
        this.listener = listener;
        utils = new MusicUtilities();
        loginCredentials = SharedPreferenceActivity.getInstance(context);


    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        myClipboard = (ClipboardManager) context.getSystemService(CLIPBOARD_SERVICE);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_list_item_adapter1, parent, false);
        CustomViewHolder holder = new CustomViewHolder(view);
        return holder;

    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {

        holder.chatImageLeftLayout.setVisibility(View.GONE);
        holder.chatImageRightLayout.setVisibility(View.GONE);
        holder.chatContentLeftLayout.setVisibility(View.GONE);
        holder.chatContentRightLayout.setVisibility(View.GONE);
        holder.centerChatLayout.setVisibility(View.GONE);


        holder.leftChatReplyLayout.setVisibility(View.GONE);
        holder.rightChatReplyLayout.setVisibility(View.GONE);
        holder.replyrighttextviewlayout.setVisibility(View.GONE);
        holder.replyrightimageviewlayout.setVisibility(View.GONE);
        holder.replyleftimageviewlayout.setVisibility(View.GONE);
        holder.replylefttextviewlayout.setVisibility(View.GONE);


        holder.ImageLeftchatContentDocumentLeft.setVisibility(View.GONE);
        holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
        holder.ImageRightchatContentDocumentRight.setVisibility(View.GONE);
        holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
        holder.mapRightLayout.setVisibility(View.GONE);
        holder.mapLeftLayout.setVisibility(View.GONE);

        holder.ImageLeftaudioLayout.setVisibility(View.GONE);
        holder.ImageRightaudioLayout.setVisibility(View.GONE);
        holder.contactRightLayout.setVisibility(View.GONE);
        holder.contactLeftLayout.setVisibility(View.GONE);


        final ChatMessageEntity chatMessageEntity = chatMessageEntityList.get(position);
        messageBody = null;
        messageBody = mainActivity.getDecryptedText(chatMessageEntity.MessageBody);
        Long time = null;
        if (chatMessageEntity.MessageTimeInLong != null) {
            time = Long.parseLong(chatMessageEntity.MessageTimeInLong);
            messageTime = mainActivity.fromDotNetTicks(time);
            messageTimes.add(time);
        }
        if (chatMessageEntity.CreatedOnUtc != null) {
            messageTime = mainActivity.getLocalTimeToShow(chatMessageEntity.CreatedOnUtc);
        }
        if (chatMessageEntity.TimeStamp != null) {
            messageTime = mainActivity.getLocalTimeToShow(chatMessageEntity.TimeStamp);
        }
        messageFrom = chatMessageEntity.SenderName;
        senderId = chatMessageEntity.SenderId;
        messageType = chatMessageEntity.MessageType;
        String getlocalFilePath = null;
        getlocalFilePath = chatMessageEntity.ImagePath;
        if (TextUtils.isEmpty(messageFrom)) {
            try {
                messageFrom = "Admin";
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }

        /*if (previousScreen != null && previousScreen.equalsIgnoreCase(context.getString(R.string.questionactivitydesigner))) {
            if (messageBody.contains(activityName)) {
               // continue;
            }
        }*/
        if (messageFrom != null && messageFrom.equalsIgnoreCase("system_message")) {
         /*   if (previousScreen != null
                    && !previousScreen.equalsIgnoreCase(context.getString(R.string.questionactivitydesigner))
                    && !previousScreen.equalsIgnoreCase(context.getString(R.string.one2oneactivitydesigner))) {
             //   setChatContentSystem(messageBody, messageTime);

            }*/
            holder.centerChatLayout.setVisibility(View.VISIBLE);
            holder.systemMessageBubble.setText(messageBody);

        } else if (messageType != null && messageType.equalsIgnoreCase("Picture")) {

            receivedFileName = messageBody;
            if (senderId != null && !senderId.equalsIgnoreCase(String.valueOf(loginCredentials.getAdminId()))) {

/**************************************chat image left layout *********************************************/

                holder.chatImageLeftLayout.setVisibility(View.VISIBLE);
                holder.itemView.setActivated(selectedItems.get(position, false));
                holder.chatImageLeftLayout.setVisibility(View.VISIBLE);

/** click on image show image from gallery **/

                holder.chatImageLeftLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (holder.itemView.isActivated() == false && getSelectedItemCount() == 0) {
                            String Fpath = null;
                            try {
                                //      String getFilenameFromURL = chatMessageEntity.MessageBody;
                                String getFilenameFromURL = chatMessageEntity.ImagePath;
                                String fileNameFromURL = getFilenameFromURL.substring(getFilenameFromURL.lastIndexOf('/') + 1);
                                Fpath = chatMessageEntity.ImagePath;
                                // Log.i("media path is","message body"+messageBody);
                                Log.i("message body is", "media path" + chatMessageEntity.ImagePath);
                                if (Fpath != null && Fpath.endsWith(".jpg")) {
                                    if (Fpath.endsWith(".jpg") || Fpath.endsWith(".png") || Fpath.endsWith("jpeg")) {
                                        File filecheck = new File(Fpath);
                                        if (filecheck.exists()) {

                                            showDownloadedImageFile(filecheck);
                                        } else {
                                            String localpath = chatMessageEntity.ImagePath;

                                            String galleryfilename = localpath.substring(localpath.lastIndexOf('/') + 1);
                                            showImageFromUrl(localpath);

                                            /*   Bitmap final_bitmap = getBitmapFromURL(localpath);
                                            if (final_bitmap != null && !final_bitmap.equals("")) {
                                                mainActivity.SaveImage(final_bitmap, null, galleryfilename, null);
                                                String Fpath1 = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Images/" + galleryfilename;
                                                File filecheck1 = new File(Fpath1);
                                                if (filecheck1.exists()) {
                                                    showDownloadedImageFile(filecheck1);
                                                }
                                            }*/
                                        }
                                    }
                                } else if (fileNameFromURL.endsWith("jpeg")) {
                                    Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Images/"
                                            + fileNameFromURL;
                                    File filecheck = new File(Fpath);
                                    if (filecheck.exists()) {
                                        showDownloadedImageFile(filecheck);
                                    } else {
                                        showImageFromUrl(getFilenameFromURL);
                                       /* Bitmap final_bitmap = getBitmapFromURL(getFilenameFromURL);
                                        if (final_bitmap != null) {
                                            mainActivity.SaveImage(final_bitmap, null, fileNameFromURL, null);
                                            String Fpath1 = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Images/" + fileNameFromURL;
                                            File filecheck1 = new File(Fpath1);
                                            if (filecheck1.exists()) {
                                                showDownloadedImageFile(filecheck1);
                                            }
                                        }*/

                                    }

                                } else if (fileNameFromURL.toString().endsWith("pdf")
                                        || fileNameFromURL.toString().endsWith("doc")
                                        || fileNameFromURL.toString().endsWith("docx")
                                        || fileNameFromURL.toString().endsWith("ppt")
                                        || fileNameFromURL.toString().endsWith("pptx")
                                        || fileNameFromURL.toString().endsWith("xls")
                                        || fileNameFromURL.toString().endsWith("xlsx")
                                        || fileNameFromURL.toString().endsWith("txt")) {

                                    String getFilenameFromURL1 = chatMessageEntity.ImagePath;
                                    // if document file contains in local
                                    if (getFilenameFromURL1 != null) {
                                        final String fileNameFromURL1 = getFilenameFromURL1.substring(getFilenameFromURL1.lastIndexOf('/') + 1);
                                        Log.i("tag", "calling file" + fileNameFromURL1);
                                        Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Documents/"
                                                + fileNameFromURL1;
                                        File filecheck = new File(Fpath);
                                        if (filecheck.exists()) {
                                            Log.i("tag", "calling document" + filecheck);
                                            showDownloadedDocumentFile(filecheck);
                                        } else {
                                            DownloadFile downloadFile = new DownloadFile(context, messageBody, fileNameFromURL1) {
                                                @Override
                                                public void onResponseReceived(String result) {
                                                    Log.i("result on response", "result on response" + result);
                                                    String Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Documents/"
                                                            + fileNameFromURL1;
                                                    File filecheck = new File(Fpath);
                                                    if (filecheck.exists()) {
                                                        Log.i("tag", "calling document" + filecheck);
                                                        showDownloadedDocumentFile(filecheck);
                                                    }
                                                }
                                            };
                                            downloadFile.execute();
                                            // download document from url and save it into documents
                                            // after that show the user.
                                        }
                                    }

                                } else if (fileNameFromURL.endsWith("map")) {
                                    try {
                                        //  String mbody = chatMessageEntity.MessageBody;
                                        String mbody = messageBody;
                                        if (mbody.contains("#")) {
                                            String latlang = mbody.split("#")[2];
                                            Log.i("latlang", "latlang values" + latlang.split(":")[1].replace(".map", ""));
                                            String finallatlang = latlang.split(":")[1].replace(".map", "");
                                            finallatlang = finallatlang.replace("(", "");
                                            finallatlang = finallatlang.replace(")", "");
                                            Log.i("final latlang", "final latlang" + finallatlang);
                                            String mTitle = mbody.split("#")[0];
                                            String geoUri = "http://maps.google.com/maps?q=loc:" + finallatlang.trim().split(",")[0] + "," + finallatlang.trim().split(",")[1] + " (" + mTitle + ")";
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                                            context.startActivity(intent);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        listener.onMessageRowClicked(position);

                        //Toast.makeText(activity,"position is "+position,Toast.LENGTH_LONG).show();
                    }
                });


                holder.chatImageLeftLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        listener.onRowLongClicked(position);
                        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                        return true;
                    }
                });


              /* String localName = mainActivity.getNickName(context, senderId);
                if (localName != null)
                    messageFrom = localName;*/


                String docFPath = null;
                Bitmap bitmap;
                try {

/*** show image using picaso library ***/


                    String val;
                   /* if(messageBody!=null) {
                        val=messageBody;
                    }else{
                        val=chatMessageEntity.ImagePath;
                    }*/
                    if (chatMessageEntity.MessageBody != null) {
                        val = chatMessageEntity.MessageBody;
                    } else {
                        val = chatMessageEntity.ImagePath;
                    }
                    if (chatMessageEntity.ImagePath != null && !chatMessageEntity.ImagePath.equalsIgnoreCase("null")) {

                        if (val != null && val.equalsIgnoreCase(chatMessageEntity.MessageBody) && chatMessageEntity.ImagePath != null && !val.equalsIgnoreCase(chatMessageEntity.ImagePath)) {
                            val = chatMessageEntity.ImagePath;
                        }
                    }
                    if (val != null) {
                        if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg") || val.endsWith("JPG")) {

                            holder.ImageLeftchatContentImageLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.GONE);
                            if (getlocalFilePath != null) {
                                Picasso.with(context).load(getlocalFilePath).into(holder.ImageLeftchatContentImageLeft);
                            } else {
                                String Fpath = chatMessageEntity.MessageBody;


                                if (Fpath != null) {
                                    if (Fpath.endsWith(".jpg") || Fpath.endsWith(".png") || Fpath.endsWith(".jpeg") || Fpath.endsWith("JPG")) {
                                        File filecheck = new File(Fpath);
                                        Log.i("tag", "filecheck" + filecheck);
                                        if (filecheck.exists()) {
                                            Picasso.with(context)
                                                    .load(filecheck)
                                                    .placeholder(R.drawable.loadingimage)
                                                    .into(holder.ImageLeftchatContentImageLeft);
                                        } else {
                                            Picasso.with(context)
                                                    .load(Fpath.trim())
                                                    .placeholder(R.drawable.loadingimage)
                                                    .into(holder.ImageLeftchatContentImageLeft);

                                        }
                                    }
                                }

                            }
                            /*holder.ImageLeftchatContentImageLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.GONE);

                            String   Fpath =    chatMessageEntity.ImagePath;
                            if(Fpath!=null) {
                                if (Fpath.endsWith(".jpg") || Fpath.endsWith(".png") || Fpath.endsWith(".jpeg") || val.endsWith("JPG")) {
                                    File filecheck = new File(Fpath);
                                    Log.i("tag","filecheck"+filecheck);
                                    if(filecheck.exists()) {
                                        Picasso.with(context)
                                                .load(filecheck)
                                                .placeholder(R.drawable.loadingimage)
                                                .into(holder.ImageLeftchatContentImageLeft);
                                    }
                                    else
                                    {
                                        Picasso.with(context)
                                                .load("http://192.168.1.10:8081/content/images/thumbs/0004433_bm20170912161822.jpg")
                                                .placeholder(R.drawable.loadingimage)
                                                .into(holder.ImageLeftchatContentImageLeft);

                                    }
                                }
                            }
                            else {
                                Picasso.with(context)
                                        .load(val.trim())
                                        .placeholder(R.drawable.loadingimage)
                                        .into(holder.ImageLeftchatContentImageLeft);
                            }



                       */
                        } else if (val.toString().endsWith(".mp3")
                                || val.toString().endsWith(".ogg")
                                || val.toString().endsWith(".m4a")
                                || val.toString().endsWith(".amr")
                                || val.toString().endsWith(".3gpp")
                                || val.toString().endsWith(".mp4")) {


                            final String localMediaAudioPath = chatMessageEntity.ImagePath;
                            if (localMediaAudioPath != null) {
                                final String mpath = localMediaAudioPath;
                                holder.ImageLeftchatContentVideoLeft.setVisibility(View.GONE);
                                holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                                holder.ImageLeftaudioLayout.setVisibility(View.VISIBLE);
                                player = new MediaPlayer();

                                // if audio file exists in local show from local
                                try {
                                    File filecheck = new File(localMediaAudioPath);
                                    if (filecheck.exists()) {
                                        try {
                                            player.setDataSource(localMediaAudioPath);
                                            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                            player.prepare();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        // set Progress bar values

                                        holder.leftSideSongSeekbar.setProgress(0);
                                        holder.leftSideSongSeekbar.setMax(100);
                                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                mediaPlayer.stop();
                                            }
                                        });
                                        holder.leftSideSongSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                            @Override
                                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                                // remove message Handler from updating progress bar
                                                mHandler.removeCallbacks(mUpdateTimeTask);
                                            }

                                            @Override
                                            public void onStartTrackingTouch(SeekBar seekBar) {
                                                mHandler.removeCallbacks(mUpdateTimeTask);
                                                int totalDuration = player.getDuration();
                                                int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
                                                // forward or backward to certain seconds
                                                player.seekTo(currentPosition);
                                                // update timer progress again
                                                updateProgressBar();
                                            }

                                            @Override
                                            public void onStopTrackingTouch(SeekBar seekBar) {

                                            }
                                        });
                                    }
                                } catch (IllegalArgumentException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (IllegalStateException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                } catch (Exception e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                }


                                // Changing Button Image to pause image
                                //   holder.ImageLeftstart.setImageResource(R.drawable.ic_media_play);
                                holder.ImageLeftstart.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        File file = new File(localMediaAudioPath);
                                        if (file.exists()) {
                                            if (player.isPlaying()) {
                                                if (player != null) {
                                                    player.pause();
                                                    // Changing button image to play button
                                                    // holder.ImageLeftstart.setImageResource(R.drawable.ic_media_play);
                                                }
                                            } else {
                                                // Resume song
                                                if (player != null) {
                                                    player.start();
                                                    // Changing button image to pause button
                                                    //    holder.ImageLeftstart.setImageResource(R.drawable.ic_media_pause);
                                                    // Updating progress bar
                                                    updateProgressBar();
                                                }
                                            }
                                        } else {
                                            // if file not exists download and show
                                            final String audioFileName = localMediaAudioPath.substring(localMediaAudioPath.lastIndexOf('/') + 1);
                                            DownloadFile downloadFile = new DownloadFile(context, messageBody, audioFileName) {
                                                @Override
                                                public void onResponseReceived(String result) {
                                                    if (result != null && result.length() > 0) {
                                                        File file = new File(localMediaAudioPath);
                                                        if (file.exists()) {
                                                            if (player.isPlaying()) {
                                                                if (player != null) {
                                                                    player.pause();
                                                                    // Changing button image to play button
                                                                    //  holder.ImageLeftstart.setImageResource(R.drawable.ic_media_play);
                                                                }
                                                            } else {
                                                                // Resume song
                                                                if (player != null) {
                                                                    player.start();
                                                                    // Changing button image to pause button
                                                                    //  holder.ImageLeftstart.setImageResource(R.drawable.ic_media_pause);
                                                                    // Updating progress bar
                                                                    updateProgressBar();
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            };
                                            downloadFile.execute();


                                        }
                                    }
                                });
                            }


                        } else if (val.endsWith("pdf")) {
                            //  File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.pdf_download);
                        } else if (val.endsWith("doc")) {
                            //  File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.docxfile);
                        } else if (val.endsWith("docx")) {
                            //   File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.docxfile);
                        } else if (val.endsWith("ppt")) {
                            //   File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.pptfile);
                        } else if (val.endsWith("pptx")) {
                            //  File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.pptxfile);
                        } else if (val.endsWith("xls")) {
                            //  File filecheck = new File(docFPath);

                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.xlsfile);
                        } else if (val.endsWith("xlsx")) {
                            //  File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.xlsxfile);
                        } else if (val.endsWith("txt")) {
                            //  File filecheck = new File(docFPath);
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentDocumentLeft.setBackgroundResource(R.drawable.txtfile);
                        } else if (val.endsWith(".map")) {
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.GONE);
                            holder.mapLeftLayout.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentMapLeft.setVisibility(View.VISIBLE);
                            holder.ImageLeftchatContentMapLeft.setBackgroundResource(R.drawable.map_new);
                            Log.i("map add", "map add one" + val.split("#")[0].toString());
                            Log.i("map add", "map add two" + val.split("#")[1].toString());
                            Log.i("map add", "map add three" + val.split("#")[2].toString());

                            if (val.contains("#")) {
                                if (val.split("#")[0].toString().startsWith("1") || val.split("#")[0].toString().startsWith("2")) {

                                    String value = val.split("#")[1].toString();
                                    holder.ImageLeftChatContentMapText.setText(value.split(",")[0].toString());
                                    holder.ImageLeftChatContentMapTextSmall.setText(val.split("#")[1].toString());

                                } else {
                                    holder.ImageLeftChatContentMapText.setText(val.split("#")[0].toString());
                                    holder.ImageLeftChatContentMapTextSmall.setText(val.split("#")[1].toString());

                                }
                            }
                        } else if (val.endsWith(".contact")) {
                            holder.ImageLeftchatContentImageLeft.setVisibility(View.GONE);
                            holder.ImageLeftchatContentDocumentLeft.setVisibility(View.GONE);
                            holder.mapLeftLayout.setVisibility(View.GONE);
                            holder.contactLeftLayout.setVisibility(View.VISIBLE);
                            final String data = val.replace(".contact", "");
                            Log.i("data is", "data" + data);
                            holder.contactLeftName.setText(data.split("@")[0].toString());
                            holder.viewContactLeft.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Intent intent = new Intent(context, ViewContact.class);
                                    intent.putExtra("data", data);
                                    context.startActivity(intent);
                                }
                            });
                        }

                        // text_filename.setText(receivedFileName2);
                        holder.ImageLeftImage_name.setText(receivedFileName);
                        //  TextView MessageTimeLong = (TextView) viewImageMsgLeft.findViewById(R.id.messageTimeLong);
                        // holder.MessageTimeLong.setText(String.valueOf(time));

                        if (messageFrom != null && !messageFrom.equalsIgnoreCase("system_message")) {
                            holder.ImageLeftchatContentImageLeftName.setText(messageFrom);
                        } else {
                            //viewImageMsgLeft.setVisibility(View.GONE);
                        }
                        holder.ImageLefttimeLeftImage.setText(messageTime);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }


            } else {
                holder.chatImageRightLayout.setVisibility(View.VISIBLE);
                //  setChatContentImageRight(messageFrom, messageTime, receivedFileName, time, getlocalFilePath);


                String val;
                if (chatMessageEntity.MessageBody != null) {
                    val = chatMessageEntity.MessageBody;
                } else {
                    val = chatMessageEntity.ImagePath;
                }
                if (chatMessageEntity.ImagePath != null && !chatMessageEntity.ImagePath.equalsIgnoreCase("null")) {

                    if (val != null && val.equalsIgnoreCase(chatMessageEntity.MessageBody) && chatMessageEntity.ImagePath != null && !val.equalsIgnoreCase(chatMessageEntity.ImagePath)) {
                        val = chatMessageEntity.ImagePath;
                    }
                }
/** show image form gallery only when item is activated false and selected item count is equal to zero ****/

                holder.chatImageRightLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.i("tag", "itemview activated" + holder.itemView.isActivated());
                        Log.i("tag", "get selected item count" + getSelectedItemCount());

                        if (holder.itemView.isActivated() == false && getSelectedItemCount() == 0) {
                            String Fpath = null;
                            try {


                                // String getFilenameFromURL = chatMessageEntity.MessageBody;
                                String getFilenameFromURL = messageBody;
                                String fileNameFromURL = getFilenameFromURL.substring(getFilenameFromURL.lastIndexOf('/') + 1);

                                Fpath = chatMessageEntity.MessageBody;

                                if (Fpath != null && Fpath.endsWith(".jpg")) {
                                    if (Fpath.endsWith(".jpg") || Fpath.endsWith(".png") || Fpath.endsWith(".jpeg")) {
                                        File filecheck = new File(Fpath);
                                        if (filecheck.exists()) {
                                            Log.i("show from gallery", "true");
                                            showDownloadedImageFile(filecheck);
                                        } else {
                                            Log.i("show from gallery", "false");
                                            String localpath = chatMessageEntity.ImagePath;

                                            String galleryfilename = localpath.substring(localpath.lastIndexOf('/') + 1);

                                            showImageFromUrl(localpath);
                                            /*   Bitmap final_bitmap = getBitmapFromURL(getFilenameFromURL);
                                            if (final_bitmap != null) {
                                                mainActivity.SaveImage(final_bitmap, null, galleryfilename, null);
                                                String Fpath1 = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Images/" + galleryfilename;
                                                File filecheck1 = new File(Fpath1);
                                                if (filecheck.exists()) {
                                                    showDownloadedImageFile(filecheck1);
                                                }
                                            }*/
                                        }
                                    }

                                } else if (fileNameFromURL.endsWith("jpg")) {
                                    Log.i("show from gallery", "false");
                                    Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Images/" + fileNameFromURL;
                                    File filecheck = new File(Fpath);
                                    if (filecheck.exists()) {
                                        showDownloadedImageFile(filecheck);
                                    } else {

                                     //   Bitmap final_bitmap = getBitmapFromURL(getFilenameFromURL);
                                        showImageFromUrl(getFilenameFromURL);

                                    }

                                } else if (fileNameFromURL.toString().endsWith("pdf")
                                        || fileNameFromURL.toString().endsWith("doc")
                                        || fileNameFromURL.toString().endsWith("docx")
                                        || fileNameFromURL.toString().endsWith("ppt")
                                        || fileNameFromURL.toString().endsWith("pptx")
                                        || fileNameFromURL.toString().endsWith("xls")
                                        || fileNameFromURL.toString().endsWith("xlsx")
                                        || fileNameFromURL.toString().endsWith("txt")) {
                                    final String getFilenameFromURL1 = chatMessageEntity.ImagePath;
                                    // if document file contains in local
                                    if (getFilenameFromURL1 != null) {
                                        String fileNameFromURL1 = getFilenameFromURL1.substring(getFilenameFromURL1.lastIndexOf('/') + 1);
                                        Log.i("tag", "calling file" + fileNameFromURL1);
                                        Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Documents/"
                                                + fileNameFromURL1;
                                        File filecheck = new File(Fpath);
                                        if (filecheck.exists()) {
                                            Log.i("tag", "calling local document true" + filecheck);
                                            showDownloadedDocumentFile(filecheck);
                                        } else {
                                            Log.i("tag", "calling local document false");

                                            // download file from url and save it into documents
                                            // and show from local documents.

                                            DownloadFile downloadFile = new DownloadFile(context, messageBody, getFilenameFromURL1) {
                                                @Override
                                                public void onResponseReceived(String result) {
                                                    Log.i("result is", "result for file" + result);
                                                    String Fpath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy/BM Pharmacy Documents/"
                                                            + getFilenameFromURL1;
                                                    File filecheck = new File(Fpath);
                                                    if (filecheck.exists()) {
                                                        Log.i("tag", "calling local document true" + filecheck);
                                                        showDownloadedDocumentFile(filecheck);
                                                    }
                                                }
                                            };
                                            downloadFile.execute();
                                        }
                                    }
                                } else if (fileNameFromURL.endsWith(".map")) {
                                    try {
                                        //  String mbody = chatMessageEntity.MessageBody;
                                        String mbody = messageBody;
                                        if (mbody.contains("#")) {

                                            String latlang = mbody.split("#")[2];
                                            Log.i("latlang", "latlang values" + latlang.split(":")[1].replace(".map", ""));
                                            String finallatlang = latlang.split(":")[1].replace(".map", "");
                                            finallatlang = finallatlang.replace("(", "");
                                            finallatlang = finallatlang.replace(")", "");
                                            Log.i("final latlang", "final latlang" + finallatlang);
                                            String mTitle = mbody.split("#")[0];
                                            String geoUri = "http://maps.google.com/maps?q=loc:" + finallatlang.trim().split(",")[0] + "," + finallatlang.trim().split(",")[1] + " (" + mTitle + ")";
                                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                                            context.startActivity(intent);
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                }


                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
                        listener.onMessageRowClicked(position);
                        //    Toast.makeText(activity,"position is "+position,Toast.LENGTH_LONG).show();
                    }
                });

                holder.chatImageRightLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        listener.onRowLongClicked(position);
                        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                        return true;
                    }
                });

/********************************* chat image right layout **********************************************/

                // change the row state to activated
                holder.itemView.setActivated(selectedItems.get(position, false));

                String docFPath = null;
                Bitmap bitmap;
                try {
                    //  String stringNoQuotes = mainMessage.replaceAll("^\"|\"$", "");
                    // docFPath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy /BM Pharmacy  Documents/" + receivedFileName;
                    if (time != null)
                        holder.ImageRightMessageTimeLong.setText(String.valueOf(time));


                    try {
                        if (val != null) {
                            Log.i("url", "url is" + val);
                            if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg")) {


                                holder.ImageRightchatContentDocumentRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentImageRight.setVisibility(View.VISIBLE);
                                if (chatMessageEntity.ImagePath != null && !chatMessageEntity.ImagePath.equalsIgnoreCase("null")) {
                                    Picasso.with(context).load(chatMessageEntity.ImagePath).placeholder(R.drawable.loadingimage).into(holder.ImageRightchatContentImageRight);
                                } else {
                                    if (chatMessageEntity.MessageBody != null) {
                                        final BitmapFactory.Options options = new BitmapFactory.Options();
                                        options.inSampleSize = 1;
                                        bitmap = BitmapFactory.decodeFile(chatMessageEntity.MessageBody);
                                        holder.ImageRightchatContentImageRight.setImageBitmap(bitmap);
                                        /*File filecheck = new File(chatMessageEntity.MessageBody);
                                        Log.i("tag","filecheck"+filecheck);
                                        if(filecheck.exists()) {
                                            Picasso.with(context)
                                                    .load(filecheck)
                                                    .placeholder(R.drawable.loadingimage)
                                                    .into(holder.ImageRightchatContentImageRight);
                                        }
                                        else
                                        {
                                            final BitmapFactory.Options options = new BitmapFactory.Options();
                                            options.inSampleSize = 1;
                                            bitmap = BitmapFactory.decodeFile(chatMessageEntity.MessageBody);
                                            holder.  ImageRightchatContentImageRight.setImageBitmap(bitmap);
                                           *//* Picasso.with(context)
                                                    .load(val.trim())
                                                    .placeholder(R.drawable.loadingimage)
                                                    .into(holder.ImageRightchatContentImageRight);*//*

                                        }*/
                                    }
                                }










                                /*
                                holder.ImageRightchatContentDocumentRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentImageRight.setVisibility(View.VISIBLE);

                                Log.i("url","ImagePath"+chatMessageEntity.ImagePath);



                                String   Fpath =    chatMessageEntity.ImagePath;



                                if(Fpath!=null) {
                                    if (Fpath.endsWith(".jpg") || Fpath.endsWith(".png") || Fpath.endsWith(".jpeg") || Fpath.endsWith("JPG")) {
                                       File filecheck = new File(Fpath);
                                        Log.i("tag","filecheck"+filecheck);
                                       if(filecheck.exists()) {
                                           Picasso.with(context)
                                                   .load(filecheck)
                                                   .placeholder(R.drawable.loadingimage)
                                                   .into(holder.ImageRightchatContentImageRight);
                                       }
                                       else
                                       {
                                           Picasso.with(context)
                                                   .load(val.trim())
                                                   .placeholder(R.drawable.loadingimage)
                                                   .into(holder.ImageRightchatContentImageRight);

                                       }
                                    }
                                }
                                else {
                                    Picasso.with(context)
                                            .load(val.trim())
                                            .placeholder(R.drawable.loadingimage)
                                            .into(holder.ImageRightchatContentImageRight);
                                }*/

                            } else if (val.toString().endsWith(".mp3")
                                    || val.toString().endsWith(".ogg")
                                    || val.toString().endsWith(".m4a")
                                    || val.toString().endsWith(".amr")
                                    || val.toString().endsWith(".3gpp")
                                    || val.toString().endsWith(".mp4")) {
                                final String localMediaAudioPath = chatMessageEntity.ImagePath;
                                if (localMediaAudioPath != null) {
                                    final String mpath = localMediaAudioPath;
                                    // holder.ImageRightchatContentVideoRight.setVisibility(View.GONE);
                                    holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                    holder.ImageRightaudioLayout.setVisibility(View.VISIBLE);


                                    player = new MediaPlayer();

                                    // if audio file exists in local show from local
                                    try {
                                        try {
                                            player.setDataSource(localMediaAudioPath);
                                            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                            player.prepare();
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        // set Progress bar values

                                        holder.rightSideSongSeekbar.setProgress(0);
                                        holder.rightSideSongSeekbar.setMax(100);
                                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mediaPlayer) {
                                                mediaPlayer.stop();
                                            }
                                        });
                                        holder.rightSideSongSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                                            @Override
                                            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                                                // remove message Handler from updating progress bar
                                                mHandler.removeCallbacks(mUpdateTimeTask);
                                            }

                                            @Override
                                            public void onStartTrackingTouch(SeekBar seekBar) {
                                                mHandler.removeCallbacks(mUpdateTimeTask);
                                                int totalDuration = player.getDuration();
                                                int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
                                                // forward or backward to certain seconds
                                                player.seekTo(currentPosition);
                                                // update timer progress again
                                                updateProgressBar();
                                            }

                                            @Override
                                            public void onStopTrackingTouch(SeekBar seekBar) {

                                            }
                                        });
                                    } catch (IllegalArgumentException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (IllegalStateException e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    } catch (Exception e) {
                                        // TODO Auto-generated catch block
                                        e.printStackTrace();
                                    }
                                    // Changing Button Image to pause image
                                    //  holder.ImageRightstart.setImageResource(R.drawable.ic_media_play);
                                    holder.ImageRightstart.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            File file = new File(localMediaAudioPath);
                                            if (file.exists()) {
                                                if (player.isPlaying()) {
                                                    if (player != null) {
                                                        player.pause();
                                                        // Changing button image to play button
                                                        //  holder.ImageRightstart.setImageResource(R.drawable.ic_media_play);
                                                    }
                                                } else {
                                                    // Resume song
                                                    if (player != null) {
                                                        player.start();
                                                        // Changing button image to pause button
                                                        //  holder.ImageRightstart.setImageResource(R.drawable.ic_media_pause);
                                                        // Updating progress bar
                                                        updateProgressBar();
                                                    }
                                                }
                                            } else {
                                                // if file not exists download and show
                                                final String audioFileName = localMediaAudioPath.substring(localMediaAudioPath.lastIndexOf('/') + 1);
                                                DownloadFile downloadFile = new DownloadFile(context, messageBody, audioFileName) {
                                                    @Override
                                                    public void onResponseReceived(String result) {
                                                        if (result != null && result.length() > 0) {
                                                            File file = new File(localMediaAudioPath);
                                                            if (file.exists()) {
                                                                if (player.isPlaying()) {
                                                                    if (player != null) {
                                                                        player.pause();
                                                                        // Changing button image to play button
                                                                        //  holder.ImageRightstart.setImageResource(R.drawable.ic_media_play);
                                                                    }
                                                                } else {
                                                                    // Resume song
                                                                    if (player != null) {
                                                                        player.start();
                                                                        // Changing button image to pause button
                                                                        //  holder.ImageRightstart.setImageResource(R.drawable.ic_media_pause);
                                                                        // Updating progress bar
                                                                        updateProgressBar();
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                };
                                                downloadFile.execute();
                                            }
                                        }
                                    });
                                }
                            } else if (val.endsWith("pdf")) {
                                //   File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.pdf_download);
                            } else if (val.endsWith("doc")) {
                                //   File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.docxfile);
                            } else if (val.endsWith("docx")) {
                                //     File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.docxfile);
                            } else if (val.endsWith("ppt")) {
                                File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.pptfile);
                            } else if (val.endsWith("pptx")) {
                                //   File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.pptxfile);
                            } else if (val.endsWith("xls")) {

                                //  File filecheck = new File(docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.xlsfile);

                            } else if (val.endsWith("xlsx")) {
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.xlsxfile);
                            } else if (val.endsWith("txt")) {
                                //   docFPath = chatMessageEntity.ImagePath;
                                Log.i("document path", "document path" + docFPath);
                                if (holder.ImageRightchatContentDocumentRight.getVisibility() == View.VISIBLE) {
                                } else {
                                    holder.ImageRightchatContentDocumentRight.setVisibility(View.VISIBLE);
                                }
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setBackgroundResource(R.drawable.txtfile);
                            } else if (val.endsWith(".map")) {
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setVisibility(View.GONE);
                                holder.mapRightLayout.setVisibility(View.VISIBLE);
                                holder.ImageRightchatContentMapRight.setVisibility(View.VISIBLE);
                                holder.ImageRightchatContentMapRight.setBackgroundResource(R.drawable.map_new);

                                if (val.contains("#")) {
                                    Log.i("map add", "map add one" + val.split("#")[0].toString());
                                    Log.i("map add", "map add two" + val.split("#")[1].toString());
                                    Log.i("map add", "map add three" + val.split("#")[2].toString());
                                    if (val.split("#")[0].toString().startsWith("1") || val.split("#")[0].toString().startsWith("2")) {
                                        String value = val.split("#")[1].toString();
                                        holder.ImageRightChatContentMapText.setText(value.split(",")[0].toString());
                                        holder.ImageRightChatContentMapTextSmall.setText(val.split("#")[1].toString());
                                    } else {
                                        holder.ImageRightChatContentMapText.setText(val.split("#")[0].toString());
                                        holder.ImageRightChatContentMapTextSmall.setText(val.split("#")[1].toString());

                                    }
                                }
                            } else if (val.endsWith(".contact")) {
                                holder.ImageRightchatContentImageRight.setVisibility(View.GONE);
                                holder.ImageRightchatContentDocumentRight.setVisibility(View.GONE);
                                holder.mapRightLayout.setVisibility(View.GONE);
                                holder.contactRightLayout.setVisibility(View.VISIBLE);
                                final String data = val.replace(".contact", "");
                                Log.i("data is", "data" + data);
                                holder.contactRightName.setText(data.split("@")[0].toString());
                                holder.viewContactRight.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(context, ViewContact.class);
                                        intent.putExtra("data", data);
                                        context.startActivity(intent);
                                    }
                                });
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    holder.ImageRightrightImageChatFileName.setText(receivedFileName);
                    //textFilename = rightImageChatFileName.getText().toString();


                    if (messageFrom != null && messageFrom.equalsIgnoreCase("system_message")) {
                        // viewImageMsgRight.setVisibility(View.GONE);
                    }

                    holder.ImageRighttimeRightImage.setText(messageTime);


                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }


        } else {

/***************************** chat text content left ************************************/

            if (senderId == null || !(senderId.equalsIgnoreCase(String.valueOf(loginCredentials.getAdminId())))) {
               /* String localName = mainActivity.getNickName(context, senderId);
                if (localName != null)
                    messageFrom = localName;*/
                //setChatContentLeft(messageBody, messageFrom, messageTime);
                holder.chatContentLeftLayout.setVisibility(View.VISIBLE);


                // code for decryption


                String Chat_text = null;
                try {
                    // messageBody = URLDecoder.decode(messageBody, "UTF-8");
                    messageBody = messageBody.replace("\n", "").replace("\r", "");
                    //  messageBody = StringEscapeUtils.unescapeJava(messageBody);


                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                holder.chatTextMessageLeft.setText(messageBody);

                if (messageFrom != null && !messageFrom.equalsIgnoreCase("system_message")) {
                    holder.chatOwnerNameLeft.setText(messageFrom);
                }
                holder.chatTextMsgTimeLeft.setText(messageTime);


                holder.chatContentLeftLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String parentData = chatMessageEntity.Option2Value;
                        try {
                            if (parentData != null && parentData.length() > 2 && getSelectedItemCount() == 0) {
                                JSONObject jsonObject = new JSONObject(parentData);
                                Log.i("val ", "position for scroll " + Integer.parseInt(jsonObject.get("Position").toString()));

                                ((ChatMessagesActivity) context).scroll(Integer.parseInt(jsonObject.get("Position").toString()));

                            } else {
                                listener.onMessageRowClicked(position);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //  Toast.makeText(activity,"position is "+position,Toast.LENGTH_LONG).show();
                    }
                });

                holder.chatContentLeftLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        listener.onRowLongClicked(position);
                        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                        return true;
                    }
                });
/*
                holder.leftChatReplyLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String parentData = chatMessageEntity.Option2Value;
                        try {
                            if(parentData!=null && parentData.length()>2 && getSelectedItemCount()==0) {
                                JSONObject jsonObject = new JSONObject(parentData);
                                Log.i("val ","position for scroll "+Integer.parseInt(jsonObject.get("Position").toString()));
                                ((ChatMessagesActivity)activity).scroll(Integer.parseInt(jsonObject.get("Position").toString()));
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
*/
                // change the row state to activated
                holder.itemView.setActivated(selectedItems.get(position, false));
                try {
                    String parentData = chatMessageEntity.Option2Value;
                    Log.i("parentdata", "parent data is" + parentData);
                    if (parentData != null && parentData.length() > 2) {
                        holder.leftChatReplyLayout.setVisibility(View.VISIBLE);
                        JSONObject jsonObject = new JSONObject(parentData);
                        if (jsonObject.get("MessageType").toString().equalsIgnoreCase("Message")) {
                            holder.replylefttextviewlayout.setVisibility(View.VISIBLE);
                            try {
                                if (String.valueOf(loginCredentials.getAdminId()).equalsIgnoreCase(jsonObject.get("MessageSenderId").toString())) {
                                    holder.replyfromleft.setText("You");
                                    holder.replyfromleft.setTextColor(context.getResources().getColor(R.color.AppThemeColor));
                                    holder.left_reply_left_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_apptheme));

                                } else {
                                    holder.replyfromleft.setText(messageFrom);
                                    holder.replyfromleft.setTextColor(context.getResources().getColor(R.color.chatgreen));
                                    holder.left_reply_left_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_green));


                                }
                            } catch (Exception e) {
                                holder.replyfromleft.setText(jsonObject.get("MessageSenderName").toString());
                                e.printStackTrace();
                            }
                            String value = mainActivity.getDecryptedText(jsonObject.get("MessageBody").toString());
                            /// String data = value.replace("+"," ");
                            // String finaldata = data.replace("%0A"," ");
                            try {
                                value = value.replace("\n", "").replace("\r", "");
                                // value = URLDecoder.decode(value, "UTF-8");
                                //  value = StringEscapeUtils.unescapeJava(value);

                                //   finaldata = URLDecoder.decode(finaldata, "UTF-8");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            holder.replymessageleft.setText(value);

                        } else {
                            holder.replyleftimageviewlayout.setVisibility(View.VISIBLE);
                            //  String[] messageContent = jsonObject.get("MessageBody").toString().split("\\,");
                            //  mainMessage = messageContent[1];
                            //  receivedFileName = messageContent[0];
                            String val = null;
                            try {
                                val = jsonObject.get("MessageBody").toString();
                            } catch (Exception e) {
                                if (StringUtils.isEmpty(val)) {
                                    val = jsonObject.get("ImagePath").toString();
                                }
                            }

                            try {


                                if (val != null && val.equalsIgnoreCase(jsonObject.get("MessageBody").toString()) && jsonObject.get("ImagePath").toString() != null && !val.equalsIgnoreCase(jsonObject.get("ImagePath").toString())) {
                                    val = jsonObject.get("ImagePath").toString();
                                }
                            } catch (Exception e) {

                            }
                            try {
                                if (String.valueOf(loginCredentials.getAdminId()).equalsIgnoreCase(jsonObject.get("MessageSenderId").toString())) {
                                    holder.replayimagefromleft.setText("You");
                                    holder.replayimagefromleft.setTextColor(context.getResources().getColor(R.color.AppThemeColor));
                                    holder.left_reply_left_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_apptheme));

                                } else {
                                    holder.replayimagefromleft.setText(messageFrom);
                                    holder.replayimagefromleft.setTextColor(context.getResources().getColor(R.color.chatgreen));
                                    holder.left_reply_left_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_green));
                                }
                            } catch (Exception e) {
                                holder.replayimagefromleft.setText(jsonObject.get("MessageSenderName").toString());
                                e.printStackTrace();
                            }

                            String docFPath = null;
                            Bitmap bitmap;
                            try {
                                //   String stringNoQuotes = mainMessage.replaceAll("^\"|\"$", "");
                                //   docFPath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy /BM Pharmacy  Documents/" + receivedFileName;


                                Log.i("reply message ", "reply message body value" + val);
                                if (val != null) {
                                    val = val.replace("\"", "");
                                    if (val.endsWith(".jpg") || val.endsWith(".jpeg") || val.endsWith(".png")) {

                                        Picasso.with(context)
                                                .load(val.trim())
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("pdf")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pdf_download)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("doc")) {
                                        Picasso.with(context)
                                                .load(R.drawable.docfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("docx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.docxfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("ppt")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pptfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("pptx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pptxfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("xls")) {
                                        Picasso.with(context)
                                                .load(R.drawable.xlsfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("xlsx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.xlsxfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith("txt")) {
                                        Picasso.with(context)
                                                .load(R.drawable.txtfile)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith(".map")) {
                                        Picasso.with(context)
                                                .load(R.drawable.map_new)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith(".mp3")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_audio)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith(".mp4")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_audio)
                                                .into(holder.replyimageviewleft);

                                    } else if (val.toString().endsWith(".contact")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_contact)
                                                .into(holder.replyimageviewleft);
                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }
                } catch (Exception e) {
                    Log.i("error", "could not parse json");
                    e.printStackTrace();
                }


            } else {

//******************************** chat text content right ****************************************/

                /// setChatContentRight(messageBody, messageTime);
                holder.chatContentRightLayout.setVisibility(View.VISIBLE);

                String Chat_text = null;
                try {
                    Log.i("tag", "server decoded before" + Chat_text);
                    messageBody = messageBody.replace("\n", "").replace("\r", "");
                    Chat_text = messageBody;

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                holder.chatTextMessageRight.setText(Chat_text);
                if (messageTime == null)
                    holder.chatTextMsgTimeRight.setText("just now");
                else
                    holder.chatTextMsgTimeRight.setText(messageTime);

/*
                    holder.replyrighttextviewlayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            String parentData = chatMessageEntity.Option2Value;
                            try {
                                Log.i("click on reply","click on reply"+parentData);
                                Log.i("click on reply","click on reply count"+getSelectedItemCount());

                                if(parentData!=null && parentData.length()>2 && getSelectedItemCount()==0) {
                                    JSONObject jsonObject = new JSONObject(parentData);
                                    Log.i("val ","position for scroll "+Integer.parseInt(jsonObject.get("Position").toString()));
                                    ((ChatMessagesActivity)activity).scroll(Integer.parseInt(jsonObject.get("Position").toString()));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
*/

                holder.chatContentRightLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String parentData = chatMessageEntity.Option2Value;
                        try {
                            if (parentData != null && parentData.length() > 2 && getSelectedItemCount() == 0) {
                                JSONObject jsonObject = new JSONObject(parentData);
                                Log.i("val ", "position for scroll " + Integer.parseInt(jsonObject.get("Position").toString()));

                                ((ChatMessagesActivity) context).scroll(Integer.parseInt(jsonObject.get("Position").toString()));

                            } else {
                                listener.onMessageRowClicked(position);

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //   Toast.makeText(activity,"position is "+position,Toast.LENGTH_LONG).show();
                    }
                });


                holder.chatContentRightLayout.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        listener.onRowLongClicked(position);
                        view.performHapticFeedback(HapticFeedbackConstants.LONG_PRESS);
                        return true;
                    }
                });


                // change the row state to activated
                holder.itemView.setActivated(selectedItems.get(position, false));

                try {
                    String parentData = chatMessageEntity.Option2Value;
                    if (parentData != null && parentData.length() > 2) {
                        holder.rightChatReplyLayout.setVisibility(View.VISIBLE);
                        JSONObject jsonObject = new JSONObject(parentData);
                        if (jsonObject.get("MessageType").toString().equalsIgnoreCase("Message")) {
                            holder.replyrighttextviewlayout.setVisibility(View.VISIBLE);

                            try {
                                if (String.valueOf(loginCredentials.getAdminId()).equalsIgnoreCase(jsonObject.get("MessageSenderId").toString())) {
                                    holder.replyfrom.setText("You");
                                    //   holder.replyfrom.setTextColor(Color.parseColor("#8CC63E"));

                                    holder.replyfrom.setTextColor(context.getResources().getColor(R.color.AppThemeColor));
                                    holder.right_reply_right_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_apptheme));

                                } else {
                                    holder.replyfrom.setText(messageFrom);
                                    holder.replyfrom.setTextColor(context.getResources().getColor(R.color.chatgreen));
                                    holder.right_reply_right_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_green));


                                }
                            } catch (Exception e) {
                                holder.replyfrom.setText(jsonObject.get("MessageSenderName").toString());
                                e.printStackTrace();
                            }
                            String value = mainActivity.getDecryptedText(jsonObject.get("MessageBody").toString());
                            value = value.replace("\n", "").replace("\r", "");
                            try {


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            holder.replymessage.setText(value);
                        } else {
                            holder.replyrightimageviewlayout.setVisibility(View.VISIBLE);
                            // String[] messageContent = jsonObject.get("MessageBody").toString().split("\\,");
                            // mainMessage = messageContent[1];
                            // receivedFileName = messageContent[0];


                            String val = null;
                            try {
                                val = jsonObject.get("MessageBody").toString();
                            } catch (Exception e) {
                                if (TextUtils.isEmpty(val)) {
                                    val = jsonObject.get("ImagePath").toString();
                                }
                            }
                            try {


                                if (val != null && val.equalsIgnoreCase(jsonObject.get("MessageBody").toString()) && jsonObject.get("ImagePath").toString() != null && !val.equalsIgnoreCase(jsonObject.get("ImagePath").toString())) {
                                    val = jsonObject.get("ImagePath").toString();
                                }
                            } catch (Exception e) {

                            }

                            try {
                                if (String.valueOf(loginCredentials.getAdminId()).equalsIgnoreCase(jsonObject.get("MessageSenderId").toString())) {
                                    holder.replyimagefrom.setText("You");
                                    holder.replyimagefrom.setTextColor(context.getResources().getColor(R.color.AppThemeColor));
                                    holder.right_reply_right_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_apptheme));


                                } else {
                                    holder.replyimagefrom.setText(messageFrom);
                                    holder.replyimagefrom.setTextColor(context.getResources().getColor(R.color.chatgreen));
                                    holder.right_reply_right_border.setBackground(context.getResources().getDrawable(R.drawable.radiusonlyleft_green));


                                }
                            } catch (Exception e) {
                                holder.replyimagefrom.setText(jsonObject.get("MessageSenderName").toString());

                                e.printStackTrace();
                            }


                            String docFPath = null;
                            Bitmap bitmap;
                            try {
                                // String stringNoQuotes = mainMessage.replaceAll("^\"|\"$", "");
                                // docFPath = Environment.getExternalStorageDirectory().toString() + "/BM Pharmacy /BM Pharmacy  Documents/" + receivedFileName;

                                if (val != null) {
                                    val = val.replace("\"", "");
                                    if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg")) {
                                        Picasso.with(context)
                                                .load(val.trim())
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("pdf")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pdf_download)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("doc")) {
                                        Picasso.with(context)
                                                .load(R.drawable.docfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("docx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.docxfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("ppt")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pptfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("pptx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.pptxfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("xls")) {
                                        Picasso.with(context)
                                                .load(R.drawable.xlsfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("xlsx")) {
                                        Picasso.with(context)
                                                .load(R.drawable.xlsxfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith("txt")) {
                                        Picasso.with(context)
                                                .load(R.drawable.txtfile)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith(".map")) {
                                        Picasso.with(context)
                                                .load(R.drawable.map_new)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith(".mp3")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_audio)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith(".mp4")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_audio)
                                                .into(holder.replyimageview);

                                    } else if (val.toString().endsWith(".contact")) {
                                        Picasso.with(context)
                                                .load(R.drawable.bottom_sheet_contact)
                                                .into(holder.replyimageview);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    }
                } catch (Exception e) {
                    Log.i("error", "could not parse json");
                    e.printStackTrace();
                }


            }


        }
    }


    /**
     * Update timer on seekbar
     */
    public void updateProgressBar() {
        //   mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    /**
     * Background Runnable thread
     */
    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = player.getDuration();
            long currentDuration = player.getCurrentPosition();

            // Displaying Total Duration time
            //  songTotalDurationLabel.setText(""+utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            //  songCurrentDurationLabel.setText(""+utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);


            //  songProgressBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };


    public void toggleSelection(int pos) {
        currentSelectedIndex = pos;
        if (selectedItems.get(pos, false)) {
            selectedItems.delete(pos);

        } else {
            selectedItems.put(pos, true);
        }

        notifyItemChanged(pos);
    }


    private void resetCurrentIndex() {
        currentSelectedIndex = -1;
    }

    public SparseBooleanArray getSelectedItemsListData() {
        return selectedItems;
    }

    public void clearSelections() {
        //  reverseAllAnimations = true;
        selectedItems.clear();

        notifyDataSetChanged();
    }

    public int getSelectedItemCount() {
        return selectedItems.size();
    }

    @Override
    public int getItemCount() {

        return (chatMessageEntityList != null) ? chatMessageEntityList.size() : 0;
    }

    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView name;
        LinearLayout linearLayout;
        RelativeLayout chatContentLeftLayout, chatContentRightLayout, chatImageLeftLayout, chatImageRightLayout, centerChatLayout;
        TextView chatTextMessageRight, chatOwnerNameRight, chatTextMsgTimeRight;
        TextView chatTextMessageLeft, chatOwnerNameLeft, chatTextMsgTimeLeft;

        // image left
        ImageView ImageLeftchatContentImageLeft, ImageLeftchatContentDocumentLeft, ImageLeftchatContentMapLeft;
        VideoView ImageLeftchatContentVideoLeft;
        TextView ImageLeftImage_name, ImageLeftchatContentImageLeftName, ImageLefttimeLeftImage;
        RelativeLayout ImageLeftaudioLayout;
        ImageButton ImageLeftstart;
        LinearLayout mapLeftLayout;
        TextView ImageLeftChatContentMapText, ImageLeftChatContentMapTextSmall;
        SeekBar leftSideSongSeekbar;

        RelativeLayout contactLeftLayout, left_reply_left_border;
        TextView contactLeftName;
        TextView viewContactLeft;

        // image right
        ImageView ImageRightchatContentImageRight, ImageRightchatContentDocumentRight, ImageRightchatContentMapRight;
        // VideoView ImageRightchatContentVideoRight;
        TextView ImageRightrightImageChatFileName, ImageRightchatContentImageRightName, ImageRighttimeRightImage, ImageRightMessageTimeLong;
        RelativeLayout ImageRightaudioLayout;
        ImageButton ImageRightstart;
        TextView ImageRightChatContentMapText, ImageRightChatContentMapTextSmall;
        LinearLayout mapRightLayout;
        SeekBar rightSideSongSeekbar;
        TextView audioDuration;

        RelativeLayout contactRightLayout;
        TextView contactRightName;
        TextView viewContactRight;


        TextView systemMessageBubble;


        LinearLayout leftChatReplyLayout, rightChatReplyLayout;
        TextView replyfrom, replymessage;

        TextView replyfromleft, replymessageleft;
        LinearLayout replylefttextviewlayout, replyleftimageviewlayout;
        TextView replayimagefromleft;
        ImageView replyimageviewleft;
        RelativeLayout right_reply_right_border;


        LinearLayout replyrighttextviewlayout, replyrightimageviewlayout;
        TextView replyimagefrom;
        ImageView replyimageview;

        public CustomViewHolder(View itemView) {
            super(itemView);

            chatContentLeftLayout = (RelativeLayout) itemView.findViewById(R.id.leftchatLayout);
            chatTextMessageRight = (RobotoTextView) itemView.findViewById(R.id.chat_bubble_right);
            chatOwnerNameRight = (RobotoTextView) itemView.findViewById(R.id.chatOwnerNameRight);
            chatTextMsgTimeRight = (RobotoTextView) itemView.findViewById(R.id.time_right);

            chatContentRightLayout = (RelativeLayout) itemView.findViewById(R.id.rightchatLayout);
            chatTextMessageLeft = (RobotoTextView) itemView.findViewById(R.id.chat_bubble_left);
            chatOwnerNameLeft = (RobotoTextView) itemView.findViewById(R.id.chatOwnerNameLeft);
            chatTextMsgTimeLeft = (RobotoTextView) itemView.findViewById(R.id.time_left);


            chatImageLeftLayout = (RelativeLayout) itemView.findViewById(R.id.ImageLeftleftchatLayout);
            ImageLeftchatContentImageLeft = (ImageView) itemView.findViewById(R.id.ImageLeftchat_bubble_image);
            ImageLeftchatContentVideoLeft = (VideoView) itemView.findViewById(R.id.ImageLeftchat_bubble_video);
            ImageLeftImage_name = (TextView) itemView.findViewById(R.id.ImageLeftchatimage_text);
            ImageLeftchatContentImageLeftName = (RobotoTextView) itemView.findViewById(R.id.ImageLeftchatOwnerNameLeft);
            ImageLefttimeLeftImage = (RobotoTextView) itemView.findViewById(R.id.ImageLefttime_image);
            ImageLeftaudioLayout = (RelativeLayout) itemView.findViewById(R.id.ImageLeftaudioLayout);
            ImageLeftstart = (ImageButton) itemView.findViewById(R.id.ImageLeftstart);
            ImageLeftchatContentDocumentLeft = (ImageView) itemView.findViewById(R.id.ImageLeftchat_document_image);
            ImageLeftchatContentMapLeft = (ImageView) itemView.findViewById(R.id.ImageLeftchat_map_image);
            ImageLeftChatContentMapText = (TextView) itemView.findViewById(R.id.ImageLeftChat_map_text);
            ImageLeftChatContentMapTextSmall = (TextView) itemView.findViewById(R.id.ImageLeftChat_map_text_small);
            mapLeftLayout = (LinearLayout) itemView.findViewById(R.id.map_left_layout);
            leftSideSongSeekbar = (SeekBar) itemView.findViewById(R.id.ImageLeftseekbar);
            contactLeftLayout = (RelativeLayout) itemView.findViewById(R.id.contact_left_parent_layout);
            contactLeftName = (TextView) itemView.findViewById(R.id.contact_left_name);
            viewContactLeft = (TextView) itemView.findViewById(R.id.left_contact_textview);


            chatImageRightLayout = (RelativeLayout) itemView.findViewById(R.id.ImageRightimagechatLayout);
            ImageRightchatContentImageRight = (ImageView) itemView.findViewById(R.id.ImageRightchat_bubble_image);
            //  ImageRightchatContentVideoRight = (VideoView) itemView.findViewById(R.id.ImageRightchat_bubble_video);
            ImageRightrightImageChatFileName = (RobotoTextView) itemView.findViewById(R.id.ImageRightchatimage_text);
            ImageRightchatContentImageRightName = (RobotoTextView) itemView.findViewById(R.id.ImageRightchatOwnerNameRight);
            ImageRighttimeRightImage = (RobotoTextView) itemView.findViewById(R.id.ImageRighttime_image);
            ImageRightMessageTimeLong = (TextView) itemView.findViewById(R.id.ImageRightmessageTimeLong);
            ImageRightaudioLayout = (RelativeLayout) itemView.findViewById(R.id.ImageRightaudioLayout);
            ImageRightstart = (ImageButton) itemView.findViewById(R.id.ImageRightstart);
            ImageRightchatContentDocumentRight = (ImageView) itemView.findViewById(R.id.ImageRightchat_document_image);
            ImageRightchatContentMapRight = (ImageView) itemView.findViewById(R.id.ImageRightchat_map_image);
            ImageRightChatContentMapText = (TextView) itemView.findViewById(R.id.ImageRightChat_map_text);
            ImageRightChatContentMapTextSmall = (TextView) itemView.findViewById(R.id.ImageRightChat_map_text_small);
            mapRightLayout = (LinearLayout) itemView.findViewById(R.id.map_right_layout);
            rightSideSongSeekbar = (SeekBar) itemView.findViewById(R.id.ImageRightseekbar);
            audioDuration = (TextView) itemView.findViewById(R.id.audio_right_duration);
            contactRightLayout = (RelativeLayout) itemView.findViewById(R.id.contact_right_parent_layout);
            contactRightName = (TextView) itemView.findViewById(R.id.contact_right_name);
            viewContactRight = (TextView) itemView.findViewById(R.id.right_contact_textview);

            centerChatLayout = (RelativeLayout) itemView.findViewById(R.id.CenterleftchatLayout);
            systemMessageBubble = (TextView) itemView.findViewById(R.id.system_message_bubble);

            //reply

            replyfrom = (RobotoTextView) itemView.findViewById(R.id.replayfrom);
            replymessage = (RobotoTextView) itemView.findViewById(R.id.replymessage);

            leftChatReplyLayout = (LinearLayout) itemView.findViewById(R.id.leftChatReplyLayout);
            rightChatReplyLayout = (LinearLayout) itemView.findViewById(R.id.rightChatReplyLayout);


            replyrighttextviewlayout = (LinearLayout) itemView.findViewById(R.id.replyrighttextviewlayout);
            replyrightimageviewlayout = (LinearLayout) itemView.findViewById(R.id.replyrightimageviewlayout);

            replyimagefrom = (TextView) itemView.findViewById(R.id.replayfromimageright);
            replyimageview = (ImageView) itemView.findViewById(R.id.replyfromimageviewright);


            //left

            replyfromleft = (TextView) itemView.findViewById(R.id.replayfromleft);
            replymessageleft = (TextView) itemView.findViewById(R.id.replymessageleft);

            replylefttextviewlayout = (LinearLayout) itemView.findViewById(R.id.replylefttextviewlayout);
            replyleftimageviewlayout = (LinearLayout) itemView.findViewById(R.id.replyleftimageviewlayout);

            replayimagefromleft = (TextView) itemView.findViewById(R.id.replayfromimageleft);
            replyimageviewleft = (ImageView) itemView.findViewById(R.id.replyfromimageviewleft);
            left_reply_left_border = (RelativeLayout) itemView.findViewById(R.id.left_reply_left_border);
            right_reply_right_border = (RelativeLayout) itemView.findViewById(R.id.right_reply_right_border);
        }

    }


    /**** getBitmap from url ***********/

    Target target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Log.i("BitmapLoaded", "prepare" + bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {
            Log.i("BitmapLoadFailed", "failed");
        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {
            Log.i("BitmapPreparee", "prepare");
        }
    };

    public void showImageFromUrl(String src) {
        try {

            Dialog dialog = new Dialog(context, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_image_viewer);

            dialog.setCanceledOnTouchOutside(true);

            ImageView imageView = (ImageView) dialog.findViewById(R.id.ImageView);
            Glide.with(context).load(src).placeholder(R.drawable.loadingimage) .diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 /*   public Bitmap getBitmapFromURL(String src) {
        try {

            Dialog dialog = new Dialog(context, R.style.MyAlertDialogStyle);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.activity_image_viewer);

            dialog.setCanceledOnTouchOutside(true);

            ImageView imageView = (ImageView) dialog.findViewById(R.id.ImageView);
            Glide.with(context).load(src).into(imageView);


           *//* Bitmap mBitmap;
            Picasso.Builder builder = new Picasso.Builder(context);
            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception) {
                    exception.printStackTrace();
                }
            });

            return mBitmap = builder.build().with(context).load(src).get();*//*

            //  Picasso.with(context).load(src).into(target);
            // URL url = new URL(src);
          *//*  Log.i("tag", "url for bitmap is" + src);
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
*//*

           *//* Request listRequest = new Request.Builder().url(url).build();
            byte[] bytes = client.newCall(listRequest).execute().body().bytes();
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0,
                    bytes.length);*//*
            // return null;


        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/


    protected void showDownloadedImageFile(File getImageFilePath) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = getImageFilePath.getName()
                .substring(getImageFilePath.getName().indexOf(".") + 1)
                .toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);
        intent.setDataAndType(Uri.fromFile(getImageFilePath), type);
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    context,
                    "Please install proper image reader in your device to open this file",
                    Toast.LENGTH_LONG).show();
        }

    }


    protected void showDownloadedDocumentFile(File getDocumentFilePath) {
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(getDocumentFilePath
                .getName());
        String type = map.getMimeTypeFromExtension(ext);
        if (type == null)
            type = "*/*";
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(getDocumentFilePath), type);
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(
                    context,
                    "Please install proper document reader in your device to open this file",
                    Toast.LENGTH_LONG).show();
        }

    }


    public Bitmap compressImage(String filePath) {
        Bitmap scaledBitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Log.i("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.i("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.i("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.i("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            try {
                //  out = new FileOutputStream(filePath);
                // scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return scaledBitmap;
    }


    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        try {
            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inSampleSize;
    }


    public void refresh(ChatMessageEntity chatMessageEntity) {
        chatMessageEntityList.add(chatMessageEntity);
        notifyDataSetChanged();

    }


    public void refreshAll(List<ChatMessageEntity> chatMessageEntityList) {
        if (chatMessageEntityList.size() > this.chatMessageEntityList.size()) {

            List<ChatMessageEntity> chatMessageList = new ArrayList<ChatMessageEntity>();
            chatMessageList = chatMessageEntityList;
            this.chatMessageEntityList.clear();
            this.chatMessageEntityList.addAll(chatMessageList);
        } else {
            this.chatMessageEntityList.get(chatMessageEntityList.size() - 1).Id = chatMessageEntityList.get(chatMessageEntityList.size() - 1).Id;
            if (chatMessageEntityList.get(chatMessageEntityList.size() - 1).ImagePath != null) {
                this.chatMessageEntityList.get(chatMessageEntityList.size() - 1).ImagePath = chatMessageEntityList.get(chatMessageEntityList.size() - 1).ImagePath;
            }
        }
        //this.chatMessageEntityList = chatMessageEntityList;
        notifyDataSetChanged();
    }


    public interface ChatListAdapterListener {
        void onIconClicked(int position);

        void onIconImportantClicked(int position);

        void onMessageRowClicked(int position);

        void onRowLongClicked(int position);
    }


}


   




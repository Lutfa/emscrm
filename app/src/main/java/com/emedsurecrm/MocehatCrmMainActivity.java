package com.emedsurecrm;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.emedsurecrm.model.ChatMessageEntity;
import com.emedsurecrm.model.MyCustomerEntity;
import com.emedsurecrm.model.UserKeyDetails;
import com.emedsurecrm.utils.MocehatCrmDBHelper;
import com.google.gson.Gson;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 * Created by ahmed on 9/15/2017.
 */

public class MocehatCrmMainActivity extends Activity {
    public static int dbVersionNumber = 0;
    MocehatCrmDBHelper dbHelper;
    String activityJson = null;
    Gson gson = new Gson();


    //        public static String WEB_API_URL = "http://192.168.1.10:8097/api/";  //Testing
    public static String WEB_API_URL = "http://4287-25688.el-alt.com/api/";  //EMedSure
    public static String PasswordSalt = "4s4tz+0=";
    public static String PasswordRecoveryLinkProd = "http://www.emedsure.com/passwordrecovery"; //Production
    public static String PasswordRecoveryLinkTest = "http://192.168.1.10:8082/passwordrecovery"; //testing


    public static String[] PERMISSIONS_PHONESTATE = {"android.permission.READ_PHONE_STATE"};
    public static final int PERMISSIONS_PHONESTATE_REQUEST_CODE = 2;

    public boolean isInternetConnected(Context context) {
        boolean isConnected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(context.CONNECTIVITY_SERVICE);
        if (connectivityManager != null) {
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        isConnected = true;
                        break;
                    }
                }

            }

        }

        return isConnected;
    }

    public void hideSoftKeyboard(Context context, View view) {
        try {
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            if (getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            }

            // inputMethodManager.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void show_snakbar(String message, final View view) {
        try {
            Snackbar snackbar = Snackbar
                    .make(view, message, Snackbar.LENGTH_LONG)
                    .setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Snackbar snackbar1 = Snackbar.make(view, "Message is restored!", Snackbar.LENGTH_SHORT);
                            snackbar1.dismiss();
                        }
                    });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(Color.YELLOW);
            snackbar.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void savecustomerDetails(Context context, MyCustomerEntity customerChatDetails) {

        dbHelper = new MocehatCrmDBHelper(context);
        try {
            dbHelper.saveCustomerDetails(customerChatDetails);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void savepharmacyDetails(Context context, MyCustomerEntity customerChatDetails) {

        dbHelper = new MocehatCrmDBHelper(context);
        try {
            dbHelper.savepharmacyDetails(customerChatDetails);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public ArrayList<MyCustomerEntity> getCustomerChatList(Context activity, String searchText) {
        dbHelper = new MocehatCrmDBHelper(activity);
        ArrayList<MyCustomerEntity> customerChatList = new ArrayList<MyCustomerEntity>();
        try {
            customerChatList = dbHelper.getCustomerChatDetailsList(searchText);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return customerChatList;
    }

    public ArrayList<MyCustomerEntity> getPharmacyChatList(Context activity, String searchText) {
        dbHelper = new MocehatCrmDBHelper(activity);
        ArrayList<MyCustomerEntity> customerChatList = new ArrayList<MyCustomerEntity>();
        try {
            customerChatList = dbHelper.getPharmacyChatDetailsList(searchText);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return customerChatList;
    }

    public String getDecryptedText(String data) {
        try {
            Log.i("tag", "data is" + data);
            CryptLib _crypt = null;
            String key = "";
            try {
                _crypt = new CryptLib();
                key = CryptLib.SHA256("MoCehat", 32); //32 bytes = 256 bit
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            String mainbody = "", iv = "";

            Log.i("data", "data to check" + data);
            if (data != null && data.toString().length() > 0) {
                if (data.contains("separator")) {
                    try {

                        String mbody = data;
                        try {
                            mainbody = mbody.split("separator")[0].toString();
                            iv = mbody.split("separator")[1].toString();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            data = _crypt.decrypt(mainbody, key, iv); //decrypt
                        } catch (InvalidKeyException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (InvalidAlgorithmParameterException e) {
                            e.printStackTrace();
                        } catch (IllegalBlockSizeException e) {
                            e.printStackTrace();
                        } catch (BadPaddingException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        data = "";
                        return data;
                    }

                } else {
                    return data;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        return data;

    }

    public List<ChatMessageEntity> getAdminChatActivityMessages(Context activity, String ReceiverId) {
        dbHelper = new MocehatCrmDBHelper(activity);
        List<ChatMessageEntity> adminactivityMessages = new ArrayList<ChatMessageEntity>();
        try {
            adminactivityMessages = dbHelper.GetAdminChatActivityMessage(ReceiverId);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return adminactivityMessages;
    }

    public String fromDotNetTicks(long ticks) {
        String timeDelta = new String();
        try {

           /* String toParse = "2-5-2014 20:40"; // Results in "2-5-2012 20:43"
            SimpleDateFormat formatter = new SimpleDateFormat("d-M-yyyy hh:mm"); // I assume d-M, you may refer to M-d for month-day instead.
            Date date = formatter.parse(toParse); // You will need try/catch around this
            long currenTime = date.getTime();
            ticks = 621355968000000000L + currenTime * 10000;*/

            long TICKS_AT_EPOCH = 621355968000000000L;
            long millis = (ticks - TICKS_AT_EPOCH) / (10000);
            timeDelta = deltaTime(millis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return timeDelta;

    }

    public String deltaTime(long timeInMilliSeconds) {
        String timeDiff = new String();
        String min, hours;
        final int SECOND = 1;
        final int MINUTE = 60 * SECOND;
        final int HOUR = 60 * MINUTE;
        final int DAY = 24 * HOUR;
        final int MONTH = 30 * DAY;
        final int YEAR = 12 * MONTH;
        int twoDigitsDate = 0, twoDigitsMonth = 0;
        String twoDigitDateString = "", twoDigitsMonthString = "";
        try {
            TimeZone localTimeZone = TimeZone.getDefault();
            Calendar calendar = Calendar.getInstance(localTimeZone,
                    Locale.ENGLISH);
            long currenTime = System.currentTimeMillis();
            long diffInSeconds = ((currenTime - timeInMilliSeconds) / 1000);

            if (calendar.get(Calendar.MINUTE) < 10)
                min = "0" + calendar.get(Calendar.MINUTE);
            else
                min = String.valueOf(calendar.get(Calendar.MINUTE));

            if (calendar.get(Calendar.HOUR) < 10)
                hours = "0" + calendar.get(Calendar.HOUR);
            else
                hours = String.valueOf(calendar.get(Calendar.HOUR));

            if (diffInSeconds > 0) {
                if (diffInSeconds > 1 && diffInSeconds < MINUTE)
                    timeDiff = diffInSeconds + " seconds ago";
                else if (diffInSeconds > MINUTE && diffInSeconds < HOUR)
                    timeDiff = Math.round(diffInSeconds / MINUTE)
                            + " minutes ago";

                else if (diffInSeconds > HOUR && diffInSeconds < DAY) {
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                    //timeDiff = "at " + hours + ":" + min + " " + pm;
                    timeDiff = hours + ":" + min + " " + pm;

                } else if (diffInSeconds > DAY && diffInSeconds < MONTH) {
                    // timeDiff = Math.round(diffInSeconds / DAY) +
                    // " day(s)  ago";

                    calendar = Calendar.getInstance(localTimeZone,
                            Locale.ENGLISH);
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);
                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;

                } else if (diffInSeconds > MONTH && diffInSeconds < YEAR) {
                    // timeDiff = Math.round(diffInSeconds / DAY) +
                    // " day(s)  ago";
                    calendar = Calendar.getInstance(localTimeZone,
                            Locale.ENGLISH);
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);

                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;

                } else if (diffInSeconds > YEAR) {
                    // timeDiff = Math.round(diffInSeconds / DAY) +
                    // " day(s)  ago";
                    calendar = Calendar.getInstance(localTimeZone,
                            Locale.ENGLISH);
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);
                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;

                } else if (diffInSeconds < 1)
                    timeDiff = "";
            } else {
                diffInSeconds = -(diffInSeconds);
                if (diffInSeconds > 1 && diffInSeconds < MINUTE)
                    timeDiff = diffInSeconds + " seconds left";
                else if (diffInSeconds > MINUTE && diffInSeconds < HOUR)
                    timeDiff = Math.round(diffInSeconds / MINUTE)
                            + " minute to go";

                else if (diffInSeconds > HOUR && diffInSeconds < DAY) {
                    timeDiff = Math.round(diffInSeconds / HOUR)
                            + " hour(s)  to go";

                    calendar.setTimeInMillis(timeInMilliSeconds);
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                    //timeDiff = "at " + hours + ":" + min + " " + pm;
                    timeDiff = hours + ":" + min + " " + pm;

                } else if (diffInSeconds > DAY && diffInSeconds < MONTH) {
                    // timeDiff = Math.round(diffInSeconds / DAY)
                    // + " day(s)  to go";

                    calendar.setTimeInMillis(timeInMilliSeconds);
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);
                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;

                } else if (diffInSeconds > MONTH && diffInSeconds < YEAR) {
                    // timeDiff = Math.round(diffInSeconds / DAY) +
                    // " day(s)  ago";
                    calendar = Calendar.getInstance(localTimeZone,
                            Locale.ENGLISH);
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);
                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;

                } else if (diffInSeconds > YEAR) {
                    // timeDiff = Math.round(diffInSeconds / DAY) +
                    // " day(s)  ago";
                    calendar = Calendar.getInstance(localTimeZone,
                            Locale.ENGLISH);
                    calendar.setTimeInMillis(timeInMilliSeconds);
                    if (calendar.get(Calendar.MINUTE) < 10)
                        min = "0" + calendar.get(Calendar.MINUTE);
                    else
                        min = String.valueOf(calendar.get(Calendar.MINUTE));

                    if (calendar.get(Calendar.HOUR) < 10)
                        hours = "0" + calendar.get(Calendar.HOUR);
                    else
                        hours = String.valueOf(calendar.get(Calendar.HOUR));
                    String pm = "AM";
                    if (calendar.get(Calendar.AM_PM) == 1)
                        pm = "PM";
                   /* timeDiff = calendar.get(Calendar.DATE)
                            + dateAdditional(calendar.get(Calendar.DATE)) + " "
                            + getMonthForInt(calendar.get(Calendar.MONTH))
                            + ", " + hours + ":" + min + " " + pm;*/
                    twoDigitsDate = calendar.get(Calendar.DATE);
                    twoDigitsMonth = calendar.get(Calendar.MONTH);
                    if (twoDigitsDate > 0 && twoDigitsDate < 10)
                        twoDigitDateString = "0" + calendar.get(Calendar.DATE);
                    else
                        twoDigitDateString = String.valueOf(calendar.get(Calendar.DATE));

                    if (twoDigitsMonth > 0 && twoDigitsMonth < 10)
                        twoDigitsMonthString = "0" + calendar.get(Calendar.MONTH);
                    else
                        twoDigitsMonthString = String.valueOf(calendar.get(Calendar.MONTH));

                    timeDiff = twoDigitDateString + "/" + twoDigitsMonthString + "/"
                            + calendar.get(Calendar.YEAR) + ", " + hours + ":" + min + " " + pm;
                } else if (diffInSeconds < 1)
                    timeDiff = "";

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return timeDiff;
    }

    public String getLocalTimeToShow(String time) {

        Date orderDate = null;
        long timeInMilliseconds = 0;
        String timeinmillis = null;
        try {
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'h:mm:ss.SSS", Locale.ENGLISH);
            simpleDateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
            if (time != null && !time.equalsIgnoreCase("null")) {

                orderDate = simpleDateformat.parse(time);

                timeInMilliseconds = orderDate.getTime();

            }

        } catch (Exception ex) {
            try {

                SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'h:mm:ss", Locale.ENGLISH);
                simpleDateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
                if (time != null && !time.equalsIgnoreCase("null")) {

                    orderDate = simpleDateformat.parse(time);

                    timeInMilliseconds = orderDate.getTime();

                }
            } catch (Exception x) {

            }
        }
        final String dateTimeFormatString = "dd/MM/yyyy, h:mm a";
        String timezoneID = TimeZone.getDefault().getID();
        TimeZone tmzn = TimeZone.getTimeZone(timezoneID);
        Calendar cal = Calendar.getInstance(tmzn, Locale.ENGLISH);
        cal.setTimeInMillis(timeInMilliseconds);

        String localTime = DateFormat.format(dateTimeFormatString, cal).toString();
        return localTime;
    }

    public void SaveImage(Bitmap bitmap, byte[] byteArrayData, String filename, String imgActualPath) {
        File file = null, videoDirectory, AudioDirectory, galleryDirectory, directory, documentDirectory;
        String root;
        Bitmap localBitMap;
        FileOutputStream fos;
        boolean isGallery = false, isOthers = false;
        try {
            root = Environment.getExternalStorageDirectory().toString();
            directory = new File(root + "/MoCehatCRM");
            if (!directory.exists())
                directory.mkdirs();
            videoDirectory = directory.getParentFile();
            AudioDirectory = directory.getParentFile();
            galleryDirectory = directory.getParentFile();
            videoDirectory = new File(directory + "/MoCehatCRM Videos");
            AudioDirectory = new File(directory + "/MoCehatCRM Audio");
            galleryDirectory = new File(directory + "/MoCehatCRM Images");
            documentDirectory = new File(directory + "/MoCehatCRM Documents");

            if (!videoDirectory.exists())
                videoDirectory.mkdirs();
            if (!AudioDirectory.exists())
                AudioDirectory.mkdirs();
            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();
            if (!documentDirectory.exists())
                documentDirectory.mkdirs();

            if (filename != null) {
                if (filename.toString().endsWith(".jpg")
                        || filename.toString().endsWith(".png")
                        || filename.toString().endsWith(".jpeg")) {
                    file = new File(galleryDirectory, filename);
                    isGallery = true;
                } else if (filename.toString().endsWith(".pdf")
                        || filename.toString().endsWith(".doc")
                        || filename.toString().endsWith(".docx")
                        || filename.toString().endsWith(".ppt")
                        || filename.toString().endsWith(".pptx")
                        || filename.toString().endsWith(".xls")
                        || filename.toString().endsWith(".xlsx")
                        || filename.toString().endsWith(".txt")) {
                    file = new File(documentDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp3")
                        || filename.toString().endsWith(".ogg")
                        || filename.toString().endsWith(".m4a")
                        || filename.toString().endsWith(".amr")
                        || filename.toString().endsWith(".3gpp")) {
                    file = new File(AudioDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp4")
                        || filename.toString().endsWith(".3gp")
                        || filename.toString().endsWith(".avi")
                        || filename.toString().endsWith(".mkv")) {
                    file = new File(videoDirectory, filename);
                    isOthers = true;
                }
            }
            if (file.exists())
                file.delete();
            try {
                fos = new FileOutputStream(file, false);
                if (isGallery) {
                    if (imgActualPath != null) {
                        localBitMap = BitmapFactory.decodeFile(imgActualPath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byteArrayData = stream.toByteArray();
                        fos.write(byteArrayData);
                    } else if (byteArrayData != null) {
                        // bitmap = BitmapFactory.decodeFile(filpath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        // getByteArray = stream.toByteArray();
                        fos = new FileOutputStream(file, false);
                        fos.write(byteArrayData);
                        fos.flush();
                        fos.close();
                    } else
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos);
                } else if (isOthers) {
                    fos.write(byteArrayData);
                }
                fos.flush();
                fos.close();

            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }

    public String SaveImageGallery(Bitmap bitmap, byte[] byteArrayData, String filename, String imgActualPath) {
        File file = null, videoDirectory, AudioDirectory, galleryDirectory, directory, documentDirectory;
        String root;
        Bitmap localBitMap;
        FileOutputStream fos;
        boolean isGallery = false, isOthers = false;
        try {
            root = Environment.getExternalStorageDirectory().toString();
            directory = new File(root + "/MoCehatCRM");
            if (!directory.exists())
                directory.mkdirs();
            videoDirectory = directory.getParentFile();
            AudioDirectory = directory.getParentFile();
            galleryDirectory = directory.getParentFile();
            videoDirectory = new File(directory + "/MoCehatCRM Videos");
            AudioDirectory = new File(directory + "/MoCehatCRM Audio");
            galleryDirectory = new File(directory + "/MoCehatCRM Images");
            documentDirectory = new File(directory + "/MoCehatCRM Documents");

            if (!videoDirectory.exists())
                videoDirectory.mkdirs();
            if (!AudioDirectory.exists())
                AudioDirectory.mkdirs();
            if (!galleryDirectory.exists())
                galleryDirectory.mkdirs();
            if (!documentDirectory.exists())
                documentDirectory.mkdirs();

            if (filename != null) {
                if (filename.toString().endsWith(".jpg")
                        || filename.toString().endsWith(".png")
                        || filename.toString().endsWith(".jpeg")) {
                    file = new File(galleryDirectory, filename);
                    isGallery = true;
                } else if (filename.toString().endsWith(".pdf")
                        || filename.toString().endsWith(".doc")
                        || filename.toString().endsWith(".docx")
                        || filename.toString().endsWith(".ppt")
                        || filename.toString().endsWith(".pptx")
                        || filename.toString().endsWith(".xls")
                        || filename.toString().endsWith(".xlsx")
                        || filename.toString().endsWith(".txt")) {
                    file = new File(documentDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp3")
                        || filename.toString().endsWith(".ogg")
                        || filename.toString().endsWith(".m4a")
                        || filename.toString().endsWith(".amr")
                        || filename.toString().endsWith(".3gpp")) {
                    file = new File(AudioDirectory, filename);
                    isOthers = true;
                } else if (filename.toString().endsWith(".mp4")
                        || filename.toString().endsWith(".3gp")
                        || filename.toString().endsWith(".avi")
                        || filename.toString().endsWith(".mkv")) {
                    file = new File(videoDirectory, filename);
                    isOthers = true;
                }
            }
            if (file.exists())
                file.delete();
            try {
                fos = new FileOutputStream(file, false);
                if (isGallery) {
                    if (imgActualPath != null) {
                        localBitMap = BitmapFactory.decodeFile(imgActualPath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        localBitMap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        byteArrayData = stream.toByteArray();
                        fos.write(byteArrayData);
                    } else if (byteArrayData != null) {
                        // bitmap = BitmapFactory.decodeFile(filpath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        // bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                        // getByteArray = stream.toByteArray();
                        fos = new FileOutputStream(file, false);
                        fos.write(byteArrayData);
                        fos.flush();
                        fos.close();
                    } else
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 95, fos);
                } else if (isOthers) {
                    fos.write(byteArrayData);
                }
                fos.flush();
                fos.close();

                return galleryDirectory + "/" + filename;
            } catch (Exception e) {
                e.printStackTrace();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
        return "";
    }

    public void addsendermessaget(Context activity,
                                  ChatMessageEntity messageEntity) {
        dbHelper = new MocehatCrmDBHelper(activity);
        try {
            dbHelper.InsertActivitySenderMessages(messageEntity);

        } catch (Exception ex) {

        }

    }

    public String getCurrentTime(Context context) {

        String startDateTime = null;
        SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
        simpleDateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        Calendar calendar = Calendar.getInstance(Locale.ENGLISH);
        startDateTime = simpleDateformat.format(calendar.getTime());

        return startDateTime;
    }

    public int getNotificationToDB(Context context) {
        int unReadNotificationCount = 0;
        dbHelper = new MocehatCrmDBHelper(context);
        try {
            unReadNotificationCount = dbHelper.getNotification();
        } catch (Exception ex) {

        }
        return unReadNotificationCount;
    }

    public String getImageBasedOnMessageBody(String messagebody) {
        try {
            if (messagebody != null && messagebody.length() > 0) {
                if (messagebody.endsWith(".jpg") || messagebody.endsWith(".jpeg")
                        || messagebody.endsWith(".png") || messagebody.endsWith(".gif")) {
                    return "Photo";
                } else if (messagebody.toString().endsWith(".pdf")
                        || messagebody.toString().endsWith(".doc")
                        || messagebody.toString().endsWith(".docx")
                        || messagebody.toString().endsWith(".ppt")
                        || messagebody.toString().endsWith(".pptx")
                        || messagebody.toString().endsWith(".xls")
                        || messagebody.toString().endsWith(".xlsx")
                        || messagebody.toString().endsWith(".txt")) {
                    return "File";

                } else if (messagebody.endsWith(".contact")) {
                    return "Contact";
                } else if (messagebody.endsWith(".map")) {
                    return "Location";
                } else {
                    return "Text";
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "";
    }


    /* public String getLocalTimeToShow(String time) {

         Date orderDate = null;
         long timeInMilliseconds = 0;
         String timeinmillis = null;
         try {
             SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'h:mm:ss.SSS", Locale.ENGLISH);
             //simpleDateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
             if (time != null && !time.equalsIgnoreCase("null")) {
                 orderDate = simpleDateformat.parse(time);
                 timeInMilliseconds = orderDate.getTime();
             }
         } catch (Exception ex) {
             try {

                 SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd'T'h:mm:ss", Locale.ENGLISH);
                 //   simpleDateformat.setTimeZone(TimeZone.getTimeZone("UTC"));
                 if (time != null && !time.equalsIgnoreCase("null")) {

                     orderDate = simpleDateformat.parse(time);

                     timeInMilliseconds = orderDate.getTime();

                 }
             } catch (Exception x) {

             }
         }

         // final String dateTimeFormatString = "h:mm a";
         //final String dateTimeFormatString = "dd-MMM, h:mm a";

         *//*final String dateTimeFormatString = "yyyy-MM-dd, h:mm a";*//*
        // String timezoneID = TimeZone.getDefault().getID();
        // TimeZone tmzn = TimeZone.getTimeZone(timezoneID);
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(timeInMilliseconds);
        int day_of_month=cal.get(Calendar.DAY_OF_MONTH);
        String  suffix=getDayNumberSuffix(day_of_month);
        final String dateTimeFormatString = "MMM d'"+suffix+"', yyyy";
        String localTime = DateFormat.format(dateTimeFormatString, cal).toString();
        return localTime;
    }

    private String getDayNumberSuffix(int day) {
        if (day >= 11 && day <= 13) {
            return "th";
        }
        switch (day % 10) {
            case 1:
                return "st";
            case 2:
                return "nd";
            case 3:
                return "rd";
            default:
                return "th";
        }
    }*/

    public int getAdminChatCount(Context activity, String Id) {
        dbHelper = new MocehatCrmDBHelper(activity);
        int adminactivityReceiverMessages = 0;
        try {
            adminactivityReceiverMessages = dbHelper.GetAdminChatActivityMessageCount(Id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return adminactivityReceiverMessages;
    }

    public UserKeyDetails getTokenFromDb(Context context) {
        dbHelper = new MocehatCrmDBHelper(context);

        UserKeyDetails userKeyDetails = dbHelper.getTokenFromDB();
        return userKeyDetails;
    }
}

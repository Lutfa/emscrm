package com.emedsurecrm;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Poulav on 21-12-2015.
 */
public class NetworkUtils {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    public static int getConnectivityStatus(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            if (null != activeNetwork) {
                if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                    return TYPE_WIFI;

                if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                    return TYPE_MOBILE;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString(Context context) {
        String status = null;
        try {
            int conn = NetworkUtils.getConnectivityStatus(context);
            if (conn == NetworkUtils.TYPE_WIFI) {
                status = "Wifi enabled";
            } else if (conn == NetworkUtils.TYPE_MOBILE) {
                status = "Mobile data enabled";
            } else if (conn == NetworkUtils.TYPE_NOT_CONNECTED) {
                status = "Not connected to Internet";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return status;
    }

}

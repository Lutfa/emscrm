package com.emedsurecrm.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.emedsurecrm.MocehatCrmMainActivity;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.R;
import com.emedsurecrm.adapter.ChatFragmentAdapter;
import com.emedsurecrm.model.MyCustomerEntity;
import com.emedsurecrm.model.SharedPreferenceActivity;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Poulav on 28-07-2016.
 */
public class CustomerChatFragment extends Fragment {
    View rootView;
    ProgressDialog pdialogue;
    Gson gson = new Gson();
    ListView Listview;
    RobotoTextView detailDate, tv_current_date;
    MocehatCrmMainActivity mainActivity;
    ArrayList<MyCustomerEntity> mycustomerArrayList;
    ChatFragmentAdapter chatFragmentAdapter;
    private SwipeRefreshLayout swipeContainer;
    boolean isLive = false,active =false;
    Fragment fragment;
SharedPreferenceActivity sharedPreferenceActivity;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            rootView = inflater.inflate(R.layout.chat_list, container, false);
            Listview = (ListView) rootView.findViewById(R.id.dynamic_listview);
            swipeContainer = (SwipeRefreshLayout) rootView.findViewById(R.id.swipeContainer);
            mainActivity = new MocehatCrmMainActivity();
            pdialogue = new ProgressDialog(getActivity());
            isLive = mainActivity.isInternetConnected(getActivity());
            sharedPreferenceActivity=new SharedPreferenceActivity(getActivity());
            GetDataFromLocalDb(null);
            if (mainActivity.isInternetConnected(getActivity())) {
                new GetListOfCustomersChat(null).execute();
            }
            fragment=this;
            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    //isRefresh = true;
                    if (isLive) {
                        new GetListOfCustomersChat(swipeContainer).execute();
                        //refresh(swipeContainer);
                    } else {
                        if (swipeContainer != null) {
                            swipeContainer.setRefreshing(false);
                        }
                        Toast.makeText(getActivity(), getActivity().getResources().
                                getString(R.string.checkInternetCon), Toast.LENGTH_SHORT).show();
                    }
                }
            });
            swipeContainer.setColorSchemeResources(R.color.AppThemeColor/*,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_purple*/);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return rootView;
    }

    public void GetDataFromLocalDb(String searchText) {

        mycustomerArrayList = new ArrayList<MyCustomerEntity>();
        mycustomerArrayList = mainActivity.getCustomerChatList(getActivity(),searchText);
        chatFragmentAdapter = new ChatFragmentAdapter(getActivity(),
                android.R.layout.simple_list_item_1, mycustomerArrayList, "CustomerChatFragment");
        Listview.setAdapter(chatFragmentAdapter);
    }


    private class GetListOfCustomersChat extends AsyncTask<String, String, String> {
        String json;
        SwipeRefreshLayout swipeRefreshLayout;
        public GetListOfCustomersChat( SwipeRefreshLayout swipeContainer) {
            if(swipeContainer!=null) {
                swipeRefreshLayout = swipeContainer;
            }
        }

        protected void onPreExecute() {
            if(mycustomerArrayList.size()==0) {
            pdialogue.setMessage("Loading customer chat ...");
            pdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pdialogue.setCancelable(true);
            pdialogue.show();
            }
            HashMap hashMap=new HashMap();
            hashMap.put("LastUpdatedTimeTicks",sharedPreferenceActivity.getCustomerListUpdateTime());
            json=gson.toJson(hashMap);
            super.onPreExecute();
        }

        protected String doInBackground(String... param) {

            String result = "";
            URL url;
            try {
                url = new URL(MocehatCrmMainActivity.WEB_API_URL
                        + "getcustomerchatlist");
                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                writer.write(json);
                writer.flush();
                writer.close();

                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }

            /*   // if( mycustomerArrayList.size()>0) {
                    updateLocalDb(result);
              //  }*/

            } catch (Exception e) {
                Log.d("GetPinCode", "Error:" + e.getMessage());
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if(pdialogue.isShowing()){
                    pdialogue.dismiss();
                }

                if(active && mycustomerArrayList.size()==0) {

                    Gson gson = new Gson();
                    MyCustomerEntity AllCustomerChat = gson.fromJson(result, MyCustomerEntity.class);
                    ArrayList<MyCustomerEntity> mycustomerArrayList = AllCustomerChat.chatlist;

                    chatFragmentAdapter = new ChatFragmentAdapter(getActivity(),
                            android.R.layout.simple_list_item_1, mycustomerArrayList, "CustomerChatFragment");
                    Listview.setAdapter(chatFragmentAdapter);
                    new UpdateLocalDb(result,false).execute();

                }else{

                        new UpdateLocalDb(result,true).execute();

                }
                    // pdialogue.dismiss();
                    if (swipeRefreshLayout != null) {
                        swipeRefreshLayout.setRefreshing(false);
                    }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }
    private class UpdateLocalDb extends AsyncTask<String, String, String> {
        String result;
        boolean reload=false;


        public UpdateLocalDb(String result,boolean reloaddata) {
            this.result=result;
            reload=reloaddata;
        }


        protected void onPreExecute() {

            super.onPreExecute();
        }

        protected String doInBackground(String... param) {


            try {
                Gson gson = new Gson();
                MyCustomerEntity AllCustomerChat = gson.fromJson(result, MyCustomerEntity.class);
                ArrayList<MyCustomerEntity> mycustomerArrayList = AllCustomerChat.chatlist;
                sharedPreferenceActivity.setCustomerListUpdateTime(AllCustomerChat.LastUpdatedTimeTicks);
                for (MyCustomerEntity CustomerChatDetails : mycustomerArrayList) {
                    try {
                        mainActivity.savecustomerDetails(getActivity(), CustomerChatDetails);

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    /// save in local db
                }

            } catch (Exception e) {
                Log.d("GetPinCode", "Error:" + e.getMessage());
                e.printStackTrace();
            }
            return null;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if(reload && active){
                GetDataFromLocalDb(null);
            }


        }
    }
}


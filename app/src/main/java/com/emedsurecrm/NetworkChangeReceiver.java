package com.emedsurecrm;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;


/**
 * Created by Poulav on 21-12-2015.
 */
public class NetworkChangeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(final Context context, final Intent intent) {
        try {
            String status = NetworkUtils.getConnectivityStatusString(context);

            if (status != null && !(status.equalsIgnoreCase("Not connected to Internet"))) {
                MocehatCrmMainActivity mainActivity = new MocehatCrmMainActivity();

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
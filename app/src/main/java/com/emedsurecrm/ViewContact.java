package com.emedsurecrm;

import android.Manifest;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class ViewContact extends AppCompatActivity {

    TextView viewContactName,viewContactPhone;
    Button viewContactAdd;
    ImageView viewContactCall;
    static final Integer CALL = 0x2;
    String data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_contact);

        getSupportActionBar().setTitle("View Contact");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewContactName     =   (TextView) findViewById(R.id.view_contact_name);
        viewContactPhone    =   (TextView)findViewById(R.id.view_contact_phone_number);
        viewContactAdd      =   (Button) findViewById(R.id.view_contact_add);
        viewContactCall     =   (ImageView)findViewById(R.id.view_contact_call);



       data = getIntent().getStringExtra("data");
        if(data!=null && data.length()>0)
        {
            if(data.contains("@"))
            {
                viewContactName.setText(data.split("@")[0].toString());
                viewContactPhone.setText(data.split("@")[1].toString());
            }
        }

        viewContactCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
         askForPermission(Manifest.permission.CALL_PHONE,CALL);

            }
        });

        viewContactAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(ViewContact.this);
                final AlertDialog alertDialog = builder.create();

                LayoutInflater inflater     =   (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view1                   =   inflater.inflate(R.layout.contact_dialog, null);

                Button newContact = (Button) view1.findViewById(R.id.new_contact);
                Button existingContact = (Button) view1.findViewById(R.id.existing_contact);

                newContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();

                        // this code for creating new contact
                        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
                        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                        intent.putExtra(ContactsContract.Intents.Insert.EMAIL, "")
                                .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE, "")
                                .putExtra(ContactsContract.Intents.Insert.PHONE, data.split("@")[1])
                                .putExtra(ContactsContract.Intents.Insert.NAME,data.split("@")[0])
                                .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK);
                        startActivity(intent);

                    }
                });

                existingContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    alertDialog.dismiss();

                      Log.i("phone","phone number"+getPhoneNumber(data.split("@")[1]));
                        if(getPhoneNumber(data.split("@")[1])!=-1)
                        {
                            long idPhone = getPhoneNumber(data.split("@")[1]);
                            if (idPhone > 0) {
                                Intent intent = new Intent(Intent.ACTION_EDIT);
                                intent.setData(ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, idPhone));
                                startActivity(intent);
                            } else {
                                Toast.makeText(getApplicationContext(), "contact not in list",
                                        Toast.LENGTH_SHORT).show();}
                        }

                    }
                });

                alertDialog.setView(view1);
                alertDialog.show();
                alertDialog.setCancelable(true);

             // this code for editing contact

            }
        });


    }

    private long getPhoneNumber(String phone)
    {
        // CONTENT_FILTER_URI allow to search contact by phone number
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
// This query will return NAME and ID of conatct, associated with phone //number.
        Cursor mcursor = getContentResolver().query(lookupUri,new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, ContactsContract.PhoneLookup._ID},null, null, null);
//Now retrive _ID from query result
        long idPhone = 0;
        try {
            if (mcursor != null) {
                if (mcursor.moveToFirst()) {
                    idPhone = Long.valueOf(mcursor.getString(mcursor.getColumnIndex(ContactsContract.PhoneLookup._ID)));
                    Log.d("", "Contact id::" + idPhone);
                }
            }
           return idPhone;
        } finally {
            mcursor.close();
        }

    }

    private void askForPermission(String permission, Integer requestCode) {
        if (ContextCompat.checkSelfPermission(ViewContact.this, permission) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(ViewContact.this, permission)) {

                //This is called if user has denied the permission before
                //In this case I am just asking the permission again
                ActivityCompat.requestPermissions(ViewContact.this, new String[]{permission}, requestCode);

            } else {

                ActivityCompat.requestPermissions(ViewContact.this, new String[]{permission}, requestCode);
            }
        } else {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:"+data.split("@")[1]));
            startActivity(callIntent);

         //   Toast.makeText(this, "" + permission + " is already granted.", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(ActivityCompat.checkSelfPermission(this, permissions[0]) == PackageManager.PERMISSION_GRANTED){
                      Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+data.split("@")[1]));
                          startActivity(callIntent);

          //  Toast.makeText(this, "Permission granted", Toast.LENGTH_SHORT).show();
        }else{
            Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
        {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}

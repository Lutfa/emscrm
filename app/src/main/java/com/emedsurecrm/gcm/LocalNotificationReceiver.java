package com.emedsurecrm.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.emedsurecrm.HomeScreen;
import com.emedsurecrm.MocehatCrmMainActivity;
import com.emedsurecrm.R;


/**
 * Created by Poulav on 27-10-2016.
 */

public class LocalNotificationReceiver extends WakefulBroadcastReceiver {

    private static final String TAG = "LocalNotification";
   MocehatCrmMainActivity mainActivity = null;
    //NotificationEntity notificationsEntity;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    Context context;

    public void onReceive(Context cont, Intent intent) {

        Log.i(TAG, "Local notification received");
        // Explicitly specify that GcmIntentService will handle the intent.
        /*try {
            context = cont;
            mainActivity = new NpcllcMainActivity();
            int unReadNotification = mainActivity.getNotificationToDB(context);
            if (unReadNotification > 0) {
                sendNotification(unReadNotification);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/

        //get and send location information
    }

    private void sendNotification(int notificationCount) {
        PendingIntent contentIntent;
        try {
            mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            contentIntent = PendingIntent.getActivity(context,
                    0, new Intent(context, HomeScreen.class), 0);

            Uri notificationSound = Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.localsound);

            long[] v = {0, 300};
            Notification.Builder builder = new Notification.Builder(context);
            String messageBody = "", fullMsgBody = "";
            if (notificationCount == 1)
                messageBody = "order";
            else
                messageBody = "orders";

            fullMsgBody = "You have (" + notificationCount + ") unattended " + messageBody;

            Notification notification;

            builder.setContentTitle(context.getString(R.string.app_name))
                    .setContentText(fullMsgBody)
                    .setSmallIcon(R.drawable.icon)
                    .setAutoCancel(true)
                    .setContentIntent(contentIntent)
                    .setSound(notificationSound)
                    .setPriority(Notification.PRIORITY_HIGH);
            //.setVibrate(v);

            notification = new Notification.BigTextStyle(
                    builder).bigText(fullMsgBody).build();
            mNotificationManager.notify(0, notification);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.emedsurecrm.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.emedsurecrm.HomeScreen;
import com.emedsurecrm.R;


import org.json.JSONException;

/**
 * Created by Suraj on 25/9/14.
 */

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    public GcmIntentService() {
        super("GcmIntentService");
    }
    public static final String TAG = "GCM Demo";

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "gcm notification received");
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of un-parcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                Log.d(TAG, "Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                Log.d(TAG, "Deleted messages on server: " + extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.
                for (int i = 0; i < 5; i++) {
                    Log.i(TAG, "Working... " + (i + 1)
                            + "/5 @ " + SystemClock.elapsedRealtime());
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                    }
                }
                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification(extras);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(Bundle msg) {
        Log.d(TAG, "sendNotification");
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        // create pending intent by choosing Activity according to the msg
        PendingIntent contentIntent = null;
        String msgTitle = msg.getString("alert");
        try {
            contentIntent = generateIntent(msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.icon)
                        .setContentTitle(msgTitle)
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msgTitle))
                        .setContentText(msgTitle);
        mBuilder.setContentIntent(contentIntent);

        Notification notification = mBuilder.build();
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notification.defaults |= Notification.DEFAULT_SOUND;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private PendingIntent generateIntent(Bundle msg) throws JSONException {
        Log.d(TAG, "message bundle: " + msg);
        String type = msg.getString("gcm_type");
        Intent intent = null;
//        JSONObject jsonObject;
//        try {
//            jsonObject = new JSONObject(msg.getString("response"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//            jsonObject = new JSONObject();
//            try {
//                jsonObject.put("query_id", 8);
//                jsonObject.put("product_title",
//                        "Canon IXUS 500 HS 10.1MP Point and Shoot Digital Camera");
//            } catch (JSONException e1) {
//                e1.printStackTrace();
//            }
//
//        }

        intent = new Intent(this, HomeScreen.class);

        /****

         // pass the suitable data
         if(type.contentEquals("bid_ended")){
         // type 2
         // bid result page; Buyer's bid time has ended
         intent = new Intent(this, OffersActivity.class);
         }
         else if(type.contentEquals("first_cheaper_bid")){
         // type 1
         // bid details page
         intent = new Intent(this, OrderConfirmActivity.class);
         intent.putExtra(getString(R.string.intent_query_response), jsonObject.getJSONArray("leads").getJSONObject(0).toString());
         }
         else if(type.contentEquals("cheap_bid_expiring")){
         // type 3
         // bid details page; cheap_bid_expiring
         intent = new Intent(this, OrderConfirmActivity.class);
         intent.putExtra(getString(R.string.intent_query_response), jsonObject.getJSONArray("leads").getJSONObject(0).toString());
         }
         else if(type.contentEquals("pending_pickup")){
         // type 4
         // Order confirmation page
         intent = new Intent(this, OrderCompleteActivity.class);
         intent.putExtra(getString(R.string.intent_query_response), jsonObject.getJSONArray("leads").getJSONObject(0).toString());
         }
         else {
         // seek_feedback
         // type 5
         // Order confirmation page
         intent = new Intent(this, FeedBackActivity.class);
         HomeFragment.commentsFLag = true;
         intent.putExtra(getString(R.string.intent_query_response), jsonObject.getJSONObject("response").getJSONArray("leads").getJSONObject(0).toString());
         }
         intent.putExtra(getString(R.string.intent_query_id), jsonObject.getString("query_id"));

         ****/

        return PendingIntent.getActivity(this, 0, intent, 0);
    }
}

package com.emedsurecrm.gcm;

/**
 * Created by meeza on 25/9/14.
 */
public interface GCMRegistrationListener {
    public void registrationCallback(String regId);
}

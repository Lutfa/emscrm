
package com.emedsurecrm.gcm;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.emedsurecrm.ChatMessagesActivity;
import com.emedsurecrm.MocehatCrmMainActivity;
import com.emedsurecrm.R;
import com.emedsurecrm.model.ChatMessageEntity;

import org.apache.commons.lang3.StringUtils;

public class MocehatCRMNotificationService extends IntentService {

    MocehatCrmMainActivity mainActivity = null;
    //ActivityEntity activityEntity = null;
    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    ChatMessageEntity chatMessageEntity;
    /*LoginCredentials loginCredentials;*/
    int tabPosition = 0;
    public static final String TAG = "MocehatCRMNotificationService";

    public MocehatCRMNotificationService() {
        super("1052719788270");
        mainActivity = new MocehatCrmMainActivity();
        //activityEntity = new ActivityEntity();
        chatMessageEntity = new ChatMessageEntity();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            Bundle extras = intent.getExtras();
            GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
            String messageType = gcm.getMessageType(intent);
            if (!extras.isEmpty()) {
                if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
                        .equals(messageType)) {
                    sendNotification("Send error: " + extras.toString(), null);
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
                        .equals(messageType)) {
                    sendNotification(
                            "Deleted messages on server: " + extras.toString(),
                            null);
                } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
                        .equals(messageType)) {
                    sendNotification("", extras);
                    /*loginCredentials = LoginCredentials.getInstance(this);
                    if (!loginCredentials.getAdminId().equalsIgnoreCase("")) {
                        sendNotification("", extras);
                    }*/
                }
            }
            GcmBroadcastReceiver.completeWakefulIntent(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void sendNotification(String message, Bundle extras) {

        String messageby = "", messageBody = "", activitGuid = "", json = null, activityTypeFromNotific = null,
                localActivityID = null, activityType = null, messageType = null, NoficationMessage = null, ApiFor = null;
        PendingIntent contentIntent;
        Uri notificationSound = null;
        Notification notification;
        ApiFor = extras.getString("APIFor").toString();
        String apifor = null, appName = null;
        if (extras.containsKey("TypeofApp")) {
            appName = extras.getString("TypeofApp");
        }
        if (appName.equalsIgnoreCase("MCCRM")) {
            try {
                final ChatMessageEntity chatMessage = new ChatMessageEntity();
                messageBody = extras.getString("MessageBody");
                chatMessage.MessageBody = messageBody;
                chatMessage.Id = extras.getString("Id");
                chatMessage.MessageType = extras.getString("MessageType");
                chatMessage.SenderId = extras.getString("SenderId");
                chatMessage.ReceiverId = extras.getString("ReceiverId");
                chatMessage.CreatedOnUtc = extras.getString("TimeStamp");
                chatMessage.MessageTime = chatMessage.MessageTimeInLong;
                chatMessage.ParentMessageId = extras.getInt("ParentMessageId");
                chatMessage.IsNotificationMessageRead = true;
                try {
                    chatMessage.Option2Value = extras.getString("Option2Value");

                    chatMessage.ImagePath = extras.getString("ImagePath");
                    chatMessage.ActivityGuid = "1";

                  /*  if(!chatMessage.SenderId.equalsIgnoreCase("1")) {
                        chatMessage.ReceiverId = "1";
                    }else{
                        chatMessage.ReceiverId = RecieverId;
                    }*/


                    //  Log.i("mediapath", "media path is" + chatMessage.MediaPath);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mainActivity.addsendermessaget(this, chatMessage);
                if (ChatMessagesActivity.ChatStatus && ChatMessagesActivity.chatActivityId.equalsIgnoreCase(chatMessage.SenderId)) {
                    mainActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ChatMessagesActivity.currentChatActivity.updateScreenFromReceiver(chatMessage);
                        }
                    });
                } else {
                    if (!StringUtils.isBlank(chatMessage.SenderId) && !chatMessage.SenderId.equalsIgnoreCase("1")) {
                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                        Intent resultIntent = new Intent(this, ChatMessagesActivity.class);


                        if (ApiFor != null && ApiFor.equalsIgnoreCase("Chat")) {
                            resultIntent.putExtra("RecieverId", chatMessage.SenderId);
                            resultIntent.putExtra("UserType", "Customer");
                        } else if (ApiFor != null && ApiFor.equalsIgnoreCase("AdminChatWithPharma")) {
                            resultIntent.putExtra("RecieverId", chatMessage.SenderId);
                            resultIntent.putExtra("UserType", "Pharmacy");
                        }
                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        resultIntent.setAction(Intent.ACTION_VIEW);
                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        stackBuilder.addParentStack(ChatMessagesActivity.class);
                        stackBuilder.addNextIntent(resultIntent);
                        contentIntent = PendingIntent.getActivity
                                (this, 1, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                        if (chatMessage.MessageType != null && chatMessage.MessageType.equalsIgnoreCase("Message")) {
                            {
                                notificationSound = Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.chatringtone);
                            }
                        }
                        builder = new android.support.v7.app.NotificationCompat.Builder(this);
                        builder.setVibrate(new long[]{100, 200, 100, 500})
                                .setContentTitle("EMS CRM")
                                .setContentText(chatMessage.MessageBody)
                                .setSmallIcon(R.drawable.icon).setAutoCancel(true)
                                .setContentIntent(contentIntent)
                                .setPriority(Notification.PRIORITY_HIGH).setSound(notificationSound);

                        android.support.v7.app.NotificationCompat.InboxStyle inboxStyle =
                                new android.support.v7.app.NotificationCompat.InboxStyle();

                        inboxStyle.setBigContentTitle("Admin");
                        inboxStyle.addLine(messageBody);
                        inboxStyle.setBigContentTitle("EMS CRM");
                        builder.setStyle(inboxStyle);
                        mNotificationManager = (NotificationManager) this
                                .getSystemService(Context.NOTIFICATION_SERVICE);
                        mNotificationManager.notify(0, builder.build());

                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }

}

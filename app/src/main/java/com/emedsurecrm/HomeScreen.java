package com.emedsurecrm;

import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.adapter.MyOrdersViewPagerAdapter;
import com.emedsurecrm.fragment.CustomerChatFragment;
import com.emedsurecrm.fragment.PharmacyChatFragment;
import com.emedsurecrm.model.SharedPreferenceActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class HomeScreen extends ActionBarActivity implements AnimationListener {
    Builder alerter;
    Uri selectedImageUri;
    public Intent intent;
    ProgressDialog pdialogue;
    SQLiteDatabase database;
    Gson gson = new Gson();
    Boolean isRemoved = false;
    //static DeliveryDBHelper db;
    Bitmap bitmap;
    MocehatCrmMainActivity mainActivity;
    HashMap<String, String> map;
    Dialog dialog;
    Boolean setBitmap = false;
    ArrayList<HashMap<String, String>> activitieslist = null;
    Menu menu;
    Intent i = null;
    RobotoTextView titleTextV, setReimnderTVbtn;
    ImageView backButon;
    RelativeLayout backLayout;
    private TabLayout chatTabLayout;
    SharedPreferenceActivity sharedPreferenceActivity;
    private ViewPager chatViewPager;
    MyOrdersViewPagerAdapter adapter;
    boolean isAlreadySearched = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_layout);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
           // getSupportActionBar().setTitle("Home Screen");
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle(getString(R.string.app_name));
            /*titleTextV = (RobotoTextView) findViewById(R.id.toolbar_title);
            titleTextV.setText(getResources().getString(R.string.home_screen));*/
           /* backLayout = (RelativeLayout) findViewById(R.id.backLayout);
            backLayout.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });*/
            setupViewPager();




            chatTabLayout.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
            changeTitle();
            chatTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    if (tab.getPosition() == 0) {
                        try {
                            RelativeLayout imageView = (RelativeLayout) tab.getCustomView().findViewById(R.id.relative_change_bg);
                            imageView.setBackgroundResource(R.drawable.white_border);

                            TextView details_button = (TextView) tab.getCustomView().findViewById(R.id.details_button);
                            details_button.setTextColor(getResources().getColor(R.color.appthemePinkMixed));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        try {
                            RelativeLayout imageView = (RelativeLayout) tab.getCustomView().findViewById(R.id.relative_change_bg);
                            TextView tab_text = (TextView) tab.getCustomView().findViewById(R.id.tab);
                            imageView.setBackgroundResource(R.drawable.white_border);
                            tab_text.setTextColor(getResources().getColor(R.color.appthemePinkMixed));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                    if (tab.getPosition() == 0) {
                        try {
                            RelativeLayout imageView = (RelativeLayout) tab.getCustomView().findViewById(R.id.relative_change_bg);
                            imageView.setBackgroundResource(R.drawable.home_top_button_unchecked_bg);

                            TextView details_button = (TextView) tab.getCustomView().findViewById(R.id.details_button);
                            details_button.setTextColor(getResources().getColor(R.color.white));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        try {
                            RelativeLayout imageView = (RelativeLayout) tab.getCustomView().findViewById(R.id.relative_change_bg);
                            TextView tab_text = (TextView) tab.getCustomView().findViewById(R.id.tab);
                            imageView.setBackgroundResource(R.drawable.home_top_button_unchecked_bg);
                            tab_text.setTextColor(getResources().getColor(R.color.white));
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });










        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setupViewPager() {
        try {
            chatViewPager = (ViewPager) findViewById(R.id.viewpager);
            setupViewPager(chatViewPager);
            chatTabLayout = (TabLayout) findViewById(R.id.tabs);
            chatTabLayout.setTabMode(TabLayout.MODE_FIXED);
            chatTabLayout.setupWithViewPager(chatViewPager);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        try {
            adapter = new MyOrdersViewPagerAdapter(getSupportFragmentManager());
            Bundle bundleorderDetails = new Bundle();
            CustomerChatFragment customerchatTracker = new CustomerChatFragment();
            customerchatTracker.setArguments(bundleorderDetails);
            PharmacyChatFragment requestList = new PharmacyChatFragment();
            requestList.setArguments(bundleorderDetails);
            adapter.addFragment(customerchatTracker, getResources().getString(R.string.customer_chatList));
            adapter.addFragment(requestList, getResources().getString(R.string.pharmacy_chatList));
            viewPager.setAdapter(adapter);
            //viewPager.setCurrentItem(tabPosition);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    public void changeTitle() {
        try {

       

                View tabOne = (View) LayoutInflater.from(this).inflate(R.layout.custom_tab_pharmacylist, null);
                if (chatTabLayout.getTabAt(1).getCustomView() == null) {
                    chatTabLayout.getTabAt(1).setCustomView(tabOne);
                }


                View tabOne2 = (View) LayoutInflater.from(this).inflate(R.layout.custom_tab_customerlist, null);
                if (chatTabLayout.getTabAt(0).getCustomView() == null) {
                    chatTabLayout.getTabAt(0).setCustomView(tabOne2);
                }

                TextView b = (TextView) chatTabLayout.getTabAt(1).getCustomView().findViewById(R.id.badge);

                TextView tab = (TextView) chatTabLayout.getTabAt(1).getCustomView().findViewById(R.id.tab);
                tab.setText( getResources().getString(R.string.pharmacy_chatList));


                TextView button = (TextView) chatTabLayout.getTabAt(0).getCustomView().findViewById(R.id.details_button);
                button.setText( getResources().getString(R.string.customer_chatList));



                View v = chatTabLayout.getTabAt(1).getCustomView().findViewById(R.id.badgeCotainer);
                v.setVisibility(View.GONE);
//            }


        

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.menu_myorder_activity, menu);
            this.menu = menu;
            /*MenuItem action_search = menu.findItem(R.id.action_search);
            action_search.setVisible(true);*/
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            //searchEditText.setTextColor(getResources().getColor(R.color.white));
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setQueryHint(getString(R.string.searchName));
           /* searchView.setBackgroundColor(this.getResources().getColor(R.color.white));*/
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    // TODO Auto-generated method stub
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    if (newText.length() > 2) {
                        isAlreadySearched = true;
                        SearchChat(newText);
                    } else {
                        /*if (isAlreadySearched)*/
                        SearchChat(null);
                    }
                    return true;
                }
            });

            /* MenuItem refresh = menu.findItem(R.id.refresh);
            refresh.setVisible(false);*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return true;
    }

    private void SearchChat(String searchText) {
        int index = -1;
        Fragment currentFragment;
        try {

            index = chatViewPager.getCurrentItem();
            adapter = ((MyOrdersViewPagerAdapter) chatViewPager.getAdapter());
            currentFragment = adapter.getItem(index);
            if (currentFragment instanceof CustomerChatFragment) {
                CustomerChatFragment CustomerFragment = (CustomerChatFragment) currentFragment;
                CustomerFragment.GetDataFromLocalDb(searchText);
            } else if (currentFragment instanceof PharmacyChatFragment) {
                PharmacyChatFragment PharmacyFragment = (PharmacyChatFragment) currentFragment;
                PharmacyFragment.GetDataFromLocalDb(searchText);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        //getSupportActionBar().setTitle(title);
        titleTextV.setText("Shops");
    }

    @Override
    public void onAnimationStart(Animation animation) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        // TODO Auto-generated method stub

    }

    //
    @Override
    public void onAnimationRepeat(Animation animation) {
        // TODO Auto-generated method stub

    }

    public void onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


}


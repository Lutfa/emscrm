package com.emedsurecrm;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.PowerManager;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by PCCS on 5/10/2017.
 */

public  abstract class DownloadFile extends AsyncTask<String, Integer, String> {

    private Context context;
    private PowerManager.WakeLock mWakeLock;
    private String surl,filename;
    ProgressDialog progressDialog;


    public DownloadFile(Context context, String url, String filename) {
        this.context = context;
        this.surl = url;
        this.filename = filename;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Downloading...");
        progressDialog.show();

    }

    @Override
    protected String doInBackground(String... sUrl) {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(surl);
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file


             if(filename.toString().endsWith("pdf")
                || filename.toString().endsWith("doc")
                || filename.toString().endsWith("docx")
                || filename.toString().endsWith("ppt")
                || filename.toString().endsWith("pptx")
                || filename.toString().endsWith("xls")
                || filename.toString().endsWith("xlsx")
                || filename.toString().endsWith("txt")) {

                 if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                     return "Server returned HTTP " + connection.getResponseCode()
                             + " " + connection.getResponseMessage();
                 }
                 // this will be useful to display download percentage
                 // might be -1: server did not report the length
                 int fileLength = connection.getContentLength();

                 Log.i("length", "length of file size" + fileLength);

                 // download the file
                 String root = root = Environment.getExternalStorageDirectory().toString();
                 File directory = new File(root + "/Xampr/Xampr Documents");
                 input = connection.getInputStream();
                 output = new FileOutputStream(directory + "/" + filename);

                 byte data[] = new byte[4096];
                 long total = 0;
                 int count;
                 while ((count = input.read(data)) != -1) {
                     // allow canceling with back button
                     if (isCancelled()) {
                         input.close();
                         return null;
                     }
                     total += count;
                     // publishing the progress....
                     if (fileLength > 0) // only if total length is known
                         publishProgress((int) (total * 100 / fileLength));
                     output.write(data, 0, count);
                 }

             }
             else if(filename.toString().endsWith(".mp3") || filename.endsWith(".mp4"))
             {
                 if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                     return "Server returned HTTP " + connection.getResponseCode()
                             + " " + connection.getResponseMessage();
                 }
                 // this will be useful to display download percentage
                 // might be -1: server did not report the length
                 int fileLength = connection.getContentLength();

                 Log.i("length", "length of file size" + fileLength);

                 // download the file
                 String root = root = Environment.getExternalStorageDirectory().toString();
                 File directory = new File(root + "/Xampr/Xampr Audio");
                 input = connection.getInputStream();
                 output = new FileOutputStream(directory + "/" + filename);

                 byte data[] = new byte[4096];
                 long total = 0;
                 int count;
                 while ((count = input.read(data)) != -1) {
                     // allow canceling with back button
                     if (isCancelled()) {
                         input.close();
                         return null;
                     }
                     total += count;
                     // publishing the progress....
                     if (fileLength > 0) // only if total length is known
                         publishProgress((int) (total * 100 / fileLength));
                     output.write(data, 0, count);
                 }
             }

        } catch (Exception e) {
            return e.toString();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
            }

            if (connection != null)
                connection.disconnect();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        if(s!=null)
        {
            progressDialog.dismiss();
            onResponseReceived(s);
        }
        progressDialog.dismiss();
    }

    public abstract void onResponseReceived(String result);
}
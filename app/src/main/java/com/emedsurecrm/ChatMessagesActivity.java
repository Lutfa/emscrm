package com.emedsurecrm;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActivityOptions;
import android.app.AlertDialog.Builder;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.Images;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.webkit.MimeTypeMap;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.emedsurecrm.NewUI.RobotoTextView;
import com.emedsurecrm.adapter.ChatListAdapter1;
import com.emedsurecrm.model.ChatMessageEntity;
import com.emedsurecrm.model.MyCustomerEntity;
import com.emedsurecrm.model.SharedPreferenceActivity;
import com.emedsurecrm.utils.MocehatCrmDBHelper;
import com.rockerhieu.emojicon.EmojiconEditText;
import com.rockerhieu.emojicon.EmojiconGridFragment;
import com.rockerhieu.emojicon.EmojiconsFragment;
import com.rockerhieu.emojicon.emoji.Emojicon;
import com.squareup.picasso.Picasso;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import static android.view.View.VISIBLE;


@SuppressLint("NewApi")
public class ChatMessagesActivity extends AppCompatActivity implements
        EmojiconGridFragment.OnEmojiconClickedListener,
        EmojiconsFragment.OnEmojiconBackspaceClickedListener,
        ChatListAdapter1.ChatListAdapterListener {
    public static final int NOTIFICATION_ID = 1;
    static final int REQUEST_VIDEO_CAPTURE = 4;
    static final int REQUEST_PICK_FILE = 5;
    private static final String TABLE_TOKENS = "userKeyDetails";
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "AudioRecorder";
    private static final int PERMISSION_REQUEST_CODE = 200;
    public static String chatActivityId = "";
    public static ChatMessagesActivity currentChatActivity = null;
    public static boolean ChatStatus = false;
    public static HashMap chatMessageHashMap = new HashMap();
    static boolean ReceivedMessage = false;
    public static final String MyPREFERENCES = null;
    List<ChatMessageEntity> ChatReceiverArrayList;
    static MocehatCrmDBHelper dbHelper;
    private static int SELECT_FILE_CAMERA = 2;
    private static int SELECT_FILE_GALLERY = 1;
    private static int SELECT_FILE_AUDIO = 3;
    private static int SELECT_PLACE_PICKER = 7;
    private static int SELECT_CONTACT_PICKER = 8;
    private final int stateMP_Error = 0;
    private final int stateMP_NotStarter = 1;
    private final int stateMP_Playing = 2;
    private final int stateMP_Pausing = 3;
    public HashMap<String, String> map;
    public LinearLayout all_content;
    public ScrollView sv;
    public ClipboardManager myClipboard;
    Bitmap bitmap;
    String filename = null;
    String imagePath = null;
    boolean isKeyBoardVisible = false;
    DisplayMetrics metrics;
    ProgressDialog pdialogue;
    String userDetails, userGuid, activityname, activityType, ParentActivityGuid;
    LinearLayout replySendUserMessageLayout, replySendMediaMessageLayout;
    TextView replySendUserMessageText;
    TextView replySendMediaMessageFrom, replySendMediaMessageText;
    ImageView replySendMediaMessageImage;
    ArrayList<HashMap<String, String>> messageslist = null;
    boolean first_Run = true;
    String messagesExists = null;
    ImageView attachmentButton, SendButton;
    ListView lv_Messages = null;
    ImageButton BackButton, EmoteButton, AudioButton;
    EmojiconEditText chatbox;
    String text_chat, parentGuid, ActivityName;
    SharedPreferenceActivity loginCredentials;
    Builder alerter;
    boolean showProgress = false;
    SharedPreferenceActivity sharedPreferenceActivity;
    RobotoTextView chatTextMessageLeft, chatOwnerNameLeft, chatTextMsgTimeLeft, chatTextMessageRight,
            chatOwnerNameRight, chatTextMsgTimeRight, Image_name, left_name_image, chatContentImageLeftName,
            chatContentImageRightName, rightImageChatFileName,
            timeLeftImage, timeRightImage, tv_fileName, question_TV, closeAnswerMsgText;
    VideoView chat_content_video, chatContentVideoLeft, chatContentVideoRight;
    ArrayList<SendChat> tosend;
    Uri selectedImageUri, currentImageUri;
    ImageView receiver, sender, chatContentImageUserSent, chatContentImageLeft, chatContentImageRight;
    LayoutInflater inflater;
    Bitmap my_image, other_image;
    MenuItem menuMoreCommunityButoon, menuActivityInfo, menuMoreQuestionActButton, menuEditCommunityItem,
            menuEditQuestionItem, menuOpenQuestionItem, menuCloseQuestionItem, pinnedIcon;
    String getActivityIcon = null, tv_activitydescription = null;
    String previousScreen = null;


    //  String ThemeCode = null;
    Gson gson = new Gson();
    String parentActivityDetails = null;
    MocehatCrmMainActivity mainActivity = null;
    String LastMessage = null, People_count_text, LastMessageBy = null,
            LastMessageTime = null, ContainerLastMessage = null,
            ContainerLastMessageBy = null, ContainerLastMessageTime = null, forumlastMessage = null, forumlastMessageBy = null, forummessageTime = null;
    String latestChat = null, lastTime = null, senderName = null;
    //ActionBar actionBar;
    FrameLayout emojiconslayout;
    RelativeLayout galleryLayout, cameraLayout, videoLayout, audioLayout_popUp,
            audioLayout, documentLayout, bottomLayoutSendChat, parentlayout, Chat_area;
    boolean isGallery = false, isAudio = false, isVideo = false,
            isCamera = false;
    byte[] imageInByte = null;
    byte[] imageFromdb = null;
    String mainMessage, receivedFileName;
    File directory = null;
    String textFilename = null;
    byte[] getByteArray = null;
    String localFilePath = null, mCurrentPhotoPath = null;
    String fileToDownload = null;
    String timeLong = null;
    ImageView imageViewToShow;
    Boolean isUpload = false;
    String statusColor;
    FloatingActionButton floatActionButton;
    Boolean isDowndoad = false, isSDPresent = false;
    SharedPreferences app_preference = null;
    ScheduledThreadPoolExecutor sch = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(2);
    boolean isBackgroundRunning = false, isFirstTimeLoad = false;
    List<Long> messageTimes = null;
    String[] PERMISSIONS = {"android.permission.CAMERA", "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    boolean isPin;
    boolean isActivityUpdated = false, isProjectWithinTeam = false;
    ClipData myClip;
    ChatListAdapter1 chatListAdapter;
    List<ChatMessageEntity> chatMessages = new ArrayList<ChatMessageEntity>();
    ImageView replyshowImageView;
    JSONObject paretnJsonObject;
    View bottomsheet;
    String encryptMessageBody = "";
    private int stateMediaPlayer;
    private MediaRecorder recorder = null;
    private int currentFormat = 0;

    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4,
            MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4,
            AUDIO_RECORDER_FILE_EXT_3GP};
    private NotificationManager mNotificationManager;
    List<ChatMessageEntity> chatMessageAdmin = new ArrayList<ChatMessageEntity>();
    private RelativeLayout chatContentRightLayoutView, chatContentLeftLayoutView;
    private Toolbar replyToolbar;
    private TextView toolbarReply, toolbarCopy;
    private String varData = "";

    private RelativeLayout replayViewLayout;
    private ImageView toolbarback;
    private RecyclerView recyclerView;
    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    private String RecieverId, RecieverName, UserType, Passwordsalt;
    ImageView closeButton;


    boolean isConnected;
    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mainActivity.isInternetConnected(ChatMessagesActivity.this)) {
                        isBackgroundRunning = true;
                        //handler.postDelayed(runnableCode,5000);
                        // new LongRunningGetActivities().execute();
                        //    UpdateChatScreenWithMessages();
                        // GetChatMessagesFromLocalStorage();


                    }
                }
            });
            // Do something here on the main thread

        }
    };

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri
                .getAuthority());
    }

    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri
                .getAuthority());
    }

    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri
                .getAuthority());
    }

    @SuppressWarnings("deprecation")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.chatwithmember1);

            try {
                if (Build.VERSION.SDK_INT > 9) {
                    StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            dbHelper = new MocehatCrmDBHelper(ChatMessagesActivity.this);
            loginCredentials = SharedPreferenceActivity.getInstance(ChatMessagesActivity.this);
            bottomsheet = (View) findViewById(R.id.bottom_sheet_home_page);
            myClipboard = (ClipboardManager) this.getSystemService(CLIPBOARD_SERVICE);
            closeButton = (ImageView) findViewById(R.id.close);
            replayViewLayout = (RelativeLayout) findViewById(R.id.replayviewlayout);
            //pasteitem = (TextView) findViewById(R.id.pasteitem);
            replyshowImageView = (ImageView) findViewById(R.id.replyshowimageview);
            recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

            getSupportActionBar().setElevation(0);
            //        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.AppThemeColor)));
            final Drawable upArrow = getResources().getDrawable(R.drawable.vector_back_white_icon);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            // getActionBar().setDisplayShowTitleEnabled(true);

            // getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
            getSupportActionBar().setElevation(0);
            getWindow().setBackgroundDrawableResource(R.drawable.chat_background);

            isFirstTimeLoad = true;
            mainActivity = new MocehatCrmMainActivity();
            messageTimes = new ArrayList<Long>();
            //registerReceiver(broadcastReceiver, new IntentFilter("com.zibyr.ChatMessagesActivity"));
            app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
            sharedPreferenceActivity = new SharedPreferenceActivity(ChatMessagesActivity.this);
          /*  activityUserInfo = new ActivityUserEntity();
            activityUsers = new ArrayList<ActivityUserEntity>();*/
            // lv_Messages = (ListView) findViewById(R.id.list_activity);
            //  sv = (ScrollView) findViewById(R.id.scrollView1);
            //  all_content = (LinearLayout) findViewById(R.id.fullchatscreen);
            SendButton = (ImageView) findViewById(R.id.SendButton);
            attachmentButton = (ImageView) findViewById(R.id.attachmentButton);
            EmoteButton = (ImageButton) findViewById(R.id.EmoteButton);
           /* questionLayout = (RelativeLayout) findViewById(R.id.question_Layout);*/
            parentlayout = (RelativeLayout) findViewById(R.id.parentlayout);
            bottomLayoutSendChat = (RelativeLayout) findViewById(R.id.chatboxtLayout);
          /*  question_TV = (RobotoTextView) findViewById(R.id.your_Question);*/
            Chat_area = (RelativeLayout) findViewById(R.id.chatTextAndSendBtnLayout);
            closeAnswerMsgText = (RobotoTextView) findViewById(R.id.close_answer_TV);
            // AudioButton = (ImageButton) findViewById(R.id.AudioButton);
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            chatbox = (EmojiconEditText) findViewById(R.id.chatbox);
            replySendUserMessageLayout = (LinearLayout) findViewById(R.id.user_reply_send_message_layout);
            replySendMediaMessageLayout = (LinearLayout) findViewById(R.id.image_reply_send_message_layout);
            replySendUserMessageText = (TextView) findViewById(R.id.reply_send_user_message_text);
            replySendMediaMessageFrom = (TextView) findViewById(R.id.reply_send_from);
            replySendMediaMessageText = (TextView) findViewById(R.id.reply_send_media);
            replySendMediaMessageImage = (ImageView) findViewById(R.id.reply_send_media_type_image);

            Intent intent = getIntent();
            RecieverId = intent.getStringExtra("RecieverId");
            RecieverName = intent.getStringExtra("RecieverName");
            UserType = intent.getStringExtra("UserType");
            Passwordsalt = intent.getStringExtra("Passwordsalt");

            if (UserType.equalsIgnoreCase("Customer")) {
                MyCustomerEntity chatEntity = dbHelper.getCustomerDetails(ChatMessagesActivity.this, RecieverId);
                RecieverName = chatEntity.CustomerName;


            } else {
                MyCustomerEntity chatEntity = dbHelper.getPharmacyDetails(ChatMessagesActivity.this, RecieverId);
                RecieverName = chatEntity.PharmacyName;
                Passwordsalt = chatEntity.Passwordsalt;
            }


            getSupportActionBar().setTitle(RecieverName);

            /*Bundle bundle = this.getArguments();
            if (bundle != null) {
                activityGuid = bundle.getString("circleId");
                *//*isFromOrderDetailsChat = bundle.getBoolean("IsFromOrderDetailsChat", false);
                escalate=bundle.getString("Escalate","");
                EscalateString=bundle.getString("EscalateString","");*//*
            }
*/


            try {
                //   questionLayout.setVisibility(View.GONE);
               /* if (activityType != null) {

                }*/
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //}

            emojiconslayout = (FrameLayout) findViewById(R.id.emojicons);

            EmoteButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (emojiconslayout.getVisibility() == View.INVISIBLE || emojiconslayout.getVisibility() == View.GONE) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(parentlayout.getWindowToken(), 0);
                            showEmojiPopUp();
                            checkKeyboardHeight(parentlayout);
                            emojiconslayout.setVisibility(View.VISIBLE);
                            EmoteButton.setBackgroundResource(R.drawable.keyboard);
                        } else {
                            emojiconslayout.setVisibility(View.GONE);
                            EmoteButton.setBackgroundResource(R.drawable.chat_smile);
                            InputMethodManager inputMgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputMgr.toggleSoftInput(0, 0);
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });

            setEmojiconFragment(false);
           /* if (userDetails != null) {
                StringTokenizer tokens = new StringTokenizer(userDetails, "^");
                String activityType = tokens.nextToken();
                userGuid = tokens.nextToken();
                activityGuid = tokens.nextToken();
                parentGuid = tokens.nextToken();

            } else {
                parentGuid = activityentity.ParentActivityGuid;*/


            // }
            chatActivityId = RecieverId;
            currentChatActivity = this;

            Chat_area.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {
                    emojiconslayout.setVisibility(View.GONE);

                }

            });
            // Function to limit the chat contents
            chatbox.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count,
                                              int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    if (chatbox.getText().length() > 0) {
                        SendButton.setVisibility(View.VISIBLE);
                        attachmentButton.setVisibility(View.GONE);
                        // AudioButton.setVisibility(View.INVISIBLE);
                        // emojiconslayout.setVisibility(View.GONE);
                    } else {
                        SendButton.setVisibility(View.GONE);
                        attachmentButton.setVisibility(View.VISIBLE);
                        // AudioButton.setVisibility(View.VISIBLE);
                        // emojiconslayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            chatbox.setOnEditorActionListener(new OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId,
                                              KeyEvent event) {
                    if (actionId == EditorInfo.IME_ACTION_DONE) {

                        SendButton.performClick();
                    }
                    return false;
                }
            });

            //below metohd is only for to scroll the recycler vire when keyboard
            // is open as previously keyboard was hiding messages
            if (Build.VERSION.SDK_INT >= 11) {
                recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v,
                                               int left, int top, int right, int bottom,
                                               int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (bottom < oldBottom) {
                            recyclerView.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    try {
                                        if (chatListAdapter.getItemCount() > 0) {
                                            recyclerView.smoothScrollToPosition(chatListAdapter.getItemCount() - 1);
                                        } else {
                                            recyclerView.smoothScrollToPosition(chatListAdapter.getItemCount());
                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }
                                }
                            }, 100);
                        }
                    }
                });
            }

            attachmentButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    Animation slideUp = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                    bottomsheet.startAnimation(slideUp);
                    bottomsheet.setVisibility(VISIBLE);


                    //   showPopup();
                   /*
                    isSDPresent = Environment.getExternalStorageState()
                            .equals(Environment.MEDIA_MOUNTED);

                    if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        // permissions have not been granted.
                        requestPermission();
                    } else {
                        if (isSDPresent) {
                            getPhotoFromCamera();
                        } else {
                            Toast.makeText(ChatMessagesActivity.this, getString(R.string.SDCardNotAvailable), Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }
                    */


                }
            });


            SendButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    replayViewLayout.setVisibility(View.GONE);
                    String txtMessage = chatbox.getText().toString();
                    // String lastMessageTime = "just now";
                    final String timeFormatString = "h:mm a";
                    TimeZone localTimeZone = TimeZone.getDefault();
                    Calendar calendarCurrent = Calendar.getInstance(localTimeZone);
                    String lastMessageTime = DateFormat.format(timeFormatString, calendarCurrent).toString();

                    String toServerUnicodeEncoded = null;
                    try {
                       /* Log.i("tag", "server encoded before" + toServerUnicodeEncoded);
                        toServerUnicodeEncoded = StringEscapeUtils.escapeJava(txtMessage);
                        Log.i("tag", "server encoded after" + toServerUnicodeEncoded);*/
                        //toServerUnicodeEncoded = StringEscapeUtils.escapeJava(text_chat);
                        //toServerUnicodeEncoded = URLEncoder.encode(txtMessage, "UTF-8");
                        initiateOneToOneChat();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    try {
                        // encrypt and descrypt code
                        String plainText = txtMessage;
                        //   encryptMessageBody = mainActivity.getEncryptedText(plainText);
                        encryptMessageBody = plainText;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    ChatMessageEntity chatMessageAdmin = new ChatMessageEntity();
                    try {
                        //  chatMessage.ParentData = paretnJsonObject.toString();

                        if (null != paretnJsonObject && paretnJsonObject.length() > 0) {
                            Log.i("data is", paretnJsonObject.toString());

                            chatMessageAdmin.Option1Name = "ParentData";
                            chatMessageAdmin.Option2Value = paretnJsonObject.toString();
                            chatMessageAdmin.ParentMessageId = Integer.valueOf(paretnJsonObject.get("Id").toString());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                        chatMessageAdmin.UserType = "Pharmacy";
                        chatMessageAdmin.APIFor = "SendChat";
                    }
                    chatMessageAdmin.PasswordSalt = MocehatCrmMainActivity.PasswordSalt;
                    chatMessageAdmin.MessageBody = encryptMessageBody;
                    chatMessageAdmin.MessageType = "Message";
                    chatMessageAdmin.SenderId = "1";
                    chatMessageAdmin.ReceiverId = RecieverId;
                    chatMessageAdmin.IsPresistanceRequired = true;
                    chatMessageAdmin.SenderDeviceGuid = sharedPreferenceActivity.getDeviceGuid();

                    //chatMessageAdmin.ActivityGuid = activityGuid;
                    //  mainActivity.addsendermessaget(getApplicationContext(), chatMessageAdmin);
                    try {


                        chatListAdapter.refresh(chatMessageAdmin);
                        recyclerView.scrollToPosition(chatListAdapter.getItemCount() - 1);


                        paretnJsonObject = new JSONObject();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (mainActivity.isInternetConnected(getApplicationContext())) {
                        new GetChatWithAdminSender(chatMessageAdmin).execute();
                    }
                    chatbox.setText("");
                    latestChat = text_chat;


                }
            });

            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                // permissions have not been granted.
                requestPermission();
            }

            boolean isLive = mainActivity.isInternetConnected(ChatMessagesActivity.this);

            try {


                GetChatMessagesFromLocalStorage();

                if (isLive) {

                    new GetChatWithAdminReciver().execute();

                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } catch (NoClassDefFoundError ex) {
            ex.printStackTrace();
        }


        closeButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("click close button", "yes");

                replayViewLayout.setVisibility(View.GONE);
                paretnJsonObject = new JSONObject();
                chatbox.setText("");
                try {
                    Iterator keys = paretnJsonObject.keys();
                    while (keys.hasNext())
                        paretnJsonObject.remove(keys.next().toString());

                } catch (Exception e) {
                    e.printStackTrace();
                }
                final InputMethodManager imm = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(chatbox.getWindowToken(), 0);
            }
        });


        actionModeCallback = new ActionModeCallback();


        ImageView bottomSheetGallery, bottomSheetCamera, bottomSheetFiles,
                bottomSheetAudio, bottomSheetLocation, bottomSheetContact;

        RelativeLayout layoutHide = (RelativeLayout) findViewById(R.id.layout_hide);
        bottomSheetGallery = (ImageView) findViewById(R.id.bottom_sheet_gallery);
        bottomSheetCamera = (ImageView) findViewById(R.id.bottom_sheet_camera);
        bottomSheetFiles = (ImageView) findViewById(R.id.bottom_sheet_document);
        bottomSheetAudio = (ImageView) findViewById(R.id.bottom_sheet_audio);
        bottomSheetLocation = (ImageView) findViewById(R.id.bottom_sheet_location);
        bottomSheetContact = (ImageView) findViewById(R.id.bottom_sheet_contact);


        layoutHide.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bottomsheet.getVisibility() == VISIBLE) {
                    Animation slideDown = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_down);
                    bottomsheet.startAnimation(slideDown);
                    bottomsheet.setVisibility(View.GONE);

                }
            }
        });

        bottomSheetGallery.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    isGallery = true;
                    getPhotoFromGallery();
                } else
                    mainActivity.show_snakbar("Please turn off USB storage or insert your SD card and try again", parentlayout);
                return;

            }
        });

        bottomSheetCamera.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);

                if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // permissions have not been granted.
                    requestPermission();
                } else {
                    if (isSDPresent) {
                        isGallery = true;
                        getPhotoFromCamera();
                    } else {
                        mainActivity.show_snakbar("Please turn off USB storage or insert your SD card and try again", parentlayout);
                    }
                    return;
                }
            }
        });


        bottomSheetFiles.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {


                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    getDocumentFromPhone();
                } else
                    mainActivity.show_snakbar("Please turn off USB storage or insert your SD card and try again", parentlayout);
                // showPopup.dismiss();
                return;

            }


        });


        bottomSheetAudio.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    isAudio = true;
                    getAudioFromGallery();
                } else {
                    mainActivity.show_snakbar("Please turn off USB storage or insert your SD card and try again", parentlayout);
                }
            }
        });

        bottomSheetLocation.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
/*
                try {
                    PlacePicker.IntentBuilder intentBuilder =
                            new PlacePicker.IntentBuilder();
                    //  intentBuilder.setLatLngBounds(BOUNDS_MOUNTAIN_VIEW);
                    Intent intent = intentBuilder.build(ChatMessagesActivity.this);
                    startActivityForResult(intent, SELECT_PLACE_PICKER);

                } catch (GooglePlayServicesRepairableException
                        | GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                }*/

            }
        });

        bottomSheetContact.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                // Intent intent = new Intent(context, YourNewActivity.class);
                // ((Activity) context).startActivityForResult(intent, resultCode);

               /* Intent intent = new Intent(ChatMessagesActivity.this, ShareContacts.class);
                startActivityForResult(intent, SELECT_CONTACT_PICKER);*/

            }
        });


    }

    private boolean IsBackendHitted(String actID) {
        long lastMsgLong, currentTimeLong, difference;
        boolean isBackendHitRequire = false;
        try {

            isBackendHitRequire = app_preference.getBoolean("Chat_FirstBackendHit_" + actID, false);

           /* lastMsgLong = Long.parseLong(lastMsgTime);
            long currenTime = System.currentTimeMillis();
            currentTimeLong = 621355968000000000L + currenTime * 10000;
            difference = (currentTimeLong - lastMsgLong) / (10000);
            if (difference > 15000)
                isBackendHitRequire = true;*/

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return isBackendHitRequire;
    }

    private void fillButtonColors(int color, int statusColor) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Window window = getWindow();
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                window.setStatusBarColor(color);
                // window.setStatusBarColor(Color.parseColor("#F4804C"));
            }
            if (Build.VERSION.SDK_INT <= 22) {
                attachmentButton.getBackground().setColorFilter(statusColor, PorterDuff.Mode.SRC_ATOP);
                SendButton.getBackground().setColorFilter(statusColor, PorterDuff.Mode.SRC_ATOP);
            } else {
                attachmentButton.setBackgroundTintList(ColorStateList.valueOf(statusColor));
                SendButton.setBackgroundTintList(ColorStateList.valueOf(statusColor));
            }
            getSupportActionBar().setBackgroundDrawable(new ColorDrawable(statusColor));
            //  questionLayout.setBackgroundColor(statusColor);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initiateOneToOneChat() {
       /* int chatCount = 0;
        ActivityUserEntity activityuser = new ActivityUserEntity();
        try {
            chatCount = chatListAdapter.getItemCount();
            if (previousScreen.equalsIgnoreCase(getString(R.string.one2oneactivitydesigner)) && chatCount == 1) {
                activityuser.ActivityGuid = activityGuid;
                activityuser.IsUsersParsistenceRequire = true;
                mainActivity.UpdateHomeScreenForOneToOneChat(ChatMessagesActivity.this, activityuser);
                mainActivity.UpdateUserForOneToOneChat(ChatMessagesActivity.this, activityuser);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }*/
    }

    /*private void fillActionBarColor() {
        try {
            if (activityType.equalsIgnoreCase(getString(R.string.task_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.TaskDesignerThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.todo_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.TodoActivityThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase("BusinessActivity")) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.pothemecolor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.survey_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.SurveyThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.poll_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.PollThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.container_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.ContainerThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
//                    statusColor = getResources().getColor(R.color.communityActivityprimarydark);
                //floatActionButton.setBackgroundTintList(ColorStateList.valueOf(getColor(R.color.communityActivity)));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.eventplanner_task_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.TaskDesignerThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.eventplanner_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.EventConsumerThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.serviceOrderActivity))
                    || activityType.equalsIgnoreCase(getString(R.string.siteVisitActivity))
                    || activityType.equalsIgnoreCase(getString(R.string.siteInvoiceActivity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.serviceOrderThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.meetingactivity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.meetingThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.community_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.CommunityActivityThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.questionactivity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.QuestionThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.forum_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.ForumThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.leave_activity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.leaveThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.myteamActivity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.myteamThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            } else if (activityType.equalsIgnoreCase(getString(R.string.projectActivity))) {
                if (statusColor == null) {
                    statusColor = getResources().getString(R.string.projectThemeColor);
                }

                int color = MergeColors(Color.parseColor(statusColor), Color.parseColor("#33000000"));
                fillButtonColors(color, Color.parseColor(statusColor));
            }
//            if (ThemeCode != null && !ThemeCode.equalsIgnoreCase(""))
//                setThemeDetails(ThemeCode);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            String actualFileName = null, imagepath = null;
            if (requestCode == SELECT_FILE_GALLERY) {
                int width = 0, height = 0;
                try {
                    selectedImageUri = data.getData();
                    imagePath = getRealPathFromURI(selectedImageUri);
                    imagepath = imagePath;
                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inJustDecodeBounds = true;
                    bitmap = BitmapFactory.decodeFile(imagepath, options);
                    File file = new File(imagepath);
                    long length = file.length();
                    length = length / 1024;
                    // Find the correct scale value. It should be the power of 2.
                    int width_tmp = options.outWidth, height_tmp = options.outHeight;
                    int scale = 1;
                    if ((height_tmp > 400 && height_tmp < 1200) || (width_tmp > 250 && width_tmp < 656)) {
                        final int halfHeight = height_tmp / 2;
                        final int halfWidth = width_tmp / 2;
                        if ((halfHeight / scale) > 400
                                && (halfWidth / scale) > 350) {
                            scale = 4;
                        } else
                            scale = 2;

                    } else if ((height_tmp > 1200 && height_tmp < 2500) || (width_tmp > 656 && width_tmp < 1400)) {
                        final int halfHeight = height_tmp / 2;
                        final int halfWidth = width_tmp / 2;
                        if ((halfHeight / scale) > 1200
                                && (halfWidth / scale) > 656) {
                            scale = 7;
                        } else
                            scale = 4;
                    } else if ((height_tmp > 2500) || (width_tmp > 1400)) {
                        final int halfHeight = height_tmp / 2;
                        final int halfWidth = width_tmp / 2;
                        if ((halfHeight / scale) > 1200
                                && (halfWidth / scale) > 656) {
                            scale = 11;
                        } else
                            scale = 7;
                    }

                    // Decode with inSampleSize
                    BitmapFactory.Options o2 = new BitmapFactory.Options();
                    o2.inSampleSize = scale;
                    o2.inJustDecodeBounds = false;
                    bitmap = BitmapFactory.decodeFile(imagepath, o2);
                    actualFileName = imagepath.substring(imagepath
                            .lastIndexOf("/") + 1);

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    if (length > 500)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                    else if (length > 400)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 15, stream);
                    else if (length > 250)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, stream);
                    else if (length > 100)
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream);
                    else
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);

                   /* int length2 =bitmap.getByteCount();
                    length2 = length2 / 1024;*/
                    imageInByte = stream.toByteArray();
                    /*String toServerUnicodeEncoded = Base64.encodeToString(imageInByte,
                            Base64.DEFAULT);*/
                    int size = imageInByte.length;
                    SendButton(bitmap, imageInByte, actualFileName, imagePath);
                } catch (Exception e) {
                    mainActivity.show_snakbar("Internal Error", parentlayout);
                } catch (OutOfMemoryError e) {
                    mainActivity.show_snakbar("Out of memory", parentlayout);
                }
            } else if (requestCode == SELECT_FILE_CAMERA) {
                try {

                    app_preference = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
                    String currentUri = app_preference.getString("currentImageUri", "");
                    currentImageUri = Uri.parse(currentUri);
                    galleryAddPic();

                    if (mCurrentPhotoPath == null) {
                        imagePath = currentImageUri.getPath();
                    } else
                        imagePath = mCurrentPhotoPath;

                    final BitmapFactory.Options options = new BitmapFactory.Options();
                    options.inSampleSize = 8;
                    bitmap = BitmapFactory.decodeFile(imagePath, options);
                    actualFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
                    // Bitmap Reducedbitmap = getResizedBitmap(bitmap, 30);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
                    imageInByte = stream.toByteArray();
                    SendButton(bitmap, imageInByte, actualFileName, imagePath);
                } catch (Exception ex) {
                    ex.printStackTrace();
                } catch (OutOfMemoryError e) {
                    mainActivity.show_snakbar("Out of memory", parentlayout);
                }

            } else if (requestCode == REQUEST_PICK_FILE) {
                try {
                    selectedImageUri = data.getData();
                    if (Build.VERSION.SDK_INT >= 19) {
                        imagePath = getRealPathFromURI(selectedImageUri);
                    } else
                        imagePath = data.getData().getPath();

                    actualFileName = imagePath.substring(imagePath
                            .lastIndexOf("/") + 1);
                    if (actualFileName.toString().endsWith("pdf")
                            || actualFileName.toString().endsWith("doc")
                            || actualFileName.toString().endsWith("docx")
                            || actualFileName.toString().endsWith("ppt")
                            || actualFileName.toString().endsWith("pptx")
                            || actualFileName.toString().endsWith("xls")
                            || actualFileName.toString().endsWith("xlsx")
                            || actualFileName.toString().endsWith(".txt")) {
                        File file = new File(imagePath);
                        imageInByte = FileUtils.readFileToByteArray(file);

                        Log.i("f-imagepath", "image path" + imagePath);
                        Log.i("f-imagepath", "actual filename" + actualFileName);
                        SendButton(bitmap, imageInByte, actualFileName, imagePath);
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    return;
                } catch (OutOfMemoryError e) {
                    mainActivity.show_snakbar("Out of memory", parentlayout);
                }
            } else if (requestCode == SELECT_PLACE_PICKER) {

          /*      final Place place = PlacePicker.getPlace(data, this);
                final CharSequence name = place.getName();
                final CharSequence address = place.getAddress();

                Log.i("place", "complete address" + place.getName() + "/" + place.getAddress() + "/" + place.getLatLng());
                saveLocation(place.getName().toString(), place.getAddress().toString(), place.getLatLng().toString());*/
            } else if (requestCode == SELECT_FILE_AUDIO) {
                selectedImageUri = data.getData();
                imagePath = getRealPathFromURI(selectedImageUri);
                Log.i("tag", "selected image uri" + selectedImageUri);
                Log.i("tag", "image path" + imagePath);
                actualFileName = imagePath.substring(imagePath
                        .lastIndexOf("/") + 1);
                SendButton(null, null, actualFileName, imagePath);
            } else if (requestCode == SELECT_CONTACT_PICKER) {

                Log.i("tag", "come form share contacts" + data);
                Log.i("tag", "come form share contacts" + data.getSerializableExtra("dataobj"));
                if (data != null && data.getSerializableExtra("dataobj") != null) {
                    List<Map> mapList = (List<Map>) data.getSerializableExtra("dataobj");
                    if (mapList != null && mapList.size() > 0) {
                        for (int i = 0; i < mapList.size(); i++) {
                            saveContact((HashMap) mapList.get(i));
                        }
                    }
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        }
    }


    public String getAudioPath(Uri uri) {
        String[] projection = {Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }


    // saving contact

    @TargetApi(Build.VERSION_CODES.KITKAT)
    private String getRealPathFromURI(Uri contentURI) {

        String filePath = "";
        Cursor cursor = null;
        try {
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;
            String[] column = {Images.Media.DATA};
            if (isKitKat
                    && DocumentsContract.isDocumentUri(ChatMessagesActivity.this,
                    contentURI)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    if ("primary".equalsIgnoreCase(type)) {
                        filePath = Environment.getExternalStorageDirectory()
                                + "/" + split[1];
                    } else {
                        File file = Environment.getExternalStorageDirectory();
                        String fileExtSDPath = System
                                .getenv("SECONDARY_STORAGE");
                        if ((null == fileExtSDPath)
                                || (fileExtSDPath.length() == 0)) {
                            fileExtSDPath = System
                                    .getenv("EXTERNAL_SDCARD_STORAGE");
                        }
                        filePath = file + "/" + split[1];
                    }
                }
                // if (Build.VERSION.SDK_INT >= 19)
                else if (isDownloadsDocument(contentURI)) {
                    String wholeID = DocumentsContract
                            .getDocumentId(contentURI);
                    String[] id = wholeID.split(":");
                    final String type = id[0];
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"),
                            Long.valueOf(wholeID));
                    cursor = getContentResolver().query(contentUri, column,
                            null, null, null);
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                } else if (isMediaDocument(contentURI)) {
                    final String docId = DocumentsContract
                            .getDocumentId(contentURI);
                    final String[] split = docId.split(":");
                    final String type = split[0];
                    String id = docId.split(":")[1];
                    String sel = Images.Media._ID + "=?";
                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        cursor = getContentResolver().query(
                                Images.Media.EXTERNAL_CONTENT_URI,
                                column, sel, new String[]{id}, null);
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                        cursor = getContentResolver().query(contentUri, column,
                                null, null, null);
                    }
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                }
            } else {
                cursor = getContentResolver().query(contentURI, column, null, null, null);
                if (cursor == null) { // Source is Dropbox or other similar local file path
                    filePath = contentURI.getPath();
                } else {
                    int columnIndex = cursor.getColumnIndex(column[0]);
                    if (cursor.moveToFirst()) {
                        filePath = cursor.getString(columnIndex);
                    }
                    cursor.close();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return filePath;
    }

    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 0) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void SendButton(Bitmap finalBitmap, byte[] bitmapdata, String actualFName, String filePath) {
        String toServerUnicodeEncoded = null;
        try {
            // filename = actualFName;
            toServerUnicodeEncoded = Base64.encodeToString(bitmapdata, Base64.DEFAULT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        String dateformat = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        String fName = "ZM" + dateformat;
        if (actualFName.endsWith("jpg") || actualFName.endsWith("jpeg")
                || actualFName.endsWith("png") || actualFName.endsWith("gif")) {
            filename = fName + "." + "jpg";
        } else if (actualFName.endsWith("pdf")) {
            filename = fName + "." + "pdf";
        } else if (actualFName.endsWith("doc")) {
            filename = fName + "." + "doc";
        } else if (actualFName.endsWith("docx")) {
            filename = fName + "." + "docx";
        } else if (actualFName.endsWith("txt")) {
            filename = fName + "." + "txt";
        } else if (actualFName.endsWith("ppt")) {
            filename = fName + "." + "ppt";
        } else if (actualFName.endsWith("pptx")) {
            filename = fName + "." + "pptx";
        } else if (actualFName.endsWith("xls")) {
            filename = fName + "." + "xls";
        } else if (actualFName.endsWith("xlsx")) {
            filename = fName + "." + "xlsx";
        } else if (actualFName.endsWith(".mp4")) {
            filename = fName + "." + "mp4";
        } else if (actualFName.endsWith(".3gp")) {
            filename = fName + "." + "mp4";
        } else if (actualFName.endsWith(".avi")) {
            filename = fName + "." + "mp4";
        } else if (actualFName.endsWith(".mkv")) {
            filename = fName + "." + "mp4";
        } else if (actualFName.endsWith(".mp3")) {
            filename = fName + "." + "mp3";
        } else if (actualFName.endsWith(".ogg")) {
            filename = fName + "." + "mp3";
        } else if (actualFName.endsWith(".m4a")) {
            filename = fName + "." + "mp3";
        } else if (actualFName.endsWith(".amr")) {
            filename = fName + "." + "mp3";
        } else if (actualFName.endsWith(".3gpp")) {
            filename = fName + "." + "mp3";
        } else {
            mainActivity.show_snakbar("File format not supported", parentlayout);
            return;
        }

        // saving compressed image in local
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 1;


        Log.i("filename", "filename is" + filename);
        Log.i("imagepath", "imagepath is" + imagePath);


        String afterCompressUrl = "";
        // saving image into gallery and get url
        try {
            if (filename.endsWith(".jpg") || filename.endsWith(".png")
                    || filename.endsWith(".jpeg") || filename.endsWith(".gif")) {
                Bitmap bitmap2 = compressImage(imagePath);
                afterCompressUrl = mainActivity.SaveImageGallery(bitmap2, null, filename, imagePath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        final ChatMessageEntity chatMessage = new ChatMessageEntity();
       /* chatMessage.ActivityGuid = activityGuid;
        chatMessage.ParentActivityGuid = "no_parent_";*/
        //   chatMessage.MessageBody = filename + "," + toServerUnicodeEncoded;
        chatMessage.PasswordSalt = MocehatCrmMainActivity.PasswordSalt;
        chatMessage.MessageType = "Picture";
        chatMessage.SenderId = "1";
        chatMessage.ReceiverId = RecieverId;
        chatMessage.UserType = "Pharmacy";

        // chatMessage.SystemGenerated = false;

        try {
            if (filename.endsWith(".jpg") || filename.endsWith(".png") || filename.endsWith(".jpeg") || filename.endsWith(".gif")) {
                //  chatMessage.ImagePath = Environment.getExternalStorageDirectory().toString() + "/Xampr/Xampr Images/" + filename;
                chatMessage.MediaUploadStatus = 1;
                chatMessage.MessageBody = Environment.getExternalStorageDirectory().toString() + "/MoCehatCRM/MoCehatCRM Images/" + filename;
                chatMessage.ChatPictureBinary = convertOriginaImageToBinary(chatMessage.MessageBody);
                chatMessage.MimeType = "image/jpg";
                try {
                    String[] yourArray = filename.split("\\.");
                    chatMessage.SeoFilename = yourArray[0];
                } catch (Exception e) {
                    if (filename.endsWith(".jpg")) {
                        String name = filename.replace(".jpg", "");
                        chatMessage.SeoFilename = name;
                    } else {
                        if (filename.endsWith(".png")) {
                            String name = filename.replace(".png", "");
                            chatMessage.SeoFilename = name;
                        }
                    }
                }
                chatMessage.PictureType = "Pharmacy_Mobile";
                chatMessage.APIFor = "ChatPicture";
                chatMessage.LoginType = "email";
                chatMessage.ThumbNailBinary = chatMessage.ChatPictureBinary;


            }

            if (filename.endsWith(".doc") || filename.endsWith(".pdf") || filename.endsWith(".docx") ||
                    filename.endsWith(".txt") || filename.endsWith(".ppt") || filename.endsWith(".pptx")
                    || filename.endsWith(".xls") || filename.endsWith(".xlsx")) {
                try {
                   /* File file = new File(imagePath);
                    byte[] imageInBytes;
                    imageInBytes = FileUtils.readFileToByteArray(file);
                    mainActivity.SaveImageGallery(null, imageInBytes, filename, imagePath);
                    chatMessage.ImagePath = Environment.getExternalStorageDirectory().toString() + "/MoCehatCRM/MoCehatCRM Documents/" + filename;
                    Log.i("media path", "media path" + chatMessage.ImagePath);
                    chatMessage.MediaUploadStatus = 1;
                    chatMessage.MessageBody = Environment.getExternalStorageDirectory().toString() + "/MoCehatCRM/MoCehatCRM Documents/" + filename;*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        long currenTime = System.currentTimeMillis();
        long dotNetTicks = 621355968000000000L + currenTime * 10000;
        chatMessage.MessageTimeInLong = String.valueOf(dotNetTicks);
        chatMessage.MessageTime = chatMessage.MessageTimeInLong;

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date resultdate = new Date(currenTime);
        chatMessage.MessageTimeInDate = sdf.format(resultdate);

        Log.i("filename before", "filename" + filename);
        Log.i("filename before", "filepath" + filePath);
        Log.i("filename after save ", "filename" + afterCompressUrl);


        if (!mainActivity.isInternetConnected(ChatMessagesActivity.this)) {
            chatMessage.IsPresistanceRequired = true;
            chatMessage.MediaUploadStatus = 2;
        }
        chatMessage.chatItemPosition = chatMessages.size() - 1;

        try {

            final MocehatCrmDBHelper merchantDBHelper = new MocehatCrmDBHelper(this);
            // long local_id = merchantDBHelper.InsertActivityMessagesWithGetId(chatMessage);
            long local_id = 0;
            chatbox.setText("");
            //chatListAdapter.refresh(chatMessage);
            // chatListAdapter.refreshItemchange(chatMessage);
            //chatMessages=mainActivity.getAdminChatActivityMessages(ChatMessagesActivity.this,activityGuid);
            chatListAdapter.refresh(chatMessage);
            recyclerView.scrollToPosition(chatListAdapter.getItemCount() - 1);
            if (afterCompressUrl.isEmpty())
                afterCompressUrl = imagePath;
            if (mainActivity.isInternetConnected(ChatMessagesActivity.this)) {
                uploadFilesMethod(chatMessage, filename, afterCompressUrl, this, local_id, chatMessages.size() - 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void uploadFilesMethod(final ChatMessageEntity chatMessage, final String filename, String afterCompressUrl, final Context context, long local_id, int reloadItemPosition) {

        int mPosition = (int) local_id;
        chatMessageHashMap.put(mPosition, reloadItemPosition);

        final MocehatCrmMainActivity mainActivity1 = new MocehatCrmMainActivity();

        //  chatMessages = mainActivity1.getActivityMessages(context, chatMessage.ActivityGuid, false);

        final MocehatCrmDBHelper zibMiDBHelper = new MocehatCrmDBHelper(context);


        new UploadingMediaImageFile(chatMessage) {
            //  ChatMessageEntity chatMessageAdmin = new ChatMessageEntity();

            @Override
            public void onResponseReceived(String result) {
                Log.i("response is", "response" + result);
                if (result != null && result.length() > 0) {

                    JSONObject responseJson;
                    try {
                        String jsonPicId;

               /* ChatMessageEntity sendPicIdChatEntity = new ChatMessageEntity();*/
                        if (result != null && result.contains("Success")) {
                            try {

                                responseJson = new JSONObject(result);
                                if (responseJson.has("Id") && (responseJson.getString("Id")) != "null") {
                                    chatMessage.MediaId = Integer.valueOf(responseJson.getString("Id"));
                                    chatMessage.ImagePath = responseJson.getString("ImagePath");

                                }
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }

                            if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                                chatMessage.UserType = "Pharmacy";
                                chatMessage.APIFor = "SendChat";
                            }
                            chatMessage.ChatPictureBinary = null;
                            chatMessage.ThumbNailBinary = null;
                            chatMessage.PasswordSalt = MocehatCrmMainActivity.PasswordSalt;
                            chatMessage.MessageType = "Picture";
                            chatMessage.ReceiverId = RecieverId;
                            gson = new GsonBuilder().disableHtmlEscaping().create();
                            String json = gson.toJson(chatMessage);
                            new PersistMessagesNew(json, getApplicationContext(), 0, UserType) {
                                @Override
                                public void onResponseReceived(String result, long local_id) {
                                    try {
                                        if (result != null && result.contains("Success") || result.contains("Sent")) {
                                            JSONObject responseJson = new JSONObject(result);
                                            if (responseJson.has("Id") && (responseJson.getString("Id")) != "null") {
                                                chatMessage.Id = responseJson.getString("Id");

                                            }
                                            chatMessage.CreatedOnUtc = mainActivity.getCurrentTime(getApplicationContext());
                                            // chatMessage.ActivityGuid = activityGuid;
                                            mainActivity.addsendermessaget(getApplicationContext(), chatMessage);
                                        }
                                        chatMessages = mainActivity.getAdminChatActivityMessages(getApplicationContext(), RecieverId);
                                        chatListAdapter.refreshAll(chatMessages);
                                    } catch (Exception e) {

                                    }


                                }
                            }.execute();

                        } else {
                            Toast.makeText(getApplicationContext(), getString(R.string.upload_fail), Toast.LENGTH_SHORT).show();

                            imageInByte = null;
                            bitmap = null;
                            imagePath = null;
                        }

                    } /*catch (JSONException ex) {
                        ex.printStackTrace();
                    }*/ catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    // Image not uploaded in blob server
                  /*  long localid = zibMiDBHelper.updateLocalTableChatMessageStatusFail(local_id, chatMessage.ImagePath);
                    if (localid == 1) {
                        int ide = (int) local_id;
                        int listMapId = getReloadItem(ide);
                        chatMessages.get(listMapId).MediaUploadStatus = 2;
                        chatMessages.get(listMapId).MessageBody = chatMessage.ImagePath;
                        chatListAdapter.notifyItemChanged(listMapId);
                        Toast.makeText(context, "Uploading Failed", Toast.LENGTH_LONG).show();

                    }*/
                }

            }

        }.execute();

       /* new UploadFileClassOnlyForChat(filename, afterCompressUrl, this, local_id) {
            @Override
            public void onResponseReceived(String result, long local_id) {
                Log.i("response is", "response" + result);
                if (result != null && result.length() > 0) {
                    try {
                        chatMessage.MessageBody = mainActivity1.getEncryptedText(result);
                        if (filename.endsWith(".mp3") || filename.endsWith(".mp4")) {
                            try {
                                File file = new File(imagePath);
                                byte[] imageInBytes;
                                imageInBytes = FileUtils.readFileToByteArray(file);
                                mainActivity1.SaveImageGallery(null, imageInBytes, filename, imagePath);
                                chatMessage.ImagePath = Environment.getExternalStorageDirectory().toString() + "/MoCehatCRM/MoCehatCRM Audio/" + filename;
                                Log.i("media path", "media path" + chatMessage.ImagePath);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        final String messageJson = gson.toJson(chatMessage);
                        PersistMessagesNew persistMessagesNew = new PersistMessagesNew(messageJson, context, local_id) {
                            @Override
                            public void onResponseReceived(String result, long local_id) {

                                if (result != null && result.length() > 0) {
                                    try {
                                        Gson gson = new Gson();
                                        UserActivitiesRequest response = new UserActivitiesRequest();
                                        response = gson.fromJson(result, UserActivitiesRequest.class);
                                        if (response.Status.contains("Success")) {

                                            // update timeticks
                                            try {
                                               *//* ChatMessageEntity chatMessageEntity = gson.fromJson(messageJson, ChatMessageEntity.class);
                                                chatMessageEntity.MessageTime = response.MessageTime;
                                                MerchantDBHelper zibMiDBHelper = new MerchantDBHelper(context,null,null,0);
                                                zibMiDBHelper.updateChatMessage(chatMessageEntity, chatMessageEntity.MessageTimeInLong);

                                                zibMiDBHelper.ClearMessageDirtyBits(chatMessageEntity.ActivityGuid, chatMessageEntity.MessageTime);

                                                /// update home screen last message
                                                UserActivityEntity userActivityEntity = new UserActivityEntity();
                                                userActivityEntity.ActivityGuid = chatMessageEntity.ActivityGuid;
                                                userActivityEntity.LastMessage = chatMessageEntity.MessageBody;
                                                userActivityEntity.LastMessageBy = chatMessageEntity.MessageFrom;
                                                userActivityEntity.LastMessageTime = chatMessageEntity.MessageTime;
                                                userActivityEntity.TimeStampLong = chatMessageEntity.MessageTime;
                                                mainActivity1.updateIsInvitedAfterAccept(context, userActivityEntity, false);
*//*

                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }


                                            long localid = zibMiDBHelper.updateLocalTableChatMessageStatusSuccess(local_id, chatMessage.MessageBody);
                                            if (localid == 1) {
                                                int ide = (int) local_id;
                                                // chatListAdapter.refresh(chatMessage);
                                                int listMapId = getReloadItem(ide);
                                                try {
                                                    *//*if (chatMessages != null && chatMessages.size() > 0) {
                                                        mainActivity1.UpdateHomeScreenLastMessage(ChatMessagesActivity.this, chatMessage, false, null);
                                                        initiateOneToOneChat();

                                                        chatMessages.get(listMapId).MediaUploadStatus = 3;
                                                        chatMessages.get(listMapId).MessageBody = chatMessage.MessageBody;
                                                        chatListAdapter.notifyItemChanged(listMapId);
                                                    }
                                                    removeItemFromChatMessageMap(listMapId);*//*
                                                } catch (Exception ex) {
                                                    ex.printStackTrace();
                                                }

                                                // zibMiDBHelper.ClearMessageDirtyBits(chatMessage.ActivityGuid, chatMessage.MessageTime);

                                            }

                                        }


                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }


                                } else {
                                    // Image not uploaded in application  server
                                    long localid = zibMiDBHelper.updateLocalTableChatMessageStatusFail(local_id, chatMessage.ImagePath);
                                    try {
                                        if (localid == 1) {
                                            int ide = (int) local_id;
                                            int listMapId = getReloadItem(ide);
                                            chatMessages.get(listMapId).MediaUploadStatus = 2;
                                            chatMessages.get(listMapId).MessageBody = chatMessage.ImagePath;
                                            chatListAdapter.notifyItemChanged(listMapId);
                                            Toast.makeText(context, "Uploading Failed", Toast.LENGTH_LONG).show();

                                        }
                                    } catch (Exception ex) {
                                        ex.printStackTrace();
                                    }

                                }
                            }
                        };

                        persistMessagesNew.execute();


                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    // Image not uploaded in blob server
                    long localid = zibMiDBHelper.updateLocalTableChatMessageStatusFail(local_id, chatMessage.ImagePath);
                    if (localid == 1) {
                        int ide = (int) local_id;
                        int listMapId = getReloadItem(ide);
                        chatMessages.get(listMapId).MediaUploadStatus = 2;
                        chatMessages.get(listMapId).MessageBody = chatMessage.ImagePath;
                        chatListAdapter.notifyItemChanged(listMapId);
                        Toast.makeText(context, "Uploading Failed", Toast.LENGTH_LONG).show();

                    }
                }
            }
        }.execute();*/

    }

    private int getReloadItem(int localId) {
        Set<Integer> keys = chatMessageHashMap.keySet();  //get all keys
        for (Integer i : keys) {
            if (i == localId) {
                System.out.println(chatMessageHashMap.get(i));
                return Integer.parseInt(chatMessageHashMap.get(i).toString());
            }
        }

        return 0;
    }

    private void removeItemFromChatMessageMap(int localid) {

        if (chatMessageHashMap != null) {
            Set<Integer> keys = chatMessageHashMap.keySet();  //get all keys
            for (Integer i : keys) {
                if (i == localid) {
                    System.out.println(chatMessageHashMap.get(i));
                    chatMessageHashMap.remove(i);
                    Log.i("tag", "after upload map" + chatMessageHashMap);
                }
            }
        }

    }

    // this method for save location
    private void saveLocation(String name, String address, String latlang) {
       /* final ChatMessageEntity chatMessage = new ChatMessageEntity();
        chatMessage.ActivityGuid = activityGuid;
       // chatMessage.ParentActivityGuid = "no_parent_";
        //   chatMessage.MessageBody = filename + "," + toServerUnicodeEncoded;
      //  chatMessage.MessageFrom = userKeyDetails.getNickname();
        chatMessage.MessageType = "Picture";
        chatMessage.SenderId = loginCredentials.getPharmacyId();
      //  chatMessage.SystemGenerated = false;
        chatMessage.ImagePath = "";
        long currenTime = System.currentTimeMillis();
        long dotNetTicks = 621355968000000000L + currenTime * 10000;
        chatMessage.MessageTimeInLong = String.valueOf(dotNetTicks);
        chatMessage.MessageTime = chatMessage.MessageTimeInLong;
        chatMessage.MessageBody = mainActivity.getEncryptedText(name + "#" + address + "#" + latlang + ".map");

        if (chatMessages != null)
            chatMessage.chatItemPosition = chatMessages.size() - 1;

        String messageJson = gson.toJson(chatMessage);
        Log.i("message json", "messag json is" + messageJson);
     //   new PersistChatMessages(messageJson, ChatMessagesActivity.this).execute();
        mainActivity.addChatMessageToDB(ChatMessagesActivity.this, chatMessage);
        chatbox.setText("");
        chatMessages=mainActivity.getAdminChatActivityMessages(ChatMessagesActivity.this,activityGuid);

        chatListAdapter.refresh(chatMessage);
        recyclerView.scrollToPosition(chatListAdapter.getItemCount() - 1);*/

    }

    private void saveContact(HashMap hashMap) {
      /*  final ChatMessageEntity chatMessage = new ChatMessageEntity();
        chatMessage.ActivityGuid = activityGuid;
        chatMessage.ParentActivityGuid = "no_parent_";
        //   chatMessage.MessageBody = filename + "," + toServerUnicodeEncoded;
        chatMessage.MessageFrom = userKeyDetails.getNickname();
        chatMessage.MessageType = "MediaType";
        chatMessage.SenderId = userKeyDetails.getUserid();
        chatMessage.SystemGenerated = false;
        chatMessage.ImagePath = "";
        long currenTime = System.currentTimeMillis();
        long dotNetTicks = 621355968000000000L + currenTime * 10000;
        chatMessage.MessageTimeInLong = String.valueOf(dotNetTicks);
        chatMessage.MessageTime = chatMessage.MessageTimeInLong;
        chatMessage.MessageBody = mainActivity.getEncryptedText(hashMap.get("name") + "@" + hashMap.get("phone") + ".contact");
        if (chatMessages != null)
            chatMessage.chatItemPosition = chatMessages.size() - 1;

        String messageJson = gson.toJson(chatMessage);
        Log.i("message json", "messag json is" + messageJson);
        chatMessage.IsPresistanceRequired = true;
        if (mainActivity.isInternetConnected(ChatMessagesActivity.this)) {
            new PersistChatMessages(messageJson, this).execute();
        }
        mainActivity.addChatMessageToDB(ChatMessagesActivity.this, chatMessage);
        chatbox.setText("");
        chatListAdapter.refresh(chatMessage);
        recyclerView.scrollToPosition(chatMessages.size() - 1);*/

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = Images.Media.insertImage(inContext.getContentResolver(),
                inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(Uri uri) {
        String[] projection = {Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void setEmojiconFragment(boolean useSystemDefault) {

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.emojicons,
                        EmojiconsFragment.newInstance(useSystemDefault))
                .commit();

    }

    public String getEmojiByUnicode(int unicode) {
        return new String(Character.toChars(unicode));
    }

    public void scroll(int position) {
        recyclerView.smoothScrollToPosition(position);
    }

    @Override
    public void onIconClicked(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }

        toggleSelection(position);
    }

    @Override
    public void onIconImportantClicked(int position) {

    }

    @Override
    public void onMessageRowClicked(int position) {
        chatMessages = mainActivity.getAdminChatActivityMessages(ChatMessagesActivity.this, RecieverId);
        // verify whether action mode is enabled or not
        // if enabled, change the row state to activated

        if (chatListAdapter.getSelectedItemCount() > 0) {
            enableActionMode(position);
        } else {
            // read the message which removes bold from the row
            //  Message message = messages.get(position);
            //  message.setRead(true);
            //  messages.set(position, message);
            chatListAdapter.notifyDataSetChanged();

            //Toast.makeText(getApplicationContext(), "Read: " + message.getMessage(), Toast.LENGTH_SHORT).show();
        }

        int count = chatListAdapter.getSelectedItemCount();

        if (count > 1) {
            actionMode.getMenu().getItem(0).setVisible(false);
            actionMode.getMenu().getItem(2).setVisible(false);
        } else {
            if (actionMode != null)
                actionMode.getMenu().getItem(0).setVisible(true);
            String values = "";
            paretnJsonObject = new JSONObject();
            SparseBooleanArray sparseBooleanArray1 = chatListAdapter.getSelectedItemsListData();
            try {
                int position2 = sparseBooleanArray1.keyAt(position);
                ChatMessageEntity chatMessageEntity = chatMessages.get(position);
                values += chatMessageEntity.MessageBody + "\n";
                paretnJsonObject.put("Id", chatMessageEntity.Id);

                paretnJsonObject.put("MessageTime", chatMessageEntity.CreatedOnUtc);
                if (chatMessageEntity.MessageType.equalsIgnoreCase("Message")) {

                    paretnJsonObject.put("MessageBody", chatMessageEntity.MessageBody);
                } else {
                    paretnJsonObject.put("ImagePath", chatMessageEntity.ImagePath);
                    paretnJsonObject.put("MessageBody", chatMessageEntity.ImagePath);

                }
                paretnJsonObject.put("MessageSenderId", chatMessageEntity.SenderId);
                paretnJsonObject.put("MessageType", chatMessageEntity.MessageType);
                if (chatMessageEntity.SenderId.equalsIgnoreCase("1")) {
                    paretnJsonObject.put("MessageSenderName", "Admin");

                } else {
                    paretnJsonObject.put("MessageSenderName", RecieverName);

                }
                paretnJsonObject.put("Position", "" + position2);

                String val = paretnJsonObject.get("MessageBody").toString();
                if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg")) {
                    actionMode.getMenu().getItem(2).setVisible(true);
                } else {
                    actionMode.getMenu().getItem(2).setVisible(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        if (actionMode != null) {
            if (chatListAdapter.getSelectedItemCount() == 0)
                actionMode.getMenu().getItem(0).setVisible(true);
        }

    }


    private void enableActionMode(int position) {
        if (actionMode == null) {
            actionMode = startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }


    private void toggleSelection(int position) {


        chatListAdapter.toggleSelection(position);
        int count = chatListAdapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();

        }
    }

    @Override
    public void onRowLongClicked(int position) {
        try {


            enableActionMode(position);
            chatMessages = mainActivity.getAdminChatActivityMessages(ChatMessagesActivity.this, RecieverId);
            if (chatListAdapter.getSelectedItemCount() <= 1) {
                ChatMessageEntity chatMessageEntity = chatMessages.get(position);
                String imageFormat;
                if (chatMessageEntity.MessageBody != null) {
                    imageFormat = chatMessageEntity.MessageBody;
                } else {
                    imageFormat = chatMessageEntity.ImagePath;
                }
                if (imageFormat.endsWith(".jpg") || imageFormat.endsWith(".png") || imageFormat.endsWith(".jpeg")) {
                    actionMode.getMenu().getItem(2).setVisible(true);
                } else {
                    actionMode.getMenu().getItem(2).setVisible(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetChatMessagesFromLocalStorage() {
        try {

            chatMessages = mainActivity.getAdminChatActivityMessages(getApplicationContext(), RecieverId);
            if (chatMessages.size() > 0) {
                showProgress = false;
                chatListAdapter = new ChatListAdapter1(this, null, chatMessages, previousScreen, this);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
                recyclerView.setAdapter(chatListAdapter);
                LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
                recyclerView.setLayoutManager(linearLayoutManager);
                recyclerView.scrollToPosition(chatMessages.size() - 1);
            } else {
                showProgress = true;
            }
            // sch.scheduleAtFixedRate(runnableCode, 0, 1, TimeUnit.SECONDS);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void updateScreenFromReceiver(ChatMessageEntity entity) {
        try {


            if (entity != null) {
                chatListAdapter.refresh(entity);
                recyclerView.scrollToPosition(chatListAdapter.getItemCount() - 1);
                if (!entity.SenderId.equalsIgnoreCase("1")) {
                    entity.ReceiverId = "1";
                } else {
                    entity.ReceiverId = RecieverId;
                }
                dbHelper.updateChatNotificationMessage(entity);


            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void UpdateChatScreenWithMessages() {

        // List<ChatMessageEntity> chatMessages = new ArrayList<ChatMessageEntity>();
        try {
            //  chatMessages = mainActivity.getActivityMessages(ChatMessagesActivity.this, activityGuid, true);
            //  ChatListAdapter chatListAdapter = new ChatListAdapter(this,chatMessages,activityname,previousScreen,activityGuid);
            //  recyclerView.setAdapter(chatListAdapter);

            //  LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            //  recyclerView.setLayoutManager(linearLayoutManager);
            //  chatListAdapter.refreshAll(chatMessages);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    protected void showDownloadedDocumentFile(File getDocumentFilePath) {
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String ext = MimeTypeMap.getFileExtensionFromUrl(getDocumentFilePath
                .getName());
        String type = map.getMimeTypeFromExtension(ext);
        if (type == null)
            type = "*/*";
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(getDocumentFilePath), type);
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            mainActivity.show_snakbar("Please install proper document reader in your device to open this file", parentlayout);
        }

    }

    protected void showDownloadedImageFile(File getImageFilePath) {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        String ext = getImageFilePath.getName()
                .substring(getImageFilePath.getName().indexOf(".") + 1)
                .toLowerCase();
        String type = mime.getMimeTypeFromExtension(ext);
        intent.setDataAndType(Uri.fromFile(getImageFilePath), type);
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            mainActivity.show_snakbar("Please install proper document reader in your device to open this file", parentlayout);
        }

    }

    private Uri createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss")
                .format(new Date());
        File storageDir = Environment
                .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = null;
        if (storageDir.exists()) {
            image = File.createTempFile(timeStamp, /* prefix */
                    ".jpg", /* suffix */
                    storageDir /* directory */
            );
        } else {
            storageDir = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
            if (storageDir.exists()) {
                image = File.createTempFile(timeStamp, /* prefix */
                        ".jpg", /* suffix */
                        storageDir /* directory */
                );
            }
        }
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return Uri.fromFile(image);
    }

    public void onBackPressed() {
        super.onBackPressed();
        Intent intent;
        try {

            intent = new Intent(ChatMessagesActivity.this, HomeScreen.class);
            // intent.putExtra(getString(R.string.tabPosition), 1);
            Bundle bndlanimation = ActivityOptions.makeCustomAnimation(
                    getApplicationContext(), R.anim.back_swipe2, R.anim.back_swipe1).toBundle();
            startActivity(intent, bndlanimation);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void UpdateJson() {
       /* ActivityDetails activityDetails;
        String questionFinishJson = null;
        try {
            activityDetails = new ActivityDetails();
            if (isPin) {
                activityentity.IsPinnedActivity = true;
            } else {
                activityentity.IsPinnedActivity = false;
            }
            activityDetails.ActivityBasicInfo = activityentity;
            activityDetails.ActivityUserDetailInfo = activityUserInfo;
            activityDetails.IsActivityPersistanceRequired = true;
            Gson gson = new Gson();
            questionFinishJson = gson.toJson(activityDetails);
            mainActivity.PersistActivityLocally(
                    ChatMessagesActivity.this, questionFinishJson);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        try {
            getMenuInflater().inflate(R.menu.actionmenu_chat, menu);
            pinnedIcon = menu.findItem(R.id.pinnedIcon);
            menuMoreCommunityButoon = menu.findItem(R.id.menu_overflow_community_activity);
            menuEditCommunityItem = menu.findItem(R.id.action_edit_community);
            menuCloseQuestionItem = menu.findItem(R.id.action_Close);
            menuOpenQuestionItem = menu.findItem(R.id.action_Open);
            menuEditQuestionItem = menu.findItem(R.id.action_edit_question);
            menuMoreQuestionActButton = menu.findItem(R.id.menu_overflow_question_activity);
            menuActivityInfo = menu.findItem(R.id.action_activity_info);

            menuActivityInfo.setVisible(false);
            menuMoreQuestionActButton.setVisible(false);
            pinnedIcon.setVisible(false);

           /* if (mainActivity.isAlreadyPinned(ChatMessagesActivity.this, activityentity.ActivityGuid))
                pinnedIcon.setIcon(getResources().getDrawable(R.drawable.unpinned_icon));
            else
                pinnedIcon.setIcon(getResources().getDrawable(R.drawable.pinned_icon));*/



              /*  pinnedIcon.setVisible(false);
                menuMoreCommunityButoon.setVisible(false);
                menuActivityInfo.setVisible(false);
                menuMoreQuestionActButton.setVisible(false);*/

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.pinnedIcon:
                pinThisActivity();
                return true;

            case R.id.action_activity_info:
                gotoInfo();
                return true;
            case R.id.action_Close:
                // finishQuestion(true);
                return true;
            case R.id.action_Open:
                //  finishQuestion(false);
                return true;
            case R.id.action_edit_question:
                EditQuestionActivity();
                return true;
            case R.id.action_edit_community:
                EditCommunityActivity();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void pinThisActivity() {
      /*  UserActivityEntity userActivityEntity = new UserActivityEntity();
        boolean isPinActivityExceed = false, isPinnedAlready = false;
        try {
            isPinnedAlready = mainActivity.isAlreadyPinned(ChatMessagesActivity.this, activityentity.ActivityGuid);
            isPinActivityExceed = mainActivity.getPinnedActivitiesCount(ChatMessagesActivity.this, isPinnedAlready);
            if (isPinActivityExceed)
                mainActivity.show_snakbar(getString(R.string.pinnedNotmorethanten), parentlayout);
            else {
                isActivityUpdated = true;
                if (isPinnedAlready) {
                    isPin = false;
                    userActivityEntity.IsPinnedActivity = false;
                    pinnedIcon.setIcon(getResources().getDrawable(R.drawable.pinned_icon));
                } else {
                    isPin = true;
                    userActivityEntity.IsPinnedActivity = true;
                    pinnedIcon.setIcon(getResources().getDrawable(R.drawable.unpinned_icon));
                }
                userActivityEntity.ActivityGuid = activityentity.ActivityGuid;
                mainActivity.insertPinnedActivitiesToDB(ChatMessagesActivity.this, userActivityEntity);
                String[] activityDetails = userDetails.split("\\^");
                new LongOperationGetPinnedActivity(getApplicationContext(), activityGuid, activityentity.ActivityType, activityentity.UserType, activityentity.ParentActivityGuid, activityDetails[1], isPin).execute();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }*/
    }
   /* private void ShowActivityDescription() {
        if (tv_activitydescription != null) {
            startActivity(new Intent(ChatMessagesActivity.this, ReadMore.class)
                    .addFlags(
                            Intent.FLAG_ACTIVITY_CLEAR_TOP
                                    | Intent.FLAG_ACTIVITY_SINGLE_TOP)
                    .putExtra("$moretxt", tv_activitydescription));
        } else
            mainActivity.show_snakbar("There is no description in this activity", parentlayout);

    }*/

    private void gotoInfo() {
        try {
           /* ActivityEntity activityInfo = mainActivity.getActivityDetailsFromDB(ChatMessagesActivity.this, activityentity.ActivityGuid);
            activityentity.OwnerGuid = activityInfo.OwnerGuid;
            activityentity.ActivityDescription = activityInfo.ActivityDescription;
            Intent intent = new Intent(ChatMessagesActivity.this, ActivityInformation.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.putExtra(getString(R.string.activityJson), getLatestJson(ChatMessagesActivity.this, activityDetails, activityentity));
            intent.putExtra("lastMessage", LastMessage);
            intent.putExtra("lastMessageBy", LastMessageBy);
            intent.putExtra("messageTime", LastMessageTime);
            intent.putExtra("userDetails", userDetails);
            intent.putExtra("imageInByte", getActivityIcon);
            intent.putExtra("Color", statusColor);
            Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(),
                    R.anim.next_swipe2, R.anim.next_swipe1).toBundle();
            startActivity(intent, bndlanimation);*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }


    private void EditQuestionActivity() {
        Intent intent;
        try {
           /* intent = new Intent(ChatMessagesActivity.this, QuestionActivityDesigner.class);
            intent.putExtra(getString(R.string.activityGuid), activityGuid);
            intent.putExtra(getString(R.string.isEdit), true);
            intent.putExtra("Color", statusColor);
            startActivity(intent);*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void EditCommunityActivity() {
        Intent intent;
        byte[] imageInByte = null;
        String getImageInBase64 = null;
        try {
          /*  ActivityEntity activityInfo = mainActivity.getActivityDetailsFromDB(ChatMessagesActivity.this, activityGuid);
            getImageInBase64 = activityInfo.ActivityIcon;
            if (getImageInBase64 != null) {
                String stringNoQuotes = getImageInBase64.replaceAll("^\"|\"$", "");
                imageInByte = Base64.decode(stringNoQuotes, Base64.DEFAULT);
            }
            intent = new Intent(ChatMessagesActivity.this, CommunityActivityDesigner.class);
            intent.putExtra(getString(R.string.activityGuid), activityGuid);
            intent.putExtra("themedetails", activityInfo.ActivityThemeCode);
            intent.putExtra("imageInByte", imageInByte);

            intent.putExtra(getString(R.string.isEdit), true);
            startActivity(intent);*/
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        mediaScanIntent.setData(currentImageUri);
        sendBroadcast(mediaScanIntent);
    }

    private void showPopup() {


        //  mBottomSheetDialog.show();
        /*
        final PopupWindow showPopup = PopupHelper
                .newBasicPopupWindow(ChatMessagesActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(
                R.layout.popup_attachement_chatactivity, null);




        showPopup.setContentView(popupView);
        showPopup.setWidth(LayoutParams.WRAP_CONTENT);
        showPopup.setHeight(LayoutParams.WRAP_CONTENT);
        showPopup.setAnimationStyle(R.style.Animations_GrowFromTop);
        showPopup.showAsDropDown(view);
        galleryLayout = (RelativeLayout) popupView
                .findViewById(R.id.gallerylayout);
        cameraLayout = (RelativeLayout) popupView
                .findViewById(R.id.cameralayout);
        videoLayout = (RelativeLayout) popupView.findViewById(R.id.videolayout);
        audioLayout_popUp = (RelativeLayout) popupView
                .findViewById(R.id.audiolayout);
        documentLayout = (RelativeLayout) popupView
                .findViewById(R.id.documentlayout);
        galleryLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    isGallery = true;
                    getPhotoFromGallery();
                } else
                    Toast.makeText(
                            ChatMessagesActivity.this,
                            "Please turn off USB storage or insert your SD card and try again",
                            Toast.LENGTH_SHORT).show();
                showPopup.dismiss();
                return;

            }
        });
        cameraLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);

                if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ActivityCompat.checkSelfPermission(ChatMessagesActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    // permissions have not been granted.
                    requestPermission();
                } else {
                    if (isSDPresent) {
                        isGallery = true;
                        getPhotoFromCamera();
                    } else {
                        Toast.makeText(ChatMessagesActivity.this,
                                "Please turn off USB storage or insert your SD card and try again",
                                Toast.LENGTH_SHORT).show();
                    }
                    showPopup.dismiss();
                    return;
                }

            }
        });
        videoLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    isVideo = true;
                    isVideo = true;
                    getVideoFromGallery();
                } else
                    Toast.makeText(
                            ChatMessagesActivity.this,
                            "Please turn off USB storage or insert your SD card and try again",
                            Toast.LENGTH_SHORT).show();
                showPopup.dismiss();
                return;

            }
        });
        documentLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    getDocumentFromPhone();
                } else
                    Toast.makeText(
                            ChatMessagesActivity.this,
                            "Please turn off USB storage or insert your SD card and try again",
                            Toast.LENGTH_SHORT).show();
                showPopup.dismiss();
                return;
            }
        });

        audioLayout_popUp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                isSDPresent = Environment.getExternalStorageState()
                        .equals(Environment.MEDIA_MOUNTED);
                if (isSDPresent) {
                    isAudio = true;
                    getAudioFromGallery();
                } else
                    Toast.makeText(
                            ChatMessagesActivity.this,
                            "Please turn off USB storage or insert your SD card and try again",
                            Toast.LENGTH_SHORT).show();
                showPopup.dismiss();
                return;
            }
        });
*/
    }

    @Override
    public void onEmojiconClicked(Emojicon emojicon) {
        EmojiconsFragment.input(chatbox, emojicon);
        SendButton.setVisibility(View.VISIBLE);
        attachmentButton.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onEmojiconBackspaceClicked(View v) {
        EmojiconsFragment.backspace(chatbox);

    }

    private void getAudioFromGallery() {

        Intent intent = new Intent();
        intent.setType("audio/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(
                Intent.createChooser(intent, "Select audio to upload "),
                SELECT_FILE_AUDIO);

    }

    private void getVideoFromGallery() {
        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, REQUEST_VIDEO_CAPTURE);
        }
    }

    private void getPhotoFromCamera() {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Ensure that there's a camera activity to handle the
            // intent
            if (takePictureIntent
                    .resolveActivity(getPackageManager()) != null) {
                // Create the File where the photo should go
                try {
                    currentImageUri = createImageFile();
                    //Save in shared preference
                    SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                    editor.putString("currentImageUri", String.valueOf(currentImageUri));
                    editor.commit();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                // Continue only if the File was successfully
                // created
                if (currentImageUri != null) {
                    takePictureIntent.putExtra(
                            MediaStore.EXTRA_OUTPUT,
                            currentImageUri);
                    startActivityForResult(takePictureIntent,
                            SELECT_FILE_CAMERA);

                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getPhotoFromGallery() {
        try {
            if (Build.VERSION.SDK_INT < 19) {
                Intent intent = new Intent();
                intent.setType("image/*,video/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(
                        Intent.createChooser(intent, "Select file to upload "),
                        SELECT_FILE_GALLERY);
            } else {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(Intent.createChooser(intent, "Select picture to upload "), SELECT_FILE_GALLERY);
               /* Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("image*//*,video*//*");
                startActivityForResult(intent, SELECT_FILE_GALLERY);*/
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void requestPermission() {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                ActivityCompat.requestPermissions(ChatMessagesActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            } else {
                ActivityCompat.requestPermissions(ChatMessagesActivity.this, PERMISSIONS, PERMISSION_REQUEST_CODE);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                try {

                    for (int i = 0; i < permissions.length; i++) {
                        String permission = permissions[i];
                        int grantResult = grantResults[i];
                        if (permission.equals(Manifest.permission.CAMERA)) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                //Toast.makeText(UploadLiscenceActivity.this, getString(R.string.cameraPermAccept), Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(UploadLiscenceActivity.this, getString(R.string.cameraPermDeny), Toast.LENGTH_LONG).show();
                            }
                        } else if (permission.equals(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                //Toast.makeText(UploadLiscenceActivity.this, getString(R.string.storageWritePermAccept), Toast.LENGTH_LONG).show();
                            } else {
                                //Toast.makeText(UploadLiscenceActivity.this, getString(R.string.storageWritePermDeny), Toast.LENGTH_LONG).show();
                            }
                        } else if (permission.equals(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                            if (grantResult == PackageManager.PERMISSION_GRANTED) {
                                //Toast.makeText(UploadLiscenceActivity.this, getString(R.string.storageReadPermAccept), Toast.LENGTH_LONG).show();
                            } else {
                                // Toast.makeText(UploadLiscenceActivity.this, getString(R.string.storageReadPermDeny), Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void getDocumentFromPhone() {
        Intent intent;
        String manufacturer = Build.MANUFACTURER;
        if ((Build.VERSION.SDK_INT < 19)
                || (Build.VERSION.SDK_INT >= 19 && manufacturer
                .equalsIgnoreCase("Xiaomi"))) {
            intent = new Intent();
            intent.setType("application/pdf");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(
                    Intent.createChooser(intent, "Select file"),
                    REQUEST_PICK_FILE);
        } else {
            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            intent.putExtra(
                    Intent.EXTRA_MIME_TYPES,
                    new String[]{
                            "text/plain",
                            "application/msword",
                            "application/pdf",
                            "application/vnd.ms-powerpoint",
                            "application/vnd.ms-excel",
                            "application/xls",
                            "application/vnd.openxmlformats-officedocument.presentationml.presentation",
                            "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                            "application/vnd.openxmlformats-officedocument.wordprocessingml.document"});
        }
        startActivityForResult(intent, REQUEST_PICK_FILE);
    }

    private void checkKeyboardHeight(final View parentLayout) {

        parentLayout.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @Override
                    public void onGlobalLayout() {
                        int previousHeightDiffrence = 0;
                        try {
                            if (!isKeyBoardVisible) {
                                Rect r = new Rect();
                                parentLayout.getWindowVisibleDisplayFrame(r);
                                int screenHeight = parentLayout.getRootView().getHeight();
                                int heightDifference = screenHeight - (r.bottom);

                                if (previousHeightDiffrence - heightDifference > 50) {
                                    //popupWindow.dismiss();
                                }
                                previousHeightDiffrence = heightDifference;
                                if (heightDifference > 100) {
                                    isKeyBoardVisible = true;
                                    changeKeyboardHeight(heightDifference);
                                } else {
                                    isKeyBoardVisible = false;
                                }
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });

    }

    private void changeKeyboardHeight(int height) {
        try {
            if (height > 100) {
                ViewGroup.LayoutParams params = emojiconslayout.getLayoutParams();
                params.height = height;
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                emojiconslayout.setLayoutParams(params);
                emojiconslayout.setLayoutParams(params);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void showEmojiPopUp() {
        try {
            int deviceHeight = 0;
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            deviceHeight = size.y;
            emojiconslayout.getLayoutParams().height = (int) (deviceHeight / 2.5); // Setting the height of FrameLayout
            emojiconslayout.requestLayout();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            ChatStatus = true;
          /*  if (sch.isShutdown() && isBackgroundRunning) {
                sch = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(2);
                sch.scheduleAtFixedRate(runnableCode, 0, 1, TimeUnit.SECONDS);
            }*/
            List<ChatMessageEntity> newchatMessages = new ArrayList<ChatMessageEntity>();


  /*          for (int i = 0; i < newchatMessages.size(); i++) {
                ChatMessageEntity entity = newchatMessages.get(i);
                // entity.ActivityGuid = activityGuid;
                // entity.ActivityId = Integer.valueOf(activityGuid);
                dbHelper.updateChatNotificationMessage(entity);
                updateScreenFromReceiver(entity);

            }
*/

            dbHelper = new MocehatCrmDBHelper(ChatMessagesActivity.this);

            newchatMessages = dbHelper.GetChatActivityMessageNotification(RecieverId);
            for (int i = 0; i < newchatMessages.size(); i++) {
                ChatMessageEntity entity = newchatMessages.get(i);
                if (!entity.SenderId.equalsIgnoreCase("1")) {
                    entity.ReceiverId = "1";
                } else {
                    entity.ReceiverId = RecieverId;
                }
                dbHelper.updateChatNotificationMessage(entity);
                updateScreenFromReceiver(entity);

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        // TODO Auto-generated method stub
        super.onPause();

        try {
            bottomsheet.setVisibility(View.GONE);
            //   mBottomSheetDialog.dismiss();

            //isFirstTimeLoad = false;
            ChatStatus = false;
            //  sch.shutdown();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public Bitmap compressImage(String filePath) {
        Bitmap scaledBitmap = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

            int actualHeight = options.outHeight;
            int actualWidth = options.outWidth;
            float maxHeight = 816.0f;
            float maxWidth = 612.0f;
            float imgRatio = actualWidth / actualHeight;
            float maxRatio = maxWidth / maxHeight;

            if (actualHeight > maxHeight || actualWidth > maxWidth) {
                if (imgRatio < maxRatio) {
                    imgRatio = maxHeight / actualHeight;
                    actualWidth = (int) (imgRatio * actualWidth);
                    actualHeight = (int) maxHeight;
                } else if (imgRatio > maxRatio) {
                    imgRatio = maxWidth / actualWidth;
                    actualHeight = (int) (imgRatio * actualHeight);
                    actualWidth = (int) maxWidth;
                } else {
                    actualHeight = (int) maxHeight;
                    actualWidth = (int) maxWidth;

                }
            }

            options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[16 * 1024];

            try {
                bmp = BitmapFactory.decodeFile(filePath, options);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();

            }
            try {
                scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError exception) {
                exception.printStackTrace();
            }

            float ratioX = actualWidth / (float) options.outWidth;
            float ratioY = actualHeight / (float) options.outHeight;
            float middleX = actualWidth / 2.0f;
            float middleY = actualHeight / 2.0f;

            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

            Canvas canvas = new Canvas(scaledBitmap);
            canvas.setMatrix(scaleMatrix);
            canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


            ExifInterface exif;
            try {
                exif = new ExifInterface(filePath);

                int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
                Log.i("EXIF", "Exif: " + orientation);
                Matrix matrix = new Matrix();
                if (orientation == 6) {
                    matrix.postRotate(90);
                    Log.i("EXIF", "Exif: " + orientation);
                } else if (orientation == 3) {
                    matrix.postRotate(180);
                    Log.i("EXIF", "Exif: " + orientation);
                } else if (orientation == 8) {
                    matrix.postRotate(270);
                    Log.i("EXIF", "Exif: " + orientation);
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(filePath);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return scaledBitmap;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        try {
            if (height > reqHeight || width > reqWidth) {
                final int heightRatio = Math.round((float) height / (float) reqHeight);
                final int widthRatio = Math.round((float) width / (float) reqWidth);
                inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            }
            final float totalPixels = width * height;
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return inSampleSize;
    }


    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.menu_action_mode, menu);
            int statusBarColor;
            //statusBarColor = getWindow().getStatusBarColor();
            //set your gray color
            // getWindow().setStatusBarColor(0xFF555555);
            //  getWindow().setStatusBarColor(statusBarColor);

            //   getWindow().setStatusBarColor(ContextCompat.getColor(getBaseContext(), R.color.zibylight));
            // getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#33c5ff")));

            // disable swipe refresh if action mode is enabled
            //    swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {


            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {


                case R.id.action_copy:
                    Log.i("Data", "values are" + chatListAdapter.getSelectedItemsListData());
                    paretnJsonObject = new JSONObject();

                    SparseBooleanArray sparseBooleanArray = chatListAdapter.getSelectedItemsListData();
                    try {
                        String resultData = "";
                        if (sparseBooleanArray != null) {

                            for (int m = 0; m < sparseBooleanArray.size(); m++) {
                                int position = sparseBooleanArray.keyAt(m);
                                for (int i = 0; i < chatMessages.size(); i++) {
                                    if (position == i) {
                                        ChatMessageEntity chatMessageEntity = chatMessages.get(i);
                                        if (chatMessageEntity.MessageType.toString().equalsIgnoreCase("Message"))
                                            resultData += mainActivity.getDecryptedText(chatMessageEntity.MessageBody) + "\n";

                                        break;
                                    }
                                }
                            }
                        }

                        resultData = resultData.replace("\n", "").replace("\r", "");
                        try {
                            Log.i("tag", "result copy data after" + resultData);
                            resultData = URLDecoder.decode(resultData, "UTF-8");
                            resultData = StringEscapeUtils.unescapeJava(resultData);
                            String finaldata = resultData;
                            myClip = ClipData.newPlainText("text", finaldata.trim());
                            myClipboard.setPrimaryClip(myClip);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        //  resultData.replaceAll("\\+", " ");

                        /*
                        String finaldata = "";
                        finaldata = resultData.replace("+", " ");
                        finaldata = finaldata.replace("%0A", " ");
                        Log.i("result", "final data" + finaldata);
                        try {
                            finaldata = URLDecoder.decode(finaldata, "UTF-8");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        */


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mode.finish();

                    return true;


                case R.id.action_share:

                    JSONObject jsonObject = new JSONObject();
                    Log.i("Data", "values are" + chatListAdapter.getSelectedItemsListData());
                    SparseBooleanArray sparseBooleanArray2 = chatListAdapter.getSelectedItemsListData();
                    try {
                        String values = "";
                        if (sparseBooleanArray2 != null) {

                            for (int m = 0; m < sparseBooleanArray2.size(); m++) {
                                int position = sparseBooleanArray2.keyAt(m);
                                for (int i = 0; i < chatMessages.size(); i++) {
                                    if (position == i) {
                                        ChatMessageEntity chatMessageEntity = chatMessages.get(i);
                                        values += chatMessageEntity.MessageBody + "\n";

                                        jsonObject.put("MessageTime", chatMessageEntity.MessageTime);
                                        jsonObject.put("MessageBody", chatMessageEntity.MessageBody);
                                        jsonObject.put("MessageSenderId", chatMessageEntity.SenderId);
                                        jsonObject.put("MessageType", chatMessageEntity.MessageType);
                                        if (chatMessageEntity.SenderId.equalsIgnoreCase("1")) {
                                            jsonObject.put("MessageSenderName", "Admin");

                                        } else {
                                            jsonObject.put("MessageSenderName", RecieverName);

                                        }
                                        jsonObject.put("Position", "" + position);
                                        //  paretnJsonObject.put("MessageURL",chatMessageEntity.MessageURL);

                                        break;
                                    }
                                }
                            }
                        }

                        try {
                            String val = jsonObject.get("MessageBody").toString();
//                            File f = new File(val.trim());
                            Uri uri = Uri.parse(val.trim());
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            intent.setType("image/*");
                            //intent.putExtra(Intent.EXTRA_SUBJECT, "One Healthcare App that you must have on your phone");
                            intent.putExtra(Intent.EXTRA_STREAM, uri);
                            startActivity(Intent.createChooser(intent, "Share via"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    return true;
                case R.id.action_reply:
                    replayViewLayout.setVisibility(View.VISIBLE);
                    paretnJsonObject = new JSONObject();
                    Log.i("Data", "values are" + chatListAdapter.getSelectedItemsListData());
                    SparseBooleanArray sparseBooleanArray1 = chatListAdapter.getSelectedItemsListData();
                    try {
                        String values = "";
                        if (sparseBooleanArray1 != null) {

                            for (int m = 0; m < sparseBooleanArray1.size(); m++) {
                                int position = sparseBooleanArray1.keyAt(m);
                                for (int i = 0; i < chatMessages.size(); i++) {
                                    if (position == i) {
                                        ChatMessageEntity chatMessageEntity = chatMessages.get(i);
                                        values += mainActivity.getDecryptedText(chatMessageEntity.MessageBody) + "\n";
                                        paretnJsonObject.put("Id", chatMessageEntity.Id);

                                        paretnJsonObject.put("MessageTime", chatMessageEntity.CreatedOnUtc);

                                        if (chatMessageEntity.MessageType.equalsIgnoreCase("Message")) {

                                            paretnJsonObject.put("MessageBody", chatMessageEntity.MessageBody);
                                        } else {
                                            paretnJsonObject.put("MessageBody", chatMessageEntity.ImagePath);
                                            paretnJsonObject.put("ImagePath", chatMessageEntity.ImagePath);

                                        }

                                        paretnJsonObject.put("MessageSenderId", chatMessageEntity.SenderId);
                                        paretnJsonObject.put("MessageType", chatMessageEntity.MessageType);
                                        if (chatMessageEntity.SenderId.equalsIgnoreCase("1")) {
                                            paretnJsonObject.put("MessageSenderName", "Admin");

                                        } else {
                                            paretnJsonObject.put("MessageSenderName", RecieverName);

                                        }
                                        paretnJsonObject.put("Position", "" + position);
                                        //  paretnJsonObject.put("MessageURL",chatMessageEntity.MessageURL);

                                        break;
                                    }
                                }
                            }
                        }

                        String returnType = mainActivity.getImageBasedOnMessageBody(paretnJsonObject.get("MessageBody").toString());
                        if (returnType.toString().equalsIgnoreCase("Text")) {
                            values = values.replace("\n", "").replace("\r", "");
                            values = URLDecoder.decode(values, "UTF-8");
                            String resultData = StringEscapeUtils.unescapeJava(values);
                            replySendUserMessageLayout.setVisibility(View.VISIBLE);
                            replySendMediaMessageLayout.setVisibility(View.GONE);
                            replySendUserMessageText.setText(resultData);
                        } else {
                            replySendUserMessageLayout.setVisibility(View.GONE);
                            replySendMediaMessageLayout.setVisibility(View.VISIBLE);
                            String valappend = "";
                            String val = mainActivity.getDecryptedText(paretnJsonObject.get("MessageBody").toString());
                            if (val != null) {
                                val = val.replace("\"", "");
                                if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(val.trim())
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_image);

                                } else if (val.toString().endsWith("pdf")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pdf_download)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);

                                } else if (val.toString().endsWith("doc")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.docfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("docx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.docxfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("ppt")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pptfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("pptx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pptxfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("xls")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.xlsfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("xlsx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.xlsxfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith("txt")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.txtfile)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_file);
                                } else if (val.toString().endsWith(".map")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.map_new)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_location);
                                    valappend = val.split("#")[0];
                                } else if (val.toString().endsWith(".mp3")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_audio)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_location);
                                } else if (val.toString().endsWith(".mp4")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_audio)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_location);
                                } else if (val.toString().endsWith(".contact")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_contact)
                                            .into(replyshowImageView);
                                    replySendMediaMessageImage.setBackgroundResource(R.drawable.xwall_lastmessage_contact);
                                    valappend = val.split("@")[0];
                                }
                                if (valappend != null && valappend.toString().trim().length() > 0)
                                    replySendMediaMessageText.setText(returnType + " : " + valappend);
                                else
                                    replySendMediaMessageText.setText(returnType);

                                // replySendMediaMessageFrom.setText(MainActivity.usersMap.get(paretnJsonObject.get("MessageSenderId")).NickName);
                                replySendMediaMessageFrom.setText("Sender");
                                if (statusColor != null)
                                    replySendMediaMessageFrom.setTextColor(Color.parseColor(statusColor));
                            }
                        }
                        /*if (paretnJsonObject.get("MessageType").toString().equalsIgnoreCase("Message")) {

                            String finaldata = "";

                           *//*
                            finaldata = values.replace("+", " ");
                            finaldata = finaldata.replace("%0A", " ");
                            try {
                                finaldata = URLDecoder.decode(finaldata, "UTF-8");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            *//*


                            values = values.replace("\n", "").replace("\r", "");
                            values = URLDecoder.decode(values, "UTF-8");
                            String resultData = StringEscapeUtils.unescapeJava(values);

                            pasteitem.setVisibility(VISIBLE);
                            pasteitem.setText(resultData);
                            replyshowImageView.setVisibility(View.GONE);
                        } else {
                            replyshowImageView.setVisibility(View.VISIBLE);
                            pasteitem.setVisibility(View.GONE);
                            String val = mainActivity.getDecryptedText(paretnJsonObject.get("MessageBody").toString());
                            if (val != null) {
                                val = val.replace("\"", "");
                                if (val.endsWith(".jpg") || val.endsWith(".png") || val.endsWith(".jpeg")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(val.trim())
                                            .into(replyshowImageView);
                                } else if (val.toString().endsWith("pdf")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pdf_download)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("doc")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.docfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("docx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.docxfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("ppt")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pptfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("pptx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.pptxfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("xls")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.xlsfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("xlsx")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.xlsxfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith("txt")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.txtfile)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith(".map")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.map_new)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith(".mp3")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_audio)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith(".mp4")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_audio)
                                            .into(replyshowImageView);

                                } else if (val.toString().endsWith(".contact")) {
                                    Picasso.with(ChatMessagesActivity.this)
                                            .load(R.drawable.bottom_sheet_contact)
                                            .into(replyshowImageView);
                                }
                            }
                        }*/
                        Log.i("result", "result data" + values);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    mode.finish();

                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            chatListAdapter.clearSelections();
            actionMode = null;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    // mAdapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public class UploadingMediaFile extends AsyncTask<String, Boolean, Boolean> {
        int responseCode = 0;
        String result = "", fileName, createdFileName;
        boolean response = false;
        int serverResponseCode;
        File sourceFile;
        URL url;
        HttpURLConnection connection = null;
        DataOutputStream dos = null;
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;


        public UploadingMediaFile(String fName) {
            fileName = fName;
        }

        protected void onPreExecute() {
            super.onPreExecute();
            try {
                /*pdialogue = new ProgressDialog(ChatMessagesActivity.this);
                pdialogue.setMessage("Uploading...");
                pdialogue.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                pdialogue.setCancelable(true);*/
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            //pdialogue.show();
        }

        protected Boolean doInBackground(String... params) {
            try {
                String actualFileName = imagePath.substring(imagePath.lastIndexOf("/") + 1);
                //Create connection
                ////lutfa ---url needs to change
                url = new URL(MocehatCrmMainActivity.WEB_API_URL
                        + "image");
                connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);

                if (actualFileName.toString().endsWith(".jpg")
                        || actualFileName.toString().endsWith(".png")
                        || actualFileName.toString().endsWith(".jpeg")
                        || actualFileName.toString().endsWith(".gif")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/jpg");
                    connection.setRequestProperty("Content-type", "application/jpg");
                    connection.setRequestProperty("FileType", "jpg");
                } else if (actualFileName.toString().endsWith("pdf")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/pdf");
                    connection.setRequestProperty("Content-type", "application/pdf");
                    connection.setRequestProperty("FileType", "pdf");
                } else if (actualFileName.toString().endsWith("doc")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/doc");
                    connection.setRequestProperty("Content-type", "application/doc");
                    connection.setRequestProperty("FileType", "doc");
                } else if (actualFileName.toString().endsWith("docx")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/docx");
                    connection.setRequestProperty("Content-type", "application/docx");
                    connection.setRequestProperty("FileType", "docx");
                } else if (actualFileName.toString().endsWith("ppt")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/ppt");
                    connection.setRequestProperty("Content-type", "application/ppt");
                    connection.setRequestProperty("FileType", "ppt");
                } else if (actualFileName.toString().endsWith("pptx")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/pptx");
                    connection.setRequestProperty("Content-type", "application/pptx");
                    connection.setRequestProperty("FileType", "pptx");
                } else if (actualFileName.toString().endsWith("xls")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/xls");
                    connection.setRequestProperty("Content-type", "application/xls");
                    connection.setRequestProperty("FileType", "xls");
                } else if (actualFileName.toString().endsWith("xlsx")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/xlsx");
                    connection.setRequestProperty("Content-type", "application/xlsx");
                    connection.setRequestProperty("FileType", "xlsx");
                } else if (actualFileName.toString().endsWith("txt")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/txt");
                    connection.setRequestProperty("Content-type", "application/txt");
                    connection.setRequestProperty("FileType", "txt");
                } else if (actualFileName.toString().endsWith(".mp4")
                        || actualFileName.toString().endsWith(".3gp")
                        || actualFileName.toString().endsWith(".avi")
                        || actualFileName.toString().endsWith(".mkv")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/mp4");
                    connection.setRequestProperty("Content-type", "application/mp4");
                    connection.setRequestProperty("FileType", "mp4");
                } else if (actualFileName.toString().endsWith(".mp3")
                        || actualFileName.toString().endsWith(".ogg")
                        || actualFileName.toString().endsWith(".m4a")
                        || actualFileName.toString().endsWith(".amr")
                        || actualFileName.toString().endsWith(".3gpp")) {
                    createdFileName = fileName;
                    connection.setRequestProperty("Accept", "application/mp3");
                    connection.setRequestProperty("Content-type", "application/mp3");
                    connection.setRequestProperty("FileType", "mp3");
                } else {
                    return false;
                }
               /* connection.setRequestProperty("FileName", createdFileName);
                connection.setRequestProperty("ActivityGuid", activityentity.ActivityGuid);
                connection.setRequestProperty("ParentActivityGuid", activityentity.ParentActivityGuid);
                connection.setRequestProperty("ActivityName", activityentity.ActivityName);
                connection.setRequestProperty("ActivityType", activityentity.ActivityType);
                connection.setRequestProperty("OwnerGuid", userKeyDetails.getUserid());
                connection.setRequestProperty("OwnerName", userKeyDetails.getNickname());*/

                try {
                   /* UserPreferences userPreferences = new UserPreferences(ChatMessagesActivity.this);
                    if (userPreferences.getServerToken() != null && userPreferences.getAccessToken().length() > 0)
                        connection.setRequestProperty("token", userPreferences.getServerToken());*/
                } catch (Exception e) {
                    e.printStackTrace();
                }

                connection.connect();
                //Send request
                sourceFile = new File(imagePath);

                FileInputStream fileInputStream = new FileInputStream(sourceFile);
                dos = new DataOutputStream(connection.getOutputStream());
                // create a buffer of  maximum size
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];
                // read file and write it into form...
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                while (bytesRead > 0) {
                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                fileInputStream.close();
                dos.flush();
                dos.close();

                //Get Response

                responseCode = connection.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";
                }
                connection.disconnect();
                response = Boolean.valueOf(result);
            } catch (Exception e) {
                e.printStackTrace();
                //pdialogue.dismiss();
            }
            return response;
        }

        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            try {

                if (result) {
                    if (createdFileName.toString().endsWith(".jpg")
                            || createdFileName.toString().endsWith(".png")
                            || createdFileName.toString().endsWith(".jpeg")
                            || createdFileName.toString().endsWith(".gif")) {
                        BitmapFactory.Options options = new BitmapFactory.Options();
                        options.inSampleSize = 1;
                        bitmap = BitmapFactory.decodeFile(imagePath, options);
                        mainActivity.SaveImage(bitmap, null, createdFileName, imagePath);
                    } else if (createdFileName.toString().endsWith("pdf")
                            || createdFileName.toString().endsWith("doc")
                            || createdFileName.toString().endsWith("docx")
                            || createdFileName.toString().endsWith("ppt")
                            || createdFileName.toString().endsWith("pptx")
                            || createdFileName.toString().endsWith("xls")
                            || createdFileName.toString().endsWith("xlsx")
                            || createdFileName.toString().endsWith(".txt")) {
                        File file = new File(imagePath);
                        imageInByte = FileUtils.readFileToByteArray(file);
                        mainActivity.SaveImage(null, imageInByte, createdFileName, null);
                    } else if (createdFileName.toString().endsWith(".mp4")
                            || createdFileName.toString().endsWith(".3gp")
                            || createdFileName.toString().endsWith(".avi")
                            || createdFileName.toString().endsWith(".mkv")) {
                        File file = new File(imagePath);
                        imageInByte = FileUtils.readFileToByteArray(file);
                        mainActivity.SaveImage(null, imageInByte, createdFileName, null);
                    } else if (createdFileName.toString().endsWith(".mp3")
                            || createdFileName.toString().endsWith(".ogg")
                            || createdFileName.toString().endsWith(".m4a")
                            || createdFileName.toString().endsWith(".amr")
                            || createdFileName.toString().endsWith(".3gpp")) {
                        File file = new File(imagePath);
                        imageInByte = FileUtils.readFileToByteArray(file);
                        mainActivity.SaveImage(null, imageInByte, createdFileName, null);
                    }
                    filename = "";
                    imageInByte = null;
                    bitmap = null;
                    imagePath = null;
                } else {
                    mainActivity.show_snakbar("Upload failed", parentlayout);
                    filename = "";
                    imageInByte = null;
                    bitmap = null;
                    imagePath = null;
                }
                // pdialogue.dismiss();
            } catch (Exception ex) {
                ex.printStackTrace();
                //pdialogue.dismiss();
            }
        }
    }


    class SendChat {
        String time, text;

        public SendChat(String text, String time) {
            this.text = text;
            this.time = time;
        }

        String getText() {
            return text;
        }

        String getTime() {
            return time;
        }

        void setTime(String time) {
            this.time = time;
        }
    }

  /*  public class DownloadFileOperation extends
            AsyncTask<String, String, String> {
        StringBuilder s;
        String json;

        public DownloadFileOperation(String fileName) {
            fileToDownload = fileName;

        }

        protected void onPreExecute() {
            try {
                super.onPreExecute();
                pdialogue = ProgressDialog.show(ChatMessagesActivity.this, null, "Downloading...");
                pdialogue.setCancelable(true);
                FileDownloadRequest request = new FileDownloadRequest();
                request.ActivityGuid = activityGuid;
                request.FileName = fileToDownload;
                json = gson.toJson(request);
            } catch (Exception ex) {
                ex.printStackTrace();
                pdialogue.dismiss();
            }
        }

        @Override
        protected String doInBackground(String... params) {

            String result = "";
            URL url;
            StringBuffer stringBuffer = new StringBuffer();
            try {
                url = new URL(MainActivity.SITE_URL + getString(R.string.get_download_activity_attachment));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                try {
                    UserPreferences userPreferences = new UserPreferences(ChatMessagesActivity.this);
                    if (userPreferences.getServerToken() != null && userPreferences.getAccessToken().length() > 0)
                        conn.setRequestProperty("token", userPreferences.getServerToken());
                } catch (Exception e) {
                    e.printStackTrace();
                }

                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.connect();
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                // writer.write(getPostData(postDataParams));
                writer.write(json);
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return result;
        }

        protected void onPostExecute(String result) {
            try {
                Bitmap final_bitmap = null;
                if (result != null) {
                    if (fileToDownload.endsWith(".mp3")
                            || fileToDownload.endsWith(".ogg")
                            || fileToDownload.endsWith(".m4a")
                            || fileToDownload.endsWith(".amr")
                            || fileToDownload.endsWith(".3gpp")) {

                        String stringNoQuotes = result.replaceAll("^\"|\"$", "");
                        getByteArray = Base64.decode(stringNoQuotes, Base64.DEFAULT);
                        mainActivity.SaveImage(null, getByteArray, fileToDownload, null);
                        tv_fileName.setText(fileToDownload);
                        ChatMessageEntity chatMessage = new ChatMessageEntity();
                        chatMessage.ActivityGuid = activityGuid;
                        chatMessage.MessageTimeInLong = timeLong;
                        chatMessage.ImagePath = localFilePath;
                        mainActivity.addChatMessageToDB(ChatMessagesActivity.this,
                                chatMessage);

                    } else if (fileToDownload.toString().endsWith(".jpg")) {
                        String stringNoQuotes = result.replaceAll("^\"|\"$", "");
                        byte[] getByteArray = Base64.decode(stringNoQuotes, Base64.DEFAULT);
                        final_bitmap = BitmapFactory.decodeByteArray(getByteArray, 0, getByteArray.length);

                    *//*getByteArray = Base64.decode(stringNoQuotes, Base64.DEFAULT);
                    int DESIRED_WIDTH = 640;
                    BitmapFactory.Options sizeOptions = new BitmapFactory.Options();
                    sizeOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeByteArray(getByteArray, 0, getByteArray.length, sizeOptions);
                    float widthSampling = sizeOptions.outWidth / DESIRED_WIDTH;
                    sizeOptions.inJustDecodeBounds = false;
                    sizeOptions.inSampleSize = (int) widthSampling;
                    final_bitmap = BitmapFactory.decodeByteArray(getByteArray,
                            0, getByteArray.length, sizeOptions);*//*
                    }
                    if (final_bitmap != null) {
                        mainActivity.SaveImage(final_bitmap, null, fileToDownload, null);
                        String Fpath = Environment.getExternalStorageDirectory().toString() + "/Xampr/Xampr Images/" + fileToDownload;
                        File filecheck = new File(Fpath);
                        if (filecheck.exists()) {
                            showDownloadedImageFile(filecheck);
                        }
                    }
                }
                pdialogue.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
                pdialogue.dismiss();
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
                pdialogue.dismiss();
            }
        }
    }*/

    /*public void GetActivityData(String activityId, String screenName, boolean isChildRequired) {
        activityentity = mainActivity.getActivityDetailsFromDB(ChatMessagesActivity.this, activityId);
        activityDetails.ActivityUserDetailInfo = mainActivity.getUserActivityDetailsFromDB(ChatMessagesActivity.this, activityId);
        if (isChildRequired)
            activityDetails.ChildUserActivities = mainActivity.getChildUserActivitiesFromDB(ChatMessagesActivity.this, activityId, false);
        if (activityentity != null && activityentity.ActivityGuid != null) {
            mainActivity.GetActivityDetailsBackend(ChatMessagesActivity.this, activityId, screenName, false);
        } else {
            mainActivity.GetActivityDetailsBackend(ChatMessagesActivity.this, activityId, screenName, true);
        }
    }*/
    private class GetChatWithAdminSender extends AsyncTask<String, String, String> {
        ChatMessageEntity chatMessageAdmin;
        String toServerUnicodeEncoded = null;
        String json;

        public GetChatWithAdminSender(ChatMessageEntity chatMessageAdmin) {
            this.chatMessageAdmin = chatMessageAdmin;
        }

        protected void onPreExecute() {
            gson = new GsonBuilder().disableHtmlEscaping().create();
            json = gson.toJson(chatMessageAdmin);
            super.onPreExecute();
        }

        protected String doInBackground(String... param) {

            String result = "";
            URL url;
            try {
                if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                    url = new URL(MocehatCrmMainActivity.WEB_API_URL
                            + "chat");
                } else {
                    url = new URL(MocehatCrmMainActivity.WEB_API_URL
                            + "sendmessage");
                }


                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();
                conn.setReadTimeout(15000);
                conn.setConnectTimeout(15000);
                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                // writer.write(getPostData(postDataParams));
                writer.write(json);
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }
            } catch (Exception e) {
                Log.d("GetPinCode", "Error:" + e.getMessage());
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {


                if (result != null && result.contains("Success") || result.contains("Sent")) {
                    JSONObject responseJson = new JSONObject(result);
                    if (responseJson.has("Id") && (responseJson.getString("Id")) != "null") {
                        chatMessageAdmin.Id = responseJson.getString("Id");

                    }
                    chatMessageAdmin.CreatedOnUtc = mainActivity.getCurrentTime(getApplicationContext());
                    // chatMessageAdmin.ActivityGuid = activityGuid;
                    mainActivity.addsendermessaget(getApplicationContext(), chatMessageAdmin);
                }
                chatMessages = mainActivity.getAdminChatActivityMessages(getApplicationContext(), RecieverId);
                chatListAdapter.refreshAll(chatMessages);
            } catch (Exception e) {

            }

        }
    }


    private class GetChatWithAdminReciver extends AsyncTask<String, String, String> {
        ChatMessageEntity ChatWithAdminReceiver = new ChatMessageEntity();
        String json, lastChatReadtime;
        ProgressDialog progressDialog = new ProgressDialog(ChatMessagesActivity.this);

        protected void onPreExecute() {
            progressDialog.setMessage("Loading messages..");
            if (showProgress) {

                progressDialog.show();
            }
            progressDialog.setCancelable(false);
            lastChatReadtime = null;

            lastChatReadtime = app_preference.getString("Chat_" + UserType + RecieverId, "0");
            if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                ChatWithAdminReceiver.PasswordSalt = MocehatCrmMainActivity.PasswordSalt;
                ChatWithAdminReceiver.ReceiverId = RecieverId;
                ChatWithAdminReceiver.SenderId = "1";
                ChatWithAdminReceiver.APIFor = "ReceiveChat";
                ChatWithAdminReceiver.TimeStamp = lastChatReadtime;
                ChatWithAdminReceiver.UserType = "Pharmacy";

            } else {
                lastChatReadtime = app_preference.getString("Chat_" + UserType + RecieverId, null);
                ChatWithAdminReceiver.Passwordsalt = Passwordsalt;
                ChatWithAdminReceiver.StoreId = RecieverId;
                ChatWithAdminReceiver.CurrentTimeStampOnUtc = lastChatReadtime;
            }

            gson = new GsonBuilder().disableHtmlEscaping().create();
            json = gson.toJson(ChatWithAdminReceiver);
            super.onPreExecute();
        }

        protected String doInBackground(String... param) {

            String result = "";
            URL url;
            try {
                if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                    url = new URL(MocehatCrmMainActivity.WEB_API_URL
                            + "chat");
                } else {
                    url = new URL(MocehatCrmMainActivity.WEB_API_URL
                            + "receivechat");
                }
                HttpURLConnection conn = (HttpURLConnection) url
                        .openConnection();

                conn.setRequestMethod("POST");
                conn.setDoInput(true);
                conn.setDoOutput(true);

                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(os, "UTF-8"));
                // writer.write(getPostData(postDataParams));
                writer.write(json);
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        result += line;
                    }
                } else {
                    result = "";

                }
            } catch (Exception e) {
                Log.d("GetPinCode", "Error:" + e.getMessage());
                e.printStackTrace();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            try {
                if (progressDialog.isShowing()) {
                    progressDialog.dismiss();
                }
                ChatReceiverArrayList = new ArrayList<ChatMessageEntity>();
                Gson gson = new Gson();
                ChatMessageEntity response = gson.fromJson(result, ChatMessageEntity.class);
                List<ChatMessageEntity> chatList = null;
                if (response.chatList != null) {
                    chatList = response.chatList;
                } else if (response.ReceiveChats != null) {
                    chatList = response.ReceiveChats;
                }
                SharedPreferences.Editor edit = app_preference.edit();
                if (UserType != null && UserType.equalsIgnoreCase("Customer")) {
                    edit.putString("Chat_" + UserType + RecieverId, response.CurrentTimeStamp);
                    edit.commit();

                } /*else if (UserType != null && UserType.equalsIgnoreCase("Pharmacy")) {
                    edit.putString("Chat_" + RecieverId, response.TimeStamp);
                    edit.commit();

                }*/ else {
                    edit.putString("Chat_" + UserType + RecieverId, response.CurrentTimeStampOnUtc);
                    edit.commit();
                }

                if (chatList != null) {
                    if (showProgress) {
                        setAdapter(chatList);
                    }

                    for (ChatMessageEntity mychatreceiverEntity : chatList) {
                        try {
                            mychatreceiverEntity.Id = String.valueOf(mychatreceiverEntity.Id);

                            mainActivity.addsendermessaget(getApplicationContext(), mychatreceiverEntity);
                            ChatReceiverArrayList.add(mychatreceiverEntity);
                         /* if(chatListAdapter!=null){
                                chatListAdapter.refresh(mychatreceiverEntity);
                                recyclerView.scrollToPosition(chatMessages.size() - 1);
                            }
*/
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                    }
                }
              /*  if(chatListAdapter==null) {*/
                if (!showProgress) {
                    GetChatMessagesFromLocalStorage();
                }
                // }
              /*  chatListAdapter.refresh(ChatWithAdmin);
                recyclerView.scrollToPosition(chatMessages.size() - 1);*/

            } catch (Exception ex) {
                ex.printStackTrace();
            }

        }
    }

    private void setAdapter(List<ChatMessageEntity> chatList) {
        chatMessages = chatList;
        chatListAdapter = new ChatListAdapter1(ChatMessagesActivity.this, null, chatList, previousScreen, this);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(chatListAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.scrollToPosition(chatList.size() - 1);
    }

    private String convertOriginaImageToBinary(String localPath) {
        long length;
        byte[] buffer;
        String base64String = null;
        Bitmap originalBitmap = null;
        try {
            File imageFile = new File(localPath);
            length = imageFile.length();
            length = length / 1024;

            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            originalBitmap = BitmapFactory.decodeFile(localPath, options);

            final int REQUIRED_SIZE = 400;

            // Find the correct scale value. It should be the power of 2.
            int width_tmp = options.outWidth, height_tmp = options.outHeight;
            int scale = 1;
            while (true) {
                if (width_tmp / 2 < REQUIRED_SIZE
                        || height_tmp / 2 < REQUIRED_SIZE)
                    break;
                width_tmp /= 2;
                height_tmp /= 2;
                scale *= 2;
            }
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            o2.inJustDecodeBounds = false;
            originalBitmap = BitmapFactory.decodeFile(localPath, o2);


            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            if (length > 7000)
                originalBitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
                //BitmapFactory.decodeStream(fileInputStream).compress(Bitmap.CompressFormat.JPEG, 50);
            else if (length > 6000)
                bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
                //BitmapFactory.decodeStream(fileInputStream).compress(Bitmap.CompressFormat.JPEG, 65, dos);
            else if (length > 5000)
                originalBitmap.compress(Bitmap.CompressFormat.JPEG, 65, stream);
                //BitmapFactory.decodeStream(fileInputStream).compress(Bitmap.CompressFormat.JPEG, 75, dos);
            else if (length > 4500)
                originalBitmap.compress(Bitmap.CompressFormat.JPEG, 75, stream);
                //BitmapFactory.decodeStream(fileInputStream).compress(Bitmap.CompressFormat.JPEG, 90, dos);
            else if (length > 4000)
                originalBitmap.compress(Bitmap.CompressFormat.JPEG, 85, stream);
            else
                originalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, stream);
            //BitmapFactory.decodeStream(fileInputStream).compress(Bitmap.CompressFormat.JPEG, 95, dos);

            buffer = stream.toByteArray();
            base64String = Base64.encodeToString(buffer, Base64.DEFAULT);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return base64String;
    }
}


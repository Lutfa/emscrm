package com.emedsurecrm;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.emedsurecrm.model.ChatMessageEntity;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ahmed on 9/12/2017.
 */

public abstract class UploadingMediaImageFile extends AsyncTask<String, String, String> {
    ChatMessageEntity chatEntity;
    String toServerUnicodeEncoded = null;
    String json;
    Gson gson;

    public UploadingMediaImageFile(ChatMessageEntity chatMessageEntity) {
        chatEntity = chatMessageEntity;
    }

    protected void onPreExecute() {
        gson = new GsonBuilder().disableHtmlEscaping().create();
        json = gson.toJson(chatEntity);
        super.onPreExecute();
    }

    protected String doInBackground(String... param) {

        String result = "";
        URL url;
        try {
            url = new URL(MocehatCrmMainActivity.WEB_API_URL
                    + "chat");
            HttpURLConnection conn = (HttpURLConnection) url
                    .openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            // writer.write(getPostData(postDataParams));
            writer.write(json);
            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = "";

            }
        } catch (Exception e) {
            Log.d("GetPinCode", "Error:" + e.getMessage());
            e.printStackTrace();
        }
        return result;

    }

    @Override
    protected void onPostExecute(String result) {
        try {
            //  responseJson = new JSONObject(result);

            if (result != null) {
                //  progressDialog.dismiss();
                try {
                  //  String val = result.replace("\"", "");
                    onResponseReceived(result);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                //  progressDialog.dismiss();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public abstract void onResponseReceived(String result);

}
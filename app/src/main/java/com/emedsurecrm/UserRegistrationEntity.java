package com.emedsurecrm;

public class UserRegistrationEntity {

    public String PhoneNumber;

    public String UserGuid;

    public String NickName;

    public String DeviceGuid;

    public String BackEndName;

    public String EmailAddress;

    public boolean IsZibMiUser;

    public boolean IsLocal;

    public boolean NeedSync;

    public String InvitedUser;

    public boolean IsZibMiCommunity;

    public String ProfilePicture;

    public String OrganizationName;

    public String PersonalStatus;

    public String State;
    //used for company name
    public String Status;

    public UserRegistrationEntity Response;

    public String UserId;

    public String PartitionKey;
}

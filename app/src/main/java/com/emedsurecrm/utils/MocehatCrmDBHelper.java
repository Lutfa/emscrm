package com.emedsurecrm.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.emedsurecrm.ChatMessagesActivity;
import com.emedsurecrm.model.ChatMessageEntity;
import com.emedsurecrm.model.MyCustomerEntity;
import com.emedsurecrm.model.UserKeyDetails;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MocehatCrmDBHelper extends MocehatCrmDBHandler {
   /* LoginCredentials loginCredentials;*/

    public MocehatCrmDBHelper(Context context) {
        super(context);
       /* loginCredentials = LoginCredentials.getInstance(context);*/
    }

    public void saveCustomerDetails(MyCustomerEntity customerChatDetails) {
        SQLiteDatabase db = super.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = null;
        long check = 0;
        try {
            values.put(KEY_CUSTOMER_ID, customerChatDetails.CustomerId);
            values.put(KEY_CUSTOMER_NAME, customerChatDetails.CustomerName);
            values.put(KEY_LAST_MESSAGE, customerChatDetails.LastMessage);
            values.put(KEY_LAST_ACTIVITY_TIME, customerChatDetails.LastActivityTime);
            values.put(KEY_MESSAGE_COUNT, customerChatDetails.MessageCount);

            String query = "SELECT * FROM customerchatdetails WHERE CustomerId = '"
                    + customerChatDetails.CustomerId
                    + "'";
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst())
                db.update(TABLE_CUSTOMER_CHAT_DETAILS, values, "CustomerId = '"
                        + customerChatDetails.CustomerId + "'", null);
            else
                db.insertOrThrow(TABLE_CUSTOMER_CHAT_DETAILS, null, values);

        } catch (Exception ex) {
            Log.e("InsertActivityMessage", ex.getMessage());
        }
        values.clear();
        db.close();
        cursor.close();
    }

    public void savepharmacyDetails(MyCustomerEntity customerChatDetails) {
        SQLiteDatabase db = super.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = null;
        long check = 0;
        try {
            values.put(KEY_PHARMACY_ID, customerChatDetails.PharmacyId);
            values.put(KEY_PHARMACY_NAME, customerChatDetails.PharmacyName);
            values.put(KEY_LAST_MESSAGE, customerChatDetails.LastMessage);
            values.put(KEY_LAST_ACTIVITY_TIME, customerChatDetails.LastActivityTime);
            values.put(KEY_MESSAGE_COUNT, customerChatDetails.MessageCount);
            values.put(KEY_PASSWORD_SALT, customerChatDetails.Passwordsalt);

            String query = "SELECT * FROM pharmacychatdetails WHERE PharmacyId = '"
                    + customerChatDetails.PharmacyId
                    + "'";
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst())
                db.update(TABLE_PHARMACY_CHAT_DETAILS, values, "PharmacyId = '"
                        + customerChatDetails.PharmacyId + "'", null);
            else
                db.insertOrThrow(TABLE_PHARMACY_CHAT_DETAILS, null, values);

        } catch (Exception ex) {
            Log.e("InsertActivityMessage", ex.getMessage());
        }
        values.clear();
        db.close();
        cursor.close();
    }


    public ArrayList<MyCustomerEntity> getPharmacyChatDetailsList(String searchText) {
        ArrayList<MyCustomerEntity> mycustomerchatEntity = new ArrayList<MyCustomerEntity>();
        MyCustomerEntity item = new MyCustomerEntity();
        SQLiteDatabase db = super.getWritableDatabase();

        String selectQuery = null;
        if (!StringUtils.isEmpty(searchText)) {

            selectQuery = "SELECT * FROM pharmacychatdetails WHERE  PharmacyName like '%"
                    + searchText + "%' or PharmacyId like '%" + searchText + "%' ";

        } else {
            selectQuery = "SELECT * FROM pharmacychatdetails";
        }


        Cursor c = db.rawQuery(selectQuery, null);
        try {
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    item = new MyCustomerEntity();
                    item.PharmacyId = c.getString(c.getColumnIndex(KEY_PHARMACY_ID));
                    item.PharmacyName = c.getString(c.getColumnIndex(KEY_PHARMACY_NAME));
                    item.LastMessage = c.getString(c.getColumnIndex(KEY_LAST_MESSAGE));
                    item.LastActivityTime = c.getString(c.getColumnIndex(KEY_LAST_ACTIVITY_TIME));
                    item.MessageCount = c.getInt(c.getColumnIndex(KEY_MESSAGE_COUNT));
                    item.Passwordsalt = c.getString(c.getColumnIndex(KEY_PASSWORD_SALT));

                    mycustomerchatEntity.add(item);
                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (c != null)
            c.close();
        db.close();
        return mycustomerchatEntity;
    }

    public ArrayList<MyCustomerEntity> getCustomerChatDetailsList(String searchText) {
        ArrayList<MyCustomerEntity> mycustomerchatEntity = new ArrayList<MyCustomerEntity>();
        MyCustomerEntity item = new MyCustomerEntity();
        SQLiteDatabase db = super.getWritableDatabase();
       /* String selectQuery = "SELECT * FROM customerchatdetails";*/
        String selectQuery = null;
        if (!StringUtils.isEmpty(searchText)) {

            selectQuery = "SELECT * FROM customerchatdetails WHERE  CustomerName like '%"
                    + searchText + "%' or CustomerId like '%" + searchText + "%' ";

        } else {
            selectQuery = "SELECT * FROM customerchatdetails";
        }

        Cursor c = db.rawQuery(selectQuery, null);
        try {
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    item = new MyCustomerEntity();
                    item.CustomerId = c.getString(c.getColumnIndex(KEY_CUSTOMER_ID));
                    item.CustomerName = c.getString(c.getColumnIndex(KEY_CUSTOMER_NAME));
                    item.LastMessage = c.getString(c.getColumnIndex(KEY_LAST_MESSAGE));
                    item.LastActivityTime = c.getString(c.getColumnIndex(KEY_LAST_ACTIVITY_TIME));
                    item.MessageCount = c.getInt(c.getColumnIndex(KEY_MESSAGE_COUNT));

                    mycustomerchatEntity.add(item);
                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (c != null)
            c.close();
        db.close();
        return mycustomerchatEntity;
    }

    public List<ChatMessageEntity> GetAdminChatActivityMessage(String ReceiverId) {
        SQLiteDatabase db = super.getWritableDatabase();
        List<ChatMessageEntity> adminactivityMessages = new ArrayList<ChatMessageEntity>();
        Cursor c = null;
        try {
            //   c = db.rawQuery("select * from activitymessages where OrderId='" + "' Order By MessageTimeStamp asc", null);
            c = db.rawQuery("select * from activitymessages WHERE ReceiverId='" + ReceiverId + "'  Or MessageSenderId='" + ReceiverId + "'", null);
            int total = c.getCount();
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    ChatMessageEntity adminactivityMessage = new ChatMessageEntity();
                    adminactivityMessage.SenderId = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_SENDER_ID));
                    adminactivityMessage.ReceiverId = c.getString(c
                            .getColumnIndex(KEY_RECIEVER_ID));
                    adminactivityMessage.MessageBody = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_BODY));
                    adminactivityMessage.MessageType = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_TYPE));
                    adminactivityMessage.Id = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_ID));
                    adminactivityMessage.SenderName = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_SENDER_NAME));
//adminactivityMessage.UserType=c.getString(c.getColumnIndex(KEY_USER_TYPE));
                    adminactivityMessage.TimeStamp = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_TIME));
                    adminactivityMessage.ImagePath = c.getString(c
                            .getColumnIndex(KEY_MEDIA_PATH));
                    adminactivityMessage.Option2Value = c.getString(c
                            .getColumnIndex(KEY_OPTION1_VALUE));

                    adminactivityMessages.add(adminactivityMessage);

                    c.moveToNext();
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        db.close();
        if (c != null)
            c.close();
        return adminactivityMessages;

    }


    public List<ChatMessageEntity> GetChatActivityMessageNotification(String RecieverId) {
        SQLiteDatabase db = super.getWritableDatabase();
        List<ChatMessageEntity> adminactivityMessages = new ArrayList<ChatMessageEntity>();
        Cursor c = null;
        try {
            //   c = db.rawQuery("select * from activitymessages where OrderId='" + "' Order By MessageTimeStamp asc", null);
            c = db.rawQuery("select * from activitymessages WHERE MessageSenderId='" + RecieverId + "' AND ReceiverId = 1  AND IsNotificationMessage = '1' ", null);

            int total = c.getCount();
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    ChatMessageEntity adminactivityMessage = new ChatMessageEntity();
                    adminactivityMessage.SenderId = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_SENDER_ID));
                    adminactivityMessage.ReceiverId = c.getString(c
                            .getColumnIndex(KEY_RECIEVER_ID));
                    adminactivityMessage.MessageBody = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_BODY));
                    adminactivityMessage.MessageType = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_TYPE));
                    adminactivityMessage.Id = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_ID));
                    adminactivityMessage.SenderName = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_SENDER_NAME));
//adminactivityMessage.UserType=c.getString(c.getColumnIndex(KEY_USER_TYPE));
                    adminactivityMessage.TimeStamp = c.getString(c
                            .getColumnIndex(KEY_MESSAGE_TIME));
                    adminactivityMessage.ImagePath = c.getString(c
                            .getColumnIndex(KEY_MEDIA_PATH));
                    adminactivityMessage.Option2Value = c.getString(c
                            .getColumnIndex(KEY_OPTION1_VALUE));

                    adminactivityMessages.add(adminactivityMessage);

                    c.moveToNext();
                }

            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        db.close();
        if (c != null)
            c.close();
        return adminactivityMessages;

    }


    public void InsertActivitySenderMessages(ChatMessageEntity chatMessage) {
        SQLiteDatabase db = super.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = null;
        Date orderDate;
        long check = 0;

        try {
            values.put(KEY_MESSAGE_ACTIVITY_ID, chatMessage.ActivityId);

            if (chatMessage.SenderId != null)
                values.put(KEY_MESSAGE_SENDER_ID, chatMessage.SenderId);
            if (chatMessage.ReceiverId != null) {
                values.put(KEY_RECIEVER_ID, chatMessage.ReceiverId);
            }
            if (chatMessage.SenderName != null)
                values.put(KEY_MESSAGE_SENDER_NAME, chatMessage.SenderName);
            if (chatMessage.Id != null)
                values.put(KEY_MESSAGE_ID, chatMessage.Id);
            // if (chatMessage.MessageBody != null)
            values.put(KEY_MESSAGE_BODY, chatMessage.MessageBody);
            if (chatMessage.MessageType != null)
                values.put(KEY_MESSAGE_TYPE, chatMessage.MessageType);
            if (chatMessage.TimeStamp != null)
                values.put(KEY_MESSAGE_TIME, chatMessage.TimeStamp);
            if (chatMessage.ImagePath != null) {
                values.put(KEY_MEDIA_PATH, chatMessage.ImagePath);
            }
            if (chatMessage.Option2Value != null) {
                values.put(KEY_OPTION1_VALUE, chatMessage.Option2Value);
            }
            values.put(KEY_IS_NOTIFICATION_MESSAGE, chatMessage.IsNotificationMessageRead);

            String query = "SELECT * FROM activitymessages WHERE ReceiverId = '"
                    + chatMessage.ReceiverId + "' AND MessageSenderId = '" + chatMessage.SenderId
                    + "' AND MessageID = '" + chatMessage.Id + "'";
            cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst())
                db.update(TABLE_ACTIVITY_MESSAGES, values, "ReceiverId= '"
                        + chatMessage.ReceiverId + "' AND MessageSenderId = '" + chatMessage.SenderId
                        + "' AND MessageID = '" + chatMessage.Id + "'", null);
            else
                check = db.insertOrThrow(TABLE_ACTIVITY_MESSAGES, null, values);

        } catch (Exception ex) {
            Log.e("InsertActivityMessage", ex.getMessage());
        }
        values.clear();
        cursor.close();
        db.close();

    }

    public int getNotification() {
        int count = 0;
        Cursor cursor = null;
        SQLiteDatabase db = super.getWritableDatabase();
        try {

            cursor = db.rawQuery(
                    "SELECT * FROM notifications WHERE IsNotificationShown =0", null);
            count = cursor.getCount();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        db.close();
        if (cursor != null)
            cursor.close();
        return count;
    }

    public void updateChatNotificationMessage(ChatMessageEntity chatMessageEntity) {
        Cursor cursor = null, cursor1 = null;
        try {
            SQLiteDatabase db = super.getWritableDatabase();

            try {

                ContentValues values = new ContentValues();
                values.put(KEY_RECIEVER_ID, chatMessageEntity.ReceiverId);
                values.put(KEY_MESSAGE_SENDER_ID, chatMessageEntity.SenderId);
                values.put(KEY_MESSAGE_ID,chatMessageEntity.Id);
                values.put(KEY_IS_NOTIFICATION_MESSAGE, false);

                //localId = db.update(TABLE_ACTIVITY_MESSAGES, values, KEY_CHAT_ITEM_POSITION + "=" + chatMessageEntity.chatItemPosition, null);
                String query = "SELECT * FROM activitymessages WHERE ReceiverId = '"
                        + chatMessageEntity.ReceiverId + "' AND MessageSenderId = '" + chatMessageEntity.SenderId + "' AND MessageID = '" + chatMessageEntity.Id + "'";

                cursor = db.rawQuery(query, null);
                if (cursor.moveToFirst()) {
                    db.update(TABLE_ACTIVITY_MESSAGES, values, "ReceiverId= '" + chatMessageEntity.ReceiverId + "' AND MessageSenderId = '" + chatMessageEntity.SenderId
                            + "' AND MessageID = '" + chatMessageEntity.Id + "'", null);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public MyCustomerEntity getCustomerDetails(ChatMessagesActivity chatMessagesActivity, String recieverId) {
        MyCustomerEntity item = new MyCustomerEntity();
        SQLiteDatabase db = super.getWritableDatabase();
       /* String selectQuery = "SELECT * FROM customerchatdetails";*/
        String selectQuery = null;


            selectQuery = "SELECT * FROM customerchatdetails WHERE CustomerId ='" + recieverId + "' ";


        Cursor c = db.rawQuery(selectQuery, null);
        try {
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    item = new MyCustomerEntity();
                    item.CustomerId = c.getString(c.getColumnIndex(KEY_CUSTOMER_ID));
                    item.CustomerName = c.getString(c.getColumnIndex(KEY_CUSTOMER_NAME));
                    item.LastMessage = c.getString(c.getColumnIndex(KEY_LAST_MESSAGE));
                    item.LastActivityTime = c.getString(c.getColumnIndex(KEY_LAST_ACTIVITY_TIME));
                    item.MessageCount = c.getInt(c.getColumnIndex(KEY_MESSAGE_COUNT));


                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (c != null)
            c.close();
        db.close();
        return item;

    }

    public MyCustomerEntity getPharmacyDetails(ChatMessagesActivity chatMessagesActivity, String recieverId) {

        MyCustomerEntity item = new MyCustomerEntity();
        SQLiteDatabase db = super.getWritableDatabase();
       /* String selectQuery = "SELECT * FROM customerchatdetails";*/
        String selectQuery = null;



        selectQuery = "SELECT * FROM pharmacychatdetails WHERE  PharmacyId = '" + recieverId + "' ";


        Cursor c = db.rawQuery(selectQuery, null);
        try {
            if (c.moveToFirst()) {
                while (c.isAfterLast() == false) {
                    item = new MyCustomerEntity();
                    item.PharmacyId = c.getString(c.getColumnIndex(KEY_PHARMACY_ID));
                    item.PharmacyName = c.getString(c.getColumnIndex(KEY_PHARMACY_NAME));
                    item.LastMessage = c.getString(c.getColumnIndex(KEY_LAST_MESSAGE));
                    item.LastActivityTime = c.getString(c.getColumnIndex(KEY_LAST_ACTIVITY_TIME));
                    item.MessageCount = c.getInt(c.getColumnIndex(KEY_MESSAGE_COUNT));
                    item.Passwordsalt=c.getString(c.getColumnIndex(KEY_PASSWORD_SALT));


                    c.moveToNext();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (c != null)
            c.close();
        db.close();
        return item;

    }

    public int GetAdminChatActivityMessageCount(String ReceiverId) {
        int count=0;
        SQLiteDatabase db = super.getWritableDatabase();
        List<ChatMessageEntity> adminactivityMessages = new ArrayList<ChatMessageEntity>();
        Cursor c = null;
        try {
            //   c = db.rawQuery("select * from activitymessages where OrderId='" + "' Order By MessageTimeStamp asc", null);
            c = db.rawQuery("select * from activitymessages WHERE ReceiverId='" + ReceiverId + "'  Or MessageSenderId='" + ReceiverId + "'", null);
            count = c.getCount();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        db.close();
        if (c != null)
            c.close();
        return count;
    }
    public void addUpdateUserDetails(UserKeyDetails userKeyDetails) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = null;
        try {

            if (userKeyDetails.getDeviceid() != null)
                values.put(KEY_DEVICE_ID, userKeyDetails.getDeviceid());
            if (userKeyDetails.getDeviceuniqueid() != null)
                values.put(KEY_DEVICE_UNIQUE_ID,
                        userKeyDetails.getDeviceuniqueid());
            if (userKeyDetails.getUserid() != null)
                values.put(KEY_USER_ID_TRIMMED, userKeyDetails.getUserid());
            if (userKeyDetails.getPhoneNumber() != null)
                values.put(KEY_PHONE_NUMBER, userKeyDetails.getPhoneNumber());
            if (userKeyDetails.getNickname() != null)
                values.put(KEY_NICK_NAME, userKeyDetails.getNickname());
            if (userKeyDetails.getSharedsecretkey() != null)
                values.put(KEY_SHARED_SECRET,
                        userKeyDetails.getSharedsecretkey());
            values.put(KEY_IS_ZIBMI_COMMUNITY, 0);
            if (userKeyDetails.getprofilePicture() != null)
                values.put(KEY_USER_PROFILE_PICTURE,
                        userKeyDetails.getprofilePicture());
            if (userKeyDetails.getemailadress() != null)
                values.put(KEY_EMAIL_ADDRESS, userKeyDetails.getemailadress());
            if (userKeyDetails.getprofileadress() != null)
                values.put(KEY_PROFILE_ADDRESS,
                        userKeyDetails.getprofileadress());
            if (userKeyDetails.getprofilecity() != null)
                values.put(KEY_PROFILE_CITY, userKeyDetails.getprofilecity());
            if (userKeyDetails.getprofilestate() != null)
                values.put(KEY_PROFILE_STATE, userKeyDetails.getprofilestate());
            if (userKeyDetails.getCountry() != null)
                values.put(KEY_USER_COUNTRY, userKeyDetails.getCountry());
            if (userKeyDetails.getLandmark() != null)
                values.put(KEY_USER_LANDMARK, userKeyDetails.getLandmark());
            if (userKeyDetails.getPinCode() != null)
                values.put(KEY_USER_PINCODE, userKeyDetails.getPinCode());
            if (userKeyDetails.getGender() != null)
                values.put(KEY_USER_GENDER,
                        userKeyDetails.getGender());
            if (userKeyDetails.getBloodGroup() != null)
                values.put(KEY_USER_BLOOD_GROUP, userKeyDetails.getBloodGroup());
            if (userKeyDetails.getBloodPressure() != null)
                values.put(KEY_USER_BLOOD_PRESSURE,
                        userKeyDetails.getBloodPressure());
            if (userKeyDetails.getSystolic() != null)
                values.put(KEY_USER_SYSTOLIC,
                        userKeyDetails.getSystolic());
            if (userKeyDetails.getDyastolic() != null)
                values.put(KEY_USER_DIASTOLIC,
                        userKeyDetails.getDyastolic());
            if (userKeyDetails.getBmiheight() != null)
                values.put(KEY_USER_BMI_HEIGHT,
                        userKeyDetails.getBmiheight());
            if (userKeyDetails.getBmiweight() != null)
                values.put(KEY_USER_BMI_WEIGHT,
                        userKeyDetails.getBmiweight());
            if (userKeyDetails.getDob() != null)
                values.put(KEY_USER_DOB, userKeyDetails.getDob());
            if (userKeyDetails.getpersonalStatus() != null)
                values.put(KEY_USER_PERSONAL_STATUS,
                        userKeyDetails.getpersonalStatus());
            if (userKeyDetails.getPasswordSalt() != null)
                values.put(KEY_PASSWORD_SALT,
                        userKeyDetails.getPasswordSalt());
            cursor = db.query(TABLE_USER_KEY_INFORMATION,
                    new String[]{KEY_USER_ID_TRIMMED}, KEY_USER_ID_TRIMMED
                            + " = ?",
                    new String[]{userKeyDetails.getUserid()}, null, null,
                    null, null);
            db.update(TABLE_USER_KEY_INFORMATION, values, "UserTableId = 1",
                    null);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        values.clear();
        if (cursor != null)
            cursor.close();
        db.close();
    }
    public UserKeyDetails getTokenFromDB() {
        UserKeyDetails item = new UserKeyDetails();
        SQLiteDatabase db = super.getReadableDatabase();
        String selectQuery = "SELECT * FROM UserKeyDetails WHERE UserTableId=1";
        Cursor c = db.rawQuery(selectQuery, null);
        try {
            c.moveToNext();
            if (c.moveToFirst()) {
                if ((c.getInt(c.getColumnIndex(KEY_IS_ZIBMI_USER))) == 1) {
                    item.IsUser = true;
                } else {
                    item.IsUser = false;
                }

                item.setDeviceid(c.getString(c.getColumnIndex(KEY_DEVICE_ID)));
                item.setDeviceuniqueid(c.getString(c
                        .getColumnIndex(KEY_DEVICE_UNIQUE_ID)));
                item.setUserid(c.getString(c.getColumnIndex(KEY_USER_ID_TRIMMED)));
                item.setPhoneNumber(c.getString(c.getColumnIndex(KEY_PHONE_NUMBER)));
                item.setNickname(c.getString(c.getColumnIndex(KEY_NICK_NAME)));
                item.setSharedsecretkey(c.getString(c
                        .getColumnIndex(KEY_SHARED_SECRET)));
                item.setemailadress(c.getString(c.getColumnIndex(KEY_EMAIL_ADDRESS)));
                item.setprofileadress(c.getString(c
                        .getColumnIndex(KEY_PROFILE_ADDRESS)));
                item.setprofilecity(c.getString(c.getColumnIndex(KEY_PROFILE_CITY)));
                item.setprofilestate(c.getString(c
                        .getColumnIndex(KEY_PROFILE_STATE)));
                item.setprofilePicture(c.getString(c
                        .getColumnIndex(KEY_USER_PROFILE_PICTURE)));
                item.setpersonalStatus(c.getString(c
                        .getColumnIndex(KEY_USER_PERSONAL_STATUS)));
                item.setCountry(c.getString(c.getColumnIndex(KEY_USER_COUNTRY)));
                item.setPinCode(c.getString(c.getColumnIndex(KEY_USER_PINCODE)));
                item.setLandmark(c.getString(c.getColumnIndex(KEY_USER_LANDMARK)));
                item.setGender(c.getString(c
                        .getColumnIndex(KEY_USER_GENDER)));
                item.setDob(c.getString(c.getColumnIndex(KEY_USER_DOB)));
                item.setBloodGroup(c.getString(c
                        .getColumnIndex(KEY_USER_BLOOD_GROUP)));
                item.setBloodPressure(c.getString(c
                        .getColumnIndex(KEY_USER_BLOOD_PRESSURE)));
                item.setSystolic(c.getString(c
                        .getColumnIndex(KEY_USER_SYSTOLIC)));
                item.setDyastolic(c.getString(c
                        .getColumnIndex(KEY_USER_DIASTOLIC)));
                item.setBmiheight(c.getString(c
                        .getColumnIndex(KEY_USER_BMI_HEIGHT)));
                item.setBmiweight(c.getString(c
                        .getColumnIndex(KEY_USER_BMI_WEIGHT)));
                item.setAlarmCount(c.getString(c
                        .getColumnIndex(KEY_ALARM_COUNT)));
                item.setPasswordSalt(c.getString(c
                        .getColumnIndex(KEY_PASSWORD_SALT)));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (c != null)
            c.close();
        db.close();
        return item;
    }
    public void CreateFirstEntry() {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        Cursor cursor = null;
        try {
           /* values.put(KEY_PHONE_NUMBER, "");
            values.put(KEY_NICK_NAME, "");
            values.put(KEY_USER_ID_TRIMMED, "");*/
            values.put(KEY_IS_ZIBMI_USER, true);
            String selectQuery = "SELECT * FROM UserKeyDetails WHERE UserTableId=1";
            cursor = db.rawQuery(selectQuery, null);
            if (cursor.moveToFirst()) {
                db.updateWithOnConflict(TABLE_USER_KEY_INFORMATION, values,
                        "UserTableId =" + 1, null,
                        SQLiteDatabase.CONFLICT_IGNORE);
            } else
                db.insertOrThrow(TABLE_USER_KEY_INFORMATION, null, values);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        values.clear();
        db.close();
        if (cursor != null)
            cursor.close();
    }
}

package com.emedsurecrm.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MocehatCrmDBHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MocehatCRMDB";

    // /Table Name
    public static final String TABLE_CUSTOMER_CHAT_DETAILS = "customerchatdetails";
    public static final String TABLE_PHARMACY_CHAT_DETAILS = "pharmacychatdetails";
    public static final String TABLE_ACTIVITY_MESSAGES = "activitymessages";
    public static final String TABLE_USER_KEY_INFORMATION = "UserKeyDetails";
    ///Column///

    ////  UserKeyDetails column//
    protected static final String KEY_USER_TABLE_ID = "UserTableId";
    protected static final String KEY_DEVICE_ID = "DeviceID";
    protected static final String KEY_DEVICE_UNIQUE_ID = "DeviceUniqueID";
    protected static final String KEY_PHONE_NUMBER = "PhoneNumber";
    protected static final String KEY_NICK_NAME = "NickName";
    protected static final String KEY_BACKEND_NAME = "BackEndName";
    protected static final String KEY_SHARED_SECRET = "SharedKey";
    protected static final String KEY_IS_ZIBMI_USER = "IsZibMiUser";
    protected static final String KEY_EMAIL_ADDRESS = "Email_Address";
    protected static final String KEY_PROFILE_ADDRESS = "Profile_Address";
    protected static final String KEY_PROFILE_CITY = "Profile_City";
    protected static final String KEY_PROFILE_STATE = "Profile_State";
    protected static final String KEY_USER_COUNTRY = "country";
    protected static final String KEY_USER_LANDMARK = "landmark";
    protected static final String KEY_USER_PINCODE = "pincode";
    protected static final String KEY_USER_GENDER = "gender";
    protected static final String KEY_USER_BLOOD_GROUP = "bloodgroup";
    protected static final String KEY_USER_BMI_WEIGHT = "bmiweight";
    protected static final String KEY_USER_BMI_HEIGHT = "bmiheight";
    protected static final String KEY_USER_SYSTOLIC = "systolic";
    protected static final String KEY_USER_DIASTOLIC = "diastolic";
    protected static final String KEY_USER_BLOOD_PRESSURE = "bloodpressure";
    protected static final String KEY_USER_DOB = "dob";
    protected static final String KEY_ALARM_COUNT = "AlarmCount";
    protected static final String KEY_IS_ZIBMI_COMMUNITY = "IsZibMiCommunity";
    protected static final String KEY_USER_PROFILE_PICTURE = "ProfilePicture";
    protected static final String KEY_USER_PERSONAL_STATUS = "PersonalStatus";
    protected static final String KEY_USER_ID_TRIMMED = "UserGuidTrimmed";
    public static final String KEY_IMAGE_URI = "contactimage";


    //Customer Chat Coloum
    protected static final String KEY_CUSTOMER_ID = "CustomerId";
    protected static final String KEY_CUSTOMER_NAME = "CustomerName";
    protected static final String KEY_LAST_MESSAGE = "LastMessage";
    protected static final String KEY_LAST_ACTIVITY_TIME = "LastActivityTime";
    protected static final String KEY_MESSAGE_COUNT = "MessageCount";

    //Pharmacy Chat Coloum

    protected static final String KEY_PHARMACY_ID = "PharmacyId";
    protected static final String KEY_PHARMACY_NAME = "PharmacyName";
    protected static final String KEY_PASSWORD_SALT = "PasswordSalt";


    ///Chat coloumn


    protected static final String KEY_MESSAGE_ACTIVITY_ID = "activityGuid";
    protected static final String KEY_MESSAGE_ID = "MessageID";
    protected static final String KEY_MESSAGE_TIME = "MessageTime";
    protected static final String KEY_MESSAGE_SENDER_NAME = "MessageSenderName";
    protected static final String KEY_MESSAGE_SENDER_ID = "MessageSenderId";
    protected static final String KEY_MESSAGE_BODY = "MessageBody";
    protected static final String KEY_IS_SYSTEM_GENERATED = "SystemGenerated";
    protected static final String KEY_MEDIA_PATH = "MediaPath";
    protected static final String KEY_IS_MESSAGE_PERSISTANCE_REQUIRED = "IsMessagePersistanceRequired";
    protected static final String KEY_RECIEVER_ID = "ReceiverId";
    protected static final String KEY_OPTION1_VALUE = "Option1Value";
    protected static final String KEY_PARENT_ACTIVITY_ID = "ParentActivityGuid";
    public static final String KEY_MESSAGE_TYPE = "MessageType";
    protected static final String KEY_OPTION1_NAME = "Option1Name";


    protected static final String KEY_IS_NOTIFICATION_MESSAGE = "IsNotificationMessage";

    protected static final String KEY_MEDIA_UPLOAD_STATUS = "MediaUploadStatus";
    protected static final String KEY_CHAT_ITEM_POSITION = "ChatItemPosition";


    Context con;

    public MocehatCrmDBHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        con = context;
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE "
                + TABLE_USER_KEY_INFORMATION + "(" + KEY_USER_TABLE_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT ," + KEY_USER_ID_TRIMMED
                + " TEXT ," + KEY_DEVICE_ID + " TEXT , " + KEY_IS_ZIBMI_USER
                + " INTEGER , " + KEY_IS_ZIBMI_COMMUNITY + " INTEGER , "
                + KEY_EMAIL_ADDRESS + " TEXT , " + KEY_DEVICE_UNIQUE_ID
                + " TEXT ," + KEY_PHONE_NUMBER + " TEXT ," + KEY_NICK_NAME
                + " TEXT ," + KEY_BACKEND_NAME + " TEXT ," + KEY_SHARED_SECRET
                + " TEXT ," + KEY_USER_PERSONAL_STATUS + " TEXT ,"
                + KEY_USER_PROFILE_PICTURE + " TEXT ," + KEY_PROFILE_ADDRESS
                + " TEXT ," + KEY_PROFILE_CITY + " TEXT ," + KEY_USER_COUNTRY
                + " TEXT ," + KEY_USER_PINCODE + " TEXT ," + KEY_USER_LANDMARK
                + " TEXT ," + KEY_USER_GENDER + " TEXT ,"
                + KEY_USER_BLOOD_GROUP + " TEXT ," + KEY_USER_BLOOD_PRESSURE
                + " TEXT ," + KEY_USER_DOB + " TEXT ," + KEY_USER_SYSTOLIC
                + " TEXT ," + KEY_USER_DIASTOLIC + " TEXT ," + KEY_USER_BMI_HEIGHT
                + " TEXT ," + KEY_USER_BMI_WEIGHT + " TEXT ," + KEY_PROFILE_STATE
                + " TEXT , " + KEY_IMAGE_URI + " TEXT , " + KEY_ALARM_COUNT + " TEXT ," + KEY_PASSWORD_SALT + " TEXT )";
        db.execSQL(CREATE_CONTACTS_TABLE);

        String CREATE_ALL_CUSTOMER_CHAT_TABLE = "CREATE TABLE "
                + TABLE_CUSTOMER_CHAT_DETAILS + "(" + KEY_CUSTOMER_ID
                + " TEXT ," + KEY_CUSTOMER_NAME + " TEXT ," + KEY_LAST_MESSAGE + " TEXT," + KEY_LAST_ACTIVITY_TIME + " TEXT,"
                + KEY_MESSAGE_COUNT + " TEXT )";
        db.execSQL(CREATE_ALL_CUSTOMER_CHAT_TABLE);

        String CREATE_ALL_PHARMACY_CHAT_TABLE = "CREATE TABLE "
                + TABLE_PHARMACY_CHAT_DETAILS + "(" + KEY_PHARMACY_ID
                + " TEXT ," + KEY_PHARMACY_NAME + " TEXT ," + KEY_PASSWORD_SALT + " TEXT ,"+ KEY_LAST_MESSAGE + " TEXT," + KEY_LAST_ACTIVITY_TIME + " TEXT,"
                + KEY_MESSAGE_COUNT + " TEXT )";
        db.execSQL(CREATE_ALL_PHARMACY_CHAT_TABLE);

        String CREATE_ACTIVITY_MESSAGE_TABLE = "CREATE TABLE "
                + TABLE_ACTIVITY_MESSAGES + "(" + KEY_MESSAGE_ACTIVITY_ID
                + " TEXT ," + KEY_PARENT_ACTIVITY_ID + " TEXT ,"
                + KEY_MESSAGE_ID + " TEXT  ,"
                + KEY_MESSAGE_TIME + " TEXT ," + KEY_MESSAGE_BODY + " TEXT ,"
                + KEY_MESSAGE_SENDER_ID + " TEXT,"
                + KEY_RECIEVER_ID + " TEXT,"
                + KEY_IS_SYSTEM_GENERATED
                + " INTEGER ," + KEY_MESSAGE_TYPE + " TEXT,"
                + KEY_MESSAGE_SENDER_NAME + " TEXT," + KEY_MEDIA_PATH
                + " TEXT,"
                + KEY_MEDIA_UPLOAD_STATUS + " TEXT," + KEY_CHAT_ITEM_POSITION + " TEXT,"  + KEY_IS_NOTIFICATION_MESSAGE + " INTEGER," + KEY_OPTION1_NAME + " TEXT,"
                + KEY_OPTION1_VALUE + " TEXT," + KEY_IS_MESSAGE_PERSISTANCE_REQUIRED + " INTEGER)";
        db.execSQL(CREATE_ACTIVITY_MESSAGE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}

package com.emedsurecrm.NewUI;

public class DummyModel {
	
	private long mId;
	private String mImageURL;
	private int mText;
	private int mIconRes;

	public DummyModel() {
	}

	public DummyModel(long id, String imageURL, int text, int iconRes) {
		mId = id;
		mImageURL = imageURL;
		mText = text;
		mIconRes = iconRes;
	}

	public long getId() {
		return mId;
	}

	public void setId(long id) {
		mId = id;
	}

	public String getImageURL() {
		return mImageURL;
	}

	public void setImageURL(String imageURL) {
		mImageURL = imageURL;
	}

	public int getString() {
		return mText;
	}

	public void setText(int text) {
		mText = text;
	}

	public int getIconRes() {
		return mIconRes;
	}

	public void setIconRes(int iconRes) {
		mIconRes = iconRes;
	}



}
